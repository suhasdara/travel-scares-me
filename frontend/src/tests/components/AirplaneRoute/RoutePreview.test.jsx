import React from "react";
import { renderWithRouter as r } from "tests/utils";
import RoutePreview from "components/AirplaneRoute/RoutePreview";

const routeData = {
  airline_code: "DOG",
  flight_num: 2,
  id: 1,
  departure_city: "NYC",
  departure_country: "USA",
  arrival_city: "LON",
  arrival_country: "UK",
};

const defaultProps = {
  routeData,
};

const render = (props = {}) => r(<RoutePreview {...defaultProps} {...props} />);

describe("RoutePreview", () => {
  it("shows the airplane route data", () => {
    const { getAllByRole, getByText } = render();

    expect(getByText(/DOG 2/)).toBeInTheDocument();
    expect(getAllByRole("link")[0]).toHaveAttribute("href", "/routes/1");
    expect(getByText(/NYC/)).toBeInTheDocument();
    expect(getByText(/USA/)).toBeInTheDocument();
    expect(getByText(/LON/)).toBeInTheDocument();
    expect(getByText(/UK/)).toBeInTheDocument();
  });
});
