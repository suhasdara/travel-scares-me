import React from "react";
import { renderWithRouter as r } from "tests/utils";
import AirplaneRoute from "components/AirplaneRoute/AirplaneRoute";
import * as reactQuery from "react-query";

const mockUseQuery = jest.fn();

const defaultProps = { articleId: 1 };

const departureData = {
  country_flag_url: "https://usa.com",
  country: "USA",
  country_id: 1,
  city: "New York City",
  city_code: "NYC",
  airport: "John F. Kennedy",
  airport_code: "JFK",
  latitude: 10,
  longitude: -5,
  terminal: "E",
  time: "00:30:00",
  time_zone: "UTC+3:30",
  maps_url: "https://usa-map.com",
};

const arrivalData = {
  country_flag_url: "https://uk.com",
  country: "UK",
  country_id: 1,
  city: "London",
  city_code: "LON",
  airport: "City",
  airport_code: "CTY",
  latitude: 40,
  longitude: -25,
  terminal: "F",
  time: "00:20:00",
  time_zone: "UTC+5:30",
  maps_url: "https://uk-map.com",
};

const routeData = {
  airline_code: "DOG",
  flight_num: 2,
  id: 1,
  departure_city: "NYC",
  departure_country: "USA",
  arrival_city: "LON",
  arrival_country: "UK",
  airline: "DATADOG",
  departure: departureData,
  arrival: arrivalData,
  related_articles: [{ id: 1, title: "ship gets free!" }],
};

const render = (props = {}) =>
  r(<AirplaneRoute {...defaultProps} {...props} />);

beforeEach(() => {
  reactQuery.useQuery = mockUseQuery;
});

describe("AirplaneRoute", () => {
  describe("when data is loading", () => {
    it("shows the page spinner", () => {
      mockUseQuery.mockReturnValue({ isLoading: true });
      const { container } = render();

      const html = container.innerHTML;
      expect(/spinner/.test(html)).toEqual(true);
    });
  });

  describe("when the data has loaded", () => {
    it("shows airplane route data", () => {
      mockUseQuery.mockReturnValue({ isLoading: false, data: routeData });
      const { getByAltText, getByText } = render();
      const testLocationData = (locationData) => {
        expect(getByAltText(`${locationData.country} flag`)).toHaveAttribute(
          "src",
          locationData.country_flag_url
        );
        expect(getByText(locationData.country)).toHaveAttribute(
          "href",
          `/countries/${locationData.country_id}`
        );
        expect(
          getByText(`${locationData.city} (${locationData.city_code})`)
        ).toBeInTheDocument();
        expect(
          getByText(`${locationData.airport} (${locationData.airport_code})`)
        ).toBeInTheDocument();
        expect(
          getByText(`${locationData.latitude}°, ${locationData.longitude}°`)
        ).toBeInTheDocument();
        expect(getByText(`${locationData.terminal}`)).toBeInTheDocument();
        expect(getByText(`${locationData.time}`)).toBeInTheDocument();
        expect(getByText(`${locationData.time_zone}`)).toBeInTheDocument();
      };

      expect(getByText(/DATADOG Flight 2/)).toBeInTheDocument();
      testLocationData(routeData.departure);
      testLocationData(routeData.arrival);
    });

    it("shows related news", () => {
      mockUseQuery.mockReturnValue({ isLoading: false, data: routeData });
      const { getByText } = render();

      expect(getByText(/News associated with UK/)).toBeInTheDocument();
      expect(getByText(/ship gets free!/)).toHaveAttribute(
        "href",
        "/articles/1"
      );
    });
  });
});
