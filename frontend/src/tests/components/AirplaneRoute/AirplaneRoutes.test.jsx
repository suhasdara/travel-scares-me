import React from "react";
import * as reactQuery from "react-query";
import userEvent from "@testing-library/user-event";
import AirplaneRoutes from "components/AirplaneRoute/AirplaneRoutes";
import { renderWithRouter as r } from "tests/utils";

const mockUseQuery = jest.fn();

const getRouteData = (data = {}) => ({
  airline_code: "DOG",
  flight_num: 2,
  id: 1,
  departure_city: "NYC",
  departure_country: "USA",
  arrival_city: "LON",
  arrival_country: "UK",
  ...data,
});

const defaultProps = {};

const render = (props = {}) =>
  r(<AirplaneRoutes {...defaultProps} {...props} />);

beforeEach(() => {
  reactQuery.useQuery = mockUseQuery;
});

describe("AirplaneRoutes", () => {
  it("displays title", () => {
    mockUseQuery.mockReturnValue({});
    const { getByText } = render();

    expect(getByText(/Airplane Routes/)).toBeInTheDocument();
  });

  describe("when data is loading", () => {
    it("displays spinner", () => {
      mockUseQuery.mockReturnValue({ isLoading: true });
      const { container } = render();

      const html = container.innerHTML;
      expect(/spinner/.test(html)).toEqual(true);
    });
  });

  it("calls useQuery with the key and function", () => {
    mockUseQuery.mockReturnValue({});
    render();

    expect(mockUseQuery).lastCalledWith(
      ["routes", 1, expect.any(Number), expect.any(Object)],
      expect.any(Function)
    );
  });

  describe("when first page data is loaded", () => {
    const firstPageData = {
      isLoading: false,
      data: {
        count: 3,
        routes: [
          getRouteData(),
          getRouteData({ id: 2, airline_code: "CAT" }),
          getRouteData({ id: 3, airline_code: "MOUSE" }),
        ],
      },
    };

    it("displays article previews", () => {
      mockUseQuery.mockReturnValue(firstPageData);
      const { getByText } = render();

      expect(getByText(/DOG/)).toBeInTheDocument();
      expect(getByText(/CAT/)).toBeInTheDocument();
      expect(getByText(/MOUSE/)).toBeInTheDocument();
    });
  });

  describe("when next page is clicked", () => {
    it("requests next page of data", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: { count: 100, routes: [getRouteData()] },
      });
      const { getAllByText } = render();

      expect(mockUseQuery).lastCalledWith(
        [expect.any(String), 1, expect.any(Number), expect.any(Object)],
        expect.any(Function)
      );

      getAllByText(/Next/)[0].click();

      expect(mockUseQuery).lastCalledWith(
        [expect.any(String), 2, expect.any(Number), expect.any(Object)],
        expect.any(Function)
      );
    });
  });

  describe("when filter is applied", () => {
    it("requests new data using filter", () => {
      mockUseQuery.mockReturnValue({});
      const { getByText } = render();

      getByText(/Change Filters/).click();
      userEvent.click(
        getByText(/Arrival City/)
          .closest(".row")
          .querySelector(".dropdown-toggle")
      );
      getByText(/Washington/).click();
      getByText(/Apply/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            arrivalCity: "Washington",
          }),
        ]),
        expect.any(Function)
      );
    });
  });

  describe("when sorting is applied", () => {
    it("requests new data using sorting", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: { routes: [getRouteData()], count: 100 },
      });
      const { getByText } = render();

      getByText(/Arrival city/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            orderBy: "arrival_city",
            ordering: "asc",
          }),
        ]),
        expect.any(Function)
      );
      getByText(/Arrival city/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            orderBy: "arrival_city",
            ordering: "desc",
          }),
        ]),
        expect.any(Function)
      );
    });
  });

  describe("when a search is entered", () => {
    it("requests new data with search query", () => {
      mockUseQuery.mockReturnValue({});
      const { getByPlaceholderText } = render();

      const searchInput = getByPlaceholderText(/Search/);
      userEvent.type(searchInput, "Test Search{enter}");
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            q: "Test Search",
          }),
        ]),
        expect.any(Function)
      );
    });
  });
});
