import React from "react";
import { renderWithAll as r } from "tests/utils";
import Search from "components/GlobalSearch/Search";
import * as countryQueries from "api/countries";
import * as articleQueries from "api/articles";
import * as routeQueries from "api/routes";

const mockGetCountries = jest.fn().mockReturnValue({
  count: 100,
  countries: [
    {
      capital: "D.C.",
      code: "US",
      flag_url: "https://example.com",
      id: 1,
      latitude: 10,
      longitude: -5,
      name: "United States",
      population: 5000,
      region: "US",
    },
  ],
});
const mockGetArticles = jest.fn().mockReturnValue({
  count: 100,
  articles: [
    {
      airline_code: "DOG",
      flight_num: 2,
      id: 1,
      departure_city: "NYC",
      departure_country: "USA",
      arrival_city: "LON",
      arrival_country: "UK",
    },
  ],
});
const mockGetRoutes = jest.fn().mockReturnValue({
  count: 100,
  routes: [
    {
      url_to_image: "https://istheshipstillstuck.com",
      title: "ship is too big",
      category: "how",
      published: "2021-03-13T12:00:00Z",
      article_language: "Arabic",
      country: "Egypt",
      id: 1,
    },
  ],
});
beforeEach(() => {
  countryQueries.getCountries = mockGetCountries;
  articleQueries.getArticles = mockGetArticles;
  routeQueries.getRoutes = mockGetRoutes;
});

const defaultProps = {
  searchQuery: "test query",
};
const render = (props = {}) => r(<Search {...defaultProps} {...props} />);

describe("Components GlobalSearch Search", () => {
  it("receives the search query in request", () => {
    render();

    expect(mockGetRoutes).lastCalledWith(
      expect.any(Number),
      expect.any(Number),
      expect.objectContaining({ q: "test query" })
    );
    expect(mockGetArticles).lastCalledWith(
      expect.any(Number),
      expect.any(Number),
      expect.objectContaining({ q: "test query" })
    );
    expect(mockGetCountries).lastCalledWith(
      expect.any(Number),
      expect.any(Number),
      expect.objectContaining({ q: "test query" })
    );
  });
});
