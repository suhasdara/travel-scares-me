import React from "react";
import { render as r } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import FilterModal from "components/Filter/FilterModal";

const onFilterSubmit = jest.fn();
const defaultProps = {
  filters: [
    {
      type: "select",
      active: false,
      name: "filter1",
      value: "",
      options: ["select1", "select2"],
    },
    {
      type: "range",
      activeMin: false,
      activeMax: false,
      name: "filter2",
      min: "",
      max: "",
    },
  ],
  onFilterSubmit,
};
const render = (props = {}) => r(<FilterModal {...defaultProps} {...props} />);

describe("Components Filter FilterModal", () => {
  describe("when filters are changed and submitted", () => {
    it("submits new filters", () => {
      const { getByText } = render();

      getByText(/Change Filters/).click();
      userEvent.click(
        getByText(/filter1/)
          .closest(".row")
          .querySelector(".dropdown-toggle")
      );
      getByText(/select1/).click();
      userEvent.type(getByText(/Min:/).querySelector("input"), "12");
      userEvent.type(getByText(/Max:/).querySelector("input"), "20");
      getByText(/Apply/).click();
      expect(onFilterSubmit).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            name: "filter1",
            value: "select1",
            active: true,
          }),
          expect.objectContaining({
            name: "filter2",
            min: "12",
            max: "20",
            activeMin: true,
            activeMax: true,
          }),
        ])
      );
    });
  });
});
