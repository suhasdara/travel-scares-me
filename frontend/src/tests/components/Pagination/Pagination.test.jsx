import React from "react";
import { act, render as r } from "@testing-library/react";
import Pagination from "components/Pagination";

const onPageClick = jest.fn();
const handlePageSizeChange = jest.fn();
const defaultProps = {
  currPage: 1,
  handlePageSizeChange,
  numResults: 200,
  onPageClick,
  pageSize: 8,
};

const render = (props = {}) => r(<Pagination {...defaultProps} {...props} />);

afterEach(() => {
  onPageClick.mockClear();
  handlePageSizeChange.mockClear();
});

describe("Pagination", () => {
  it("shows the number of results", () => {
    const { getByText } = render();

    expect(getByText(/Total/).textContent).toEqual("Total 200 items");
  });

  it("shows the current page number and total number of pages", () => {
    const { getByText } = render();

    const pageDropdown = getByText(/25/);
    expect(pageDropdown).toBeInTheDocument();
    expect(pageDropdown.textContent).toEqual("1of 25");
  });

  it("shows the page size", () => {
    const { getByText } = render();

    expect(getByText(/8/)).toBeInTheDocument();
  });

  describe("when the next page button is clicked", () => {
    describe("when it is not the last page", () => {
      it("requests the next page", () => {
        const { getAllByText } = render();

        getAllByText(/Next/)[0].click();
        expect(onPageClick).toHaveBeenCalledTimes(1);
        expect(onPageClick).lastCalledWith(2);
      });
    });

    describe("when it is the last page", () => {
      it("does not request any page", () => {
        const { getAllByText } = render({ currPage: 25 });

        getAllByText(/Next/)[0].click();
        expect(onPageClick).not.toHaveBeenCalled();
      });
    });
  });

  describe("when the last page button is clicked", () => {
    describe("when it is not last page", () => {
      it("requests the last page", () => {
        const { getAllByText } = render();

        getAllByText(/Last/)[0].click();
        expect(onPageClick).toHaveBeenCalledTimes(1);
        expect(onPageClick).lastCalledWith(25);
      });
    });

    describe("when it is the last page", () => {
      it("does not request any page", () => {
        const { getAllByText } = render({ currPage: 25 });

        getAllByText(/Last/)[0].click();
        expect(onPageClick).not.toHaveBeenCalled();
      });
    });
  });

  describe("when the previous page button is clicked", () => {
    describe("when it is not the first page", () => {
      it("requests the previous page", () => {
        const { getAllByText } = render({ currPage: 25 });

        getAllByText(/Prev/)[0].click();
        expect(onPageClick).toHaveBeenCalledTimes(1);
        expect(onPageClick).lastCalledWith(24);
      });
    });

    describe("when it is the first page", () => {
      it("does not request any page", () => {
        const { getAllByText } = render();

        getAllByText(/Prev/)[0].click();
        expect(onPageClick).not.toHaveBeenCalled();
      });
    });
  });

  describe("when the first page button is clicked", () => {
    describe("when it is not the first page", () => {
      it("requests the first page", () => {
        const { getAllByText } = render({ currPage: 25 });

        getAllByText(/First/)[0].click();
        expect(onPageClick).toHaveBeenCalledTimes(1);
        expect(onPageClick).lastCalledWith(1);
      });
    });

    describe("when it is the first page", () => {
      it("does not request any page", () => {
        const { getAllByText } = render();

        getAllByText(/First/)[0].click();
        expect(onPageClick).not.toHaveBeenCalled();
      });
    });
  });

  describe("when another page size is selected", () => {
    it("requests the new page size", () => {
      const { getByText } = render();

      act(() => {
        getByText(/8/).click();
      });
      act(() => {
        getByText(/40/).click();
      });
      expect(onPageClick).toHaveBeenCalledTimes(1);
      expect(onPageClick).lastCalledWith(1);
      expect(handlePageSizeChange).toHaveBeenCalledTimes(1);
      expect(handlePageSizeChange).lastCalledWith(40);
    });
  });
});
