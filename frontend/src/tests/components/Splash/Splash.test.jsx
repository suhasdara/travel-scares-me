import React from "react";
import Splash from "components/Splash/Splash";
import { renderWithRouter as r } from "tests/utils";

const defaultProps = {};

const render = (props = {}) => r(<Splash {...defaultProps} {...props} />);

describe("Splash", () => {
  it("renders countries link", () => {
    const { getByText } = render();

    expect(getByText(/View countries/)).toHaveAttribute("href", "/countries");
  });

  it("renders articles link", () => {
    const { getByText } = render();

    expect(getByText(/View news articles/)).toHaveAttribute(
      "href",
      "/articles"
    );
  });

  it("renders airplane routes link", () => {
    const { getByText } = render();

    expect(getByText(/View airplane routes/)).toHaveAttribute(
      "href",
      "/routes"
    );
  });

  it("renders about link", () => {
    const { getByText } = render();

    expect(getByText(/About Us/)).toHaveAttribute("href", "/about");
  });

  it("renders repository link", () => {
    const { getByText } = render();

    expect(getByText(/GitLab Repository/)).toHaveAttribute(
      "href",
      "https://gitlab.com/suhasdara/travel-scares-me"
    );
  });
});
