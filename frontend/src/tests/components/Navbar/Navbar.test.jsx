import React from "react";
import { renderWithRouter as r } from "tests/utils";
import Navbar from "components/Navbar/Navbar";

const defaultProps = {};

const render = (props = {}) => r(<Navbar {...defaultProps} {...props} />);

describe("Navbar", () => {
  it("renders project title with link", () => {
    const { getByText } = render();

    const homeLink = getByText("Travel Scares Me").parentElement;
    expect(homeLink).toHaveAttribute("href", "/");
  });

  it("renders about link", () => {
    const { getByText } = render();

    expect(getByText("About")).toHaveAttribute("href", "/about");
  });

  it("renders airplane routes link", () => {
    const { getByText } = render();

    expect(getByText("Airplane Routes")).toHaveAttribute("href", "/routes");
  });

  it("renders countries link", () => {
    const { getByText } = render();

    expect(getByText("Countries")).toHaveAttribute("href", "/countries");
  });

  it("renders articles link", () => {
    const { getByText } = render();

    expect(getByText("Articles")).toHaveAttribute("href", "/articles");
  });
});
