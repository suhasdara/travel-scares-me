import React from "react";
import { act, render as r } from "@testing-library/react";
import About from "components/About/About";
import * as AboutStats from "components/About/AboutStats";
import { AboutStrings } from "components/About/AboutStrings";

const defaultProps = {};
const render = (props = {}) => r(<About {...defaultProps} {...props} />);

const gitlabStats = {
  commits: {
    "Suhas Dara": 6,
    "Peter Cardenas": 5,
    "Joseph Jiang": 5,
    "Matthew Bianchi": 5,
  },
  issues: {
    "Suhas Dara": 3,
    "Peter Cardenas": 2,
    "Joseph Jiang": 2,
    "Matthew Bianchi": 2,
  },
};
const aboutData = AboutStrings();
const mockGetStatsData = jest.fn();
beforeEach(() => {
  AboutStats.getStatsData = mockGetStatsData;
  mockGetStatsData.mockClear();
  mockGetStatsData.mockReturnValue(Promise.resolve(gitlabStats));
});

describe("About", () => {
  it("calls getStatsData for GitLab data", () => {
    render();

    expect(mockGetStatsData).toHaveBeenCalledTimes(1);
  });

  it("shows all team members", () => {
    const { getByText } = render();

    expect(getByText(/Peter Cardenas/)).toBeInTheDocument();
    expect(getByText(/Suhas Dara/)).toBeInTheDocument();
    expect(getByText(/Joseph Jiang/)).toBeInTheDocument();
    expect(getByText(/Matthew Bianchi/)).toBeInTheDocument();
  });

  it("shows total number of commits, issues, and tests", async () => {
    let getAllByText;
    await act(async () => {
      getAllByText = render().getAllByText;
    });
    const { totalTests } = aboutData;

    expect(getAllByText(/Commits/)[4].parentElement.textContent).toEqual(
      `Commits: 21, Issues: 9, Tests: ${totalTests}`
    );
  });
});
