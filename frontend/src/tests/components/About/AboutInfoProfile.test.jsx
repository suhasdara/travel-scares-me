import React from "react";
import { render as r } from "@testing-library/react";
import AboutInfoProfile from "components/About/AboutInfoProfile";

const defaultProps = {
  profile: {
    image: "profile-image",
    name: "Guido van Rossum",
    role: "coder god",
    about: "made python",
    commits: 2,
    issues: 3,
    tests: 0,
    linkedIn: "doesn't need one",
  },
};

const render = (props = {}) =>
  r(<AboutInfoProfile {...defaultProps} {...props} />);

describe("AboutInfoProfile", () => {
  it("renders profile data", () => {
    const { getByRole, getByText } = render();

    expect(getByRole("img")).toHaveAttribute("src", "profile-image");
    expect(getByText("Guido van Rossum")).toBeInTheDocument();
    expect(getByText("coder god")).toBeInTheDocument();
    expect(getByText("made python")).toBeInTheDocument();
    expect(getByText(/Commits/).textContent).toEqual("Commits: 2");
    expect(getByText(/Issues/).textContent).toEqual("Issues: 3");
    expect(getByText(/Tests/).textContent).toEqual("Tests: 0");
    expect(getByText(/LinkedIn/)).toHaveAttribute("href", "doesn't need one");
  });
});
