import { getStatsData } from "components/About/AboutStats";

const commitPages = [
  [{ author_name: "Ronald McDonald" }, { author_name: "Wendy" }],
  [{ author_name: "Ronald McDonald" }],
];

const issuePages = [[{}, { closed_by: { name: "Dave Thomas" } }]];

const responseMap = {
  "https://gitlab.com/api/v4/projects/24747543/repository/commits?page=1":
    commitPages[0],
  "https://gitlab.com/api/v4/projects/24747543/repository/commits?page=2":
    commitPages[1],
  "https://gitlab.com/api/v4/projects/24747543/issues?page=1": issuePages[0],
};

const mockFetch = jest.fn();

beforeEach(() => {
  fetch = mockFetch;
  mockFetch.mockClear();

  mockFetch.mockImplementation((url) =>
    Promise.resolve({
      json: () => Promise.resolve(url in responseMap ? responseMap[url] : []),
    })
  );
});

describe("AboutStats", () => {
  it("calculates number of issues and commits per person", async () => {
    const stats = await getStatsData();
    expect(stats).toEqual({
      commits: { "Ronald McDonald": 2, Wendy: 1 },
      issues: { "Dave Thomas": 1 },
    });
  });
});
