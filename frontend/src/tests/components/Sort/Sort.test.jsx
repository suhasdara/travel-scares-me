import React from "react";
import { render as r } from "@testing-library/react";
import Sort from "components/Sort/Sort";
import { act } from "react-dom/test-utils";

const onOrderByChange = jest.fn();
const onAscendingChange = jest.fn();
const defaultProps = {
  columns: ["col1", "col2"],
  orderBy: "col2",
  onOrderByChange,
  ascending: true,
  onAscendingChange,
};
const render = (props = {}) => r(<Sort {...defaultProps} {...props} />);

describe("Sort", () => {
  it("shows the current column", () => {
    const { getByText } = render();

    expect(getByText(/Sort By:.*col2/i)).toBeInTheDocument();
  });

  it("shows the correct arrow direction", () => {
    const { getByTitle } = render();

    expect(
      getByTitle(/Toggle Sort Order/i).querySelector(".fa-arrow-up")
    ).toBeInTheDocument();
  });

  describe("when the sort dropdown is clicked", () => {
    it("shows the columns to sort by", () => {
      const { getByText } = render();

      getByText(/Sort By:.*col2/i).click();
      expect(getByText("col1")).toBeInTheDocument();
      expect(getByText("col2")).toBeInTheDocument();
    });
  });

  describe("when a new sort column is selected", () => {
    it("requests the new sort column", async () => {
      const { getByText } = render();

      getByText(/Sort By:.*col2/i).click();
      await act(async () => await getByText("col1").click());
      expect(onOrderByChange).lastCalledWith("col1");
    });
  });

  describe("when the sorting order is toggled", () => {
    it("requests the opposite sorting order of the current one", () => {
      const { getByTitle } = render();

      getByTitle(/Toggle Sort Order/i).click();
      expect(onAscendingChange).lastCalledWith(false);
    });
  });
});
