import React from "react";
import userEvent from "@testing-library/user-event";
import * as reactQuery from "react-query";
import { renderWithRouter as r } from "tests/utils";
import Articles from "components/Article/Articles";

const mockUseQuery = jest.fn();

const getArticleData = (data = {}) => ({
  url_to_image: "https://istheshipstillstuck.com",
  title: "ship is too big",
  category: "how",
  published: "2021-03-13T12:00:00Z",
  article_language: "Arabic",
  country: "Egypt",
  id: 1,
  ...data,
});

const defaultProps = {};

const render = (props = {}) => r(<Articles {...defaultProps} {...props} />);

beforeEach(() => {
  reactQuery.useQuery = mockUseQuery;
});

describe("Articles", () => {
  it("displays title", () => {
    mockUseQuery.mockReturnValue({});
    const { getByText } = render();

    expect(getByText(/Articles/)).toBeInTheDocument();
  });

  describe("when data is loading", () => {
    it("displays spinner", () => {
      mockUseQuery.mockReturnValue({ isLoading: true });
      const { container } = render();

      const html = container.innerHTML;
      expect(/spinner/.test(html)).toEqual(true);
    });
  });

  it("calls useQuery with the key and function", () => {
    mockUseQuery.mockReturnValue({});
    render();

    expect(mockUseQuery).lastCalledWith(
      ["articles", 1, expect.any(Number), expect.any(Object)],
      expect.any(Function)
    );
  });

  describe("when first page data is loaded", () => {
    const firstPageData = {
      isLoading: false,
      data: {
        count: 3,
        articles: [
          getArticleData(),
          getArticleData({ title: "it's floating", id: 2 }),
          getArticleData({ title: "it's still stuck", id: 3 }),
        ],
      },
    };

    it("displays article previews", () => {
      mockUseQuery.mockReturnValue(firstPageData);
      const { getByText } = render();

      expect(getByText(/ship is too big/)).toBeInTheDocument();
      expect(getByText(/it's floating/)).toBeInTheDocument();
      expect(getByText(/it's still stuck/)).toBeInTheDocument();
    });
  });

  describe("when next page is clicked", () => {
    it("requests next page of data", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: { count: 100, articles: [getArticleData()] },
      });
      const { getAllByText } = render();

      expect(mockUseQuery).lastCalledWith(
        ["articles", 1, expect.any(Number), expect.any(Object)],
        expect.any(Function)
      );

      getAllByText(/Next/)[0].click();

      expect(mockUseQuery).lastCalledWith(
        ["articles", 2, expect.any(Number), expect.any(Object)],
        expect.any(Function)
      );
    });
  });

  describe("when filter is applied", () => {
    it("requests new data using filter", () => {
      mockUseQuery.mockReturnValue({});
      const { getByText } = render();

      getByText(/Change Filters/).click();
      userEvent.click(
        getByText(/Country/)
          .closest(".row")
          .querySelector(".dropdown-toggle")
      );
      getByText(/Argentina/).click();
      getByText(/Apply/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            country: "Argentina",
          }),
        ]),
        expect.any(Function)
      );
    });
  });

  describe("when sorting is applied", () => {
    it("requests new data using sorting", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: { articles: [getArticleData()], count: 100 },
      });
      const { getAllByText, getByText, getByTitle } = render();

      getByText(/Sort by/i).click();
      getAllByText(/Country/)[0].click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            orderBy: "country",
            ordering: "desc",
          }),
        ]),
        expect.any(Function)
      );
      getByTitle(/Toggle Sort Order/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            orderBy: "country",
            ordering: "asc",
          }),
        ]),
        expect.any(Function)
      );
    });
  });

  describe("when a search is entered", () => {
    it("requests new data with search query", () => {
      mockUseQuery.mockReturnValue({});
      const { getByPlaceholderText } = render();

      const searchInput = getByPlaceholderText(/Search/);
      userEvent.type(searchInput, "Test Search{enter}");
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            q: "Test Search",
          }),
        ]),
        expect.any(Function)
      );
    });
  });
});
