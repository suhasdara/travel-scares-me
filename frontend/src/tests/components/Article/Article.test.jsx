import React from "react";
import { renderWithRouter as r } from "tests/utils";
import Article from "components/Article/Article";
import * as reactQuery from "react-query";

const mockUseQuery = jest.fn();

const defaultProps = { articleId: 1 };

const articleData = {
  url_to_image: "https://istheshipstillstuck.com",
  title: "ship is too big",
  category: "how",
  published: "2021-03-13T12:00:00Z",
  article_language: "Arabic",
  country: "Egypt",
  author: "evergreen",
  country_id: 1,
  country_code: "EG",
  description: "ship is block",
  url: "https://istheshipstillstuck.com",
  source_logo_url:
    "https://i.internethaber.com/storage/files/images/2021/03/29/evergreen-NswQ_cover.jpg",
  source: "literally every news outlet",
  source_description: "the internet",
  url_to_source: "https://reddit.com",
  related_routes: [{ id: 1, source: "USA" }],
  id: 1,
};

const render = (props = {}) => r(<Article {...defaultProps} {...props} />);

beforeEach(() => {
  reactQuery.useQuery = mockUseQuery;
});

describe("Article", () => {
  describe("when data is loading", () => {
    it("shows the page spinner", () => {
      mockUseQuery.mockReturnValue({ isLoading: true });
      const { container } = render();

      const html = container.innerHTML;
      expect(/spinner/.test(html)).toEqual(true);
    });
  });

  describe("when the data has loaded", () => {
    it("shows article data", () => {
      mockUseQuery.mockReturnValue({ isLoading: false, data: articleData });
      const { getByAltText, getByText } = render();

      expect(getByText("ship is too big")).toBeInTheDocument();
      expect(getByText("evergreen")).toBeInTheDocument();
      expect(getByText("how")).toBeInTheDocument();
      expect(getByText("Egypt (EG)")).toHaveAttribute("href", "/countries/1");
      expect(getByText("Arabic")).toBeInTheDocument();
      expect(
        getByText("Sat Mar 13 2021 06:00:00 GMT-0600 (Central Standard Time)")
      ).toBeInTheDocument();
      expect(getByText("ship is block")).toBeInTheDocument();
      expect(getByText(/View article/)).toHaveAttribute(
        "href",
        "https://istheshipstillstuck.com"
      );
      expect(getByAltText("literally every news outlet")).toHaveAttribute(
        "src",
        "https://i.internethaber.com/storage/files/images/2021/03/29/evergreen-NswQ_cover.jpg"
      );
      expect(getByText(/literally every news outlet/)).toBeInTheDocument();
      expect(getByText("https://reddit.com")).toBeInTheDocument();
      expect(getByText(/Flights to Egypt/)).toBeInTheDocument();
      expect(getByText(/USA/)).toHaveAttribute("href", "/routes/1");
    });
  });
});
