import React from "react";
import { renderWithRouter as r } from "tests/utils";
import ArticlePreview from "components/Article/ArticlePreview";

const articleData = {
  url_to_image: "https://istheshipstillstuck.com",
  title: "ship is too big",
  category: "how",
  published: "2021-03-13T12:00:00Z",
  article_language: "Arabic",
  country: "Egypt",
  id: 1,
};

const defaultProps = {
  articleData,
};

const render = (props = {}) =>
  r(<ArticlePreview {...defaultProps} {...props} />);

describe("ArticlePreview", () => {
  it("displays the article data", () => {
    const { container, getByRole, getByText } = render();

    const text = container.textContent;
    expect(/Category: how/.test(text)).toEqual(true);
    expect(/Published: Sat Mar 13 2021 6:00:00 AM/.test(text)).toEqual(true);
    expect(getByRole("img")).toHaveAttribute(
      "src",
      "https://istheshipstillstuck.com"
    );
    expect(getByRole("link")).toHaveAttribute("href", "/articles/1");
    expect(/Language: Arabic/.test(text)).toEqual(true);
    expect(getByText("ship is too big")).toBeInTheDocument();
    expect(/Country: Egypt/.test(text)).toEqual(true);
  });
});
