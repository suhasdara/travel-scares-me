import React from "react";
import { renderWithRouter as r } from "tests/utils";
import Country from "components/Country/Country";
import * as reactQuery from "react-query";

const mockUseQuery = jest.fn();

const defaultProps = { countryId: 1 };
const getCountryData = (data = {}) => ({
  capital: "D.C.",
  code: "US",
  covid: {
    cases: 100,
    deaths: 120,
    percent_covid_symptoms: 2.5,
    percent_vaccinated: 6.0,
    percent_mask: 4.0,
  },
  currencies: ["Dollar"],
  flag_url: "https://example.com",
  id: 1,
  latitude: 15,
  languages: ["English", "Spanish"],
  longitude: -5,
  name: "United States",
  neighbors: [
    { id: 2, name: "Canada" },
    { id: 3, name: "Mexico" },
  ],
  maps_url: "https://example.com",
  population: 5000,
  region: "region",
  related_articles: [{ id: 1, title: "an article" }],
  related_routes: [{ id: 4, destination: "South Korea" }],
  threat_level: 3,
  timezones: ["UTC-07:00", "UTC-06:00"],
  travel_advisories: "<p>Don't travel</p>",
  ...data,
});

const render = (props = {}) => r(<Country {...defaultProps} {...props} />);

beforeEach(() => {
  reactQuery.useQuery = mockUseQuery;
});

describe("Country", () => {
  describe("when data is loading", () => {
    it("shows the page spinner", () => {
      mockUseQuery.mockReturnValue({ isLoading: true });
      const { container } = render();

      const html = container.innerHTML;
      expect(/spinner/.test(html)).toEqual(true);
    });
  });

  describe("when the data has loaded", () => {
    it("shows country data", () => {
      const countryData = getCountryData();
      mockUseQuery.mockReturnValue({ isLoading: false, data: countryData });
      const { getByText } = render();

      expect(getByText("United States")).toBeInTheDocument();
      expect(getByText(/Country Code/).parentElement.textContent).toEqual(
        "Country Code: US"
      );
      expect(getByText(/Capital/).parentElement.textContent).toEqual(
        "Capital: D.C."
      );
      expect(getByText(/Coordinates/).parentElement.textContent).toEqual(
        "Coordinates: 15°, -5°"
      );
      expect(getByText(/Population/).parentElement.textContent).toEqual(
        "Population: 5,000"
      );
      expect(getByText(/Travel advisories.*/).textContent).toEqual(
        "Travel advisories (threat level 3):"
      );
      expect(getByText(/Region/).parentElement.textContent).toEqual(
        "Region: region"
      );
      expect(getByText(/Languages/).parentElement.textContent).toEqual(
        "Languages spoken: English, Spanish"
      );
      expect(getByText(/Currencies/).parentElement.textContent).toEqual(
        "Currencies used: Dollar"
      );
      expect(getByText(/Timezones/).parentElement.textContent).toEqual(
        "Timezones: UTC-07:00, UTC-06:00"
      );
    });

    it("displays links to neighboring countries", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: getCountryData(),
      });
      const { getByText } = render();

      expect(getByText("Canada")).toHaveAttribute("href", "/countries/2");
      expect(getByText("Mexico")).toHaveAttribute("href", "/countries/3");
    });

    it("displays links to related news", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: getCountryData(),
      });
      const { getByText } = render();

      expect(getByText(/an article/)).toHaveAttribute("href", "/articles/1");
    });

    it("displays links to airplane routes to destinations", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: getCountryData(),
      });
      const { getByText } = render();

      expect(getByText(/South Korea/)).toHaveAttribute("href", "/routes/4");
    });

    it("displays travel advisories", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: getCountryData(),
      });
      const { getByText } = render();

      expect(getByText(/Don't travel/)).toBeInTheDocument();
    });

    it("displays flag", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: getCountryData(),
      });
      const { getByRole } = render();

      expect(getByRole("img")).toHaveAttribute("src", "https://example.com");
    });
  });
});
