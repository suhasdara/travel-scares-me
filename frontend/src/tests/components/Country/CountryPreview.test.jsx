import React from "react";
import { renderWithRouter as r } from "tests/utils";
import CountryPreview from "components/Country/CountryPreview";

const countryData = {
  capital: "Lima",
  code: "PE",
  flag_url: "https://example.com",
  id: 2,
  latitude: 20,
  longitude: -10,
  name: "Peru",
  population: 1000,
  region: "PE",
};

const defaultProps = {
  countryData,
};

const render = (props = {}) =>
  r(<CountryPreview {...defaultProps} {...props} />);

describe("CountryPreview", () => {
  it("displays the country data", () => {
    const { container, getByRole, getByText } = render();

    const text = container.textContent;
    expect(/Capital: Lima/.test(text)).toEqual(true);
    expect(/Country Code: PE/.test(text)).toEqual(true);
    expect(getByRole("img")).toHaveAttribute("src", "https://example.com");
    expect(getByRole("link")).toHaveAttribute("href", "/countries/2");
    expect(/Coordinates: 20°, -10°/.test(text)).toEqual(true);
    expect(getByText("Peru")).toBeInTheDocument();
    expect(/Population: 1,000/.test(text)).toEqual(true);
    expect(/Region: PE/.test(text)).toEqual(true);
  });
});
