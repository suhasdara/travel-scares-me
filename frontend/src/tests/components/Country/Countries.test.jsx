import React from "react";
import userEvent from "@testing-library/user-event";
import * as reactQuery from "react-query";
import { renderWithRouter as r } from "tests/utils";
import Countries from "components/Country/Countries";

const mockUseQuery = jest.fn();

const getCountryData = (data = {}) => ({
  capital: "D.C.",
  code: "US",
  flag_url: "https://example.com",
  id: 1,
  latitude: 10,
  longitude: -5,
  name: "United States",
  population: 5000,
  region: "US",
  ...data,
});

const defaultProps = {};

const render = (props = {}) => r(<Countries {...defaultProps} {...props} />);

beforeEach(() => {
  reactQuery.useQuery = mockUseQuery;
});

describe("Countries", () => {
  it("displays title", () => {
    mockUseQuery.mockReturnValue({});
    const { getByText } = render();

    expect(getByText(/Countries/)).toBeInTheDocument();
  });

  describe("when data is loading", () => {
    it("displays spinner", () => {
      mockUseQuery.mockReturnValue({ isLoading: true });
      const { container } = render();

      const html = container.innerHTML;
      expect(/spinner/.test(html)).toEqual(true);
    });
  });

  it("calls useQuery with the key and function", () => {
    mockUseQuery.mockReturnValue({});
    render();

    expect(mockUseQuery).lastCalledWith(
      ["countries", 1, expect.any(Number), expect.any(Object)],
      expect.any(Function)
    );
  });

  describe("when first page data is loaded", () => {
    const firstPageData = {
      isLoading: false,
      data: {
        count: 3,
        countries: [
          getCountryData(),
          getCountryData({ name: "Peru", code: "PE" }),
          getCountryData({ name: "Canada", code: "CA" }),
        ],
      },
    };

    it("displays country previews", () => {
      mockUseQuery.mockReturnValue(firstPageData);
      const { getByText } = render();

      expect(getByText(/United States/)).toBeInTheDocument();
      expect(getByText(/Peru/)).toBeInTheDocument();
      expect(getByText(/Canada/)).toBeInTheDocument();
    });
  });

  describe("when next page is clicked", () => {
    it("requests next page of data", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: { count: 100, countries: [getCountryData()] },
      });
      const { getAllByText } = render();

      expect(mockUseQuery).lastCalledWith(
        ["countries", 1, expect.any(Number), expect.any(Object)],
        expect.any(Function)
      );

      getAllByText(/Next/)[0].click();

      expect(mockUseQuery).lastCalledWith(
        [expect.any(String), 2, expect.any(Number), expect.any(Object)],
        expect.any(Function)
      );
    });
  });

  describe("when filter is applied", () => {
    it("requests new data using filter", () => {
      mockUseQuery.mockReturnValue({});
      const { getByText } = render();

      getByText(/Change Filters/).click();
      userEvent.click(
        getByText(/Region/)
          .closest(".row")
          .querySelector(".dropdown-toggle")
      );
      getByText(/Africa/).click();
      getByText(/Apply/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            region: "Africa",
          }),
        ]),
        expect.any(Function)
      );
    });
  });

  describe("when sorting is applied", () => {
    it("requests new data using sorting", () => {
      mockUseQuery.mockReturnValue({
        isLoading: false,
        data: { countries: [getCountryData()], count: 100 },
      });
      const { getAllByText, getByText, getByTitle } = render();

      getByText(/Sort by/i).click();
      getAllByText(/Region/)[0].click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            orderBy: "region",
            ordering: "asc",
          }),
        ]),
        expect.any(Function)
      );
      getByTitle(/Toggle Sort Order/).click();
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            orderBy: "region",
            ordering: "desc",
          }),
        ]),
        expect.any(Function)
      );
    });
  });

  describe("when a search is entered", () => {
    it("requests new data with search query", () => {
      mockUseQuery.mockReturnValue({});
      const { getByPlaceholderText } = render();

      const searchInput = getByPlaceholderText(/Search/);
      userEvent.type(searchInput, "Test Search{enter}");
      expect(mockUseQuery).lastCalledWith(
        expect.arrayContaining([
          expect.objectContaining({
            q: "Test Search",
          }),
        ]),
        expect.any(Function)
      );
    });
  });
});
