import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL + "routes/7"


class TestRouteGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_route_source(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("SAS Flight 1885"))

        sweden = self.browser.links.find_by_text("Sweden")
        sweden.click()
        self.assertTrue(self.browser.url == MAIN_URL + "countries/240")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("SAS Flight 1885"))

    def test_route_dest(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("SAS Flight 1885"))

        france = self.browser.links.find_by_text("France")
        france.click()
        self.assertTrue(self.browser.url == MAIN_URL + "countries/62")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("SAS Flight 1885"))

    def test_route_news(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("SAS Flight 1885"))

        news = self.browser.links.find_by_text(
            "La liste complète des 18 centres commerciaux et grands magasins du Var fermés à cause de l'épidémie de Covid-19 - Var-Matin"
        )
        news.click()
        self.assertTrue(self.browser.url == MAIN_URL + "articles/649")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("SAS Flight 1885"))


if __name__ == "__main__":
    unittest.main()
