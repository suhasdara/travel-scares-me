import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL


class TestNavbarGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    # Check if correct link is active
    def check_links(self, active):
        links = ["About", "Airplane Routes", "Countries", "Articles"]
        for link in links:
            button = self.browser.links.find_by_text(link)
            if link == active:
                self.assertTrue(button.has_class("nav-link active"))
            else:
                self.assertTrue(button.has_class("nav-link"))

    def test_navbar_whole(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.check_links("")

        self.browser.links.find_by_text("About").click()
        self.assertTrue(self.browser.url == URL + "about")
        self.check_links("About")

        self.browser.links.find_by_text("Airplane Routes").click()
        self.assertTrue(self.browser.url == URL + "routes")
        self.check_links("Airplane Routes")

        self.browser.links.find_by_text("Countries").click()
        self.assertTrue(self.browser.url == URL + "countries")
        self.check_links("Countries")

        self.browser.links.find_by_text("Articles").click()
        self.assertTrue(self.browser.url == URL + "articles")
        self.check_links("Articles")

        self.browser.links.find_by_href("/").click()
        self.assertTrue(self.browser.url == URL)
        self.check_links("")

    # Test each navbar link
    def test_about(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.check_links("")

        self.browser.links.find_by_text("About").click()
        self.assertTrue(self.browser.url == URL + "about")
        self.check_links("About")

    def test_routes(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.check_links("")

        self.browser.links.find_by_text("Airplane Routes").click()
        self.assertTrue(self.browser.url == URL + "routes")
        self.check_links("Airplane Routes")

    def test_countries(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.check_links("")

        self.browser.links.find_by_text("Countries").click()
        self.assertTrue(self.browser.url == URL + "countries")
        self.check_links("Countries")

    def test_articles(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.check_links("")

        self.browser.links.find_by_text("Articles").click()
        self.assertTrue(self.browser.url == URL + "articles")
        self.check_links("Articles")

    def test_home(self):
        self.browser.visit(URL + "about")
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.check_links("about")

        self.browser.links.find_by_href("/").click()
        self.assertTrue(self.browser.url == URL)
        self.check_links("")

    def test_search(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.check_links("")

        self.browser.fill("Global Search", "Paris\n")

        # Check URL
        self.assertTrue(self.browser.url == URL + "search/q=Paris")
        self.assertTrue(self.browser.is_text_present("Search Results for Paris"))
        # Countries
        self.assertTrue(self.browser.is_text_present("France"))
        # Articles
        self.assertTrue(
            self.browser.is_text_present(
                "Incidence, évacuations, réanimations : où en est la situation sanitaire en Île-de-France ? - Le Parisien"
            )
        )
        # Routes
        self.assertTrue(self.browser.is_text_present("0B 4096"))


if __name__ == "__main__":
    unittest.main()
