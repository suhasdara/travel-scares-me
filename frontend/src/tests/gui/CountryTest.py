import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL + "countries/6"


class TestCountryGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_country_neighbor_1(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

        canada = self.browser.links.find_by_text("Canada")
        canada.first.click()
        self.assertTrue(self.browser.url == MAIN_URL + "countries/188")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

    def test_country_neighbor_2(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

        mexico = self.browser.links.find_by_text("Mexico")
        mexico.first.click()
        self.assertTrue(self.browser.url == MAIN_URL + "countries/124")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

    def test_country_news(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

        news = self.browser.links.find_by_text(
            "COVID-19 Statistics | March 12, 2021 - Lost Coast Outpost"
        )
        news.click()
        self.assertTrue(self.browser.url == MAIN_URL + "articles/93")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

    def test_country_route(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )

        route = self.browser.links.find_by_text("Finland")
        route.click()
        self.assertTrue(self.browser.url == MAIN_URL + "routes/575")
        self.browser.back()
        self.assertTrue(self.browser.is_text_present("United States of America"))
        self.assertTrue(
            self.browser.is_text_present("Flights from United States of America:")
        )


if __name__ == "__main__":
    unittest.main()
