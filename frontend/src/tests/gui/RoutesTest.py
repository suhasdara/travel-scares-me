import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL + "routes/"


class TestRoutesGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_routes_table(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        table = self.browser.find_by_tag("table")
        self.assertIsNotNone(table)

        # Check number of columns
        tcols = self.browser.find_by_xpath("//table/thead/tr/th")
        self.assertIsNotNone(tcols)
        colscount = 0
        col_text = [
            "Flight number",
            "Departure city",
            "Departure country",
            "Arrival city",
            "Arrival country",
        ]
        for col in tcols:
            self.assertEqual(col.text, col_text[colscount])
            colscount += 1
        self.assertEqual(colscount, 5)

        # Check number of rows
        # defaults to 40 per table on reload
        trows = self.browser.find_by_xpath("//table/tbody/tr")
        self.assertIsNotNone(trows)
        rowscount = 0
        for row in trows:
            rowscount += 1
        self.assertEqual(rowscount, 40)

    def test_routes_next(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        # Go to next page
        self.browser.find_by_text("Next ").click()
        self.assertTrue(self.browser.is_text_present("2D 302"))

    def test_routes_last(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        # Go to last page
        self.browser.find_by_text("Last ").click()
        self.assertTrue(self.browser.is_text_present("ZT 2291"))

    def test_routes_prev(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        # Go to last page
        self.browser.find_by_text("Last ").click()
        self.assertTrue(self.browser.is_text_present("ZT 2291"))

        # Go to previous page
        self.browser.find_by_text(" Prev").click()
        self.assertTrue(self.browser.is_text_present("ZI 274"))

    def test_routes_first(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        # Go to next page
        self.browser.find_by_text("Next ").click()
        self.assertTrue(self.browser.is_text_present("2D 302"))

        # Go to first page
        self.browser.find_by_text(" First").click()
        self.assertTrue(self.browser.is_text_present("0B 108"))

    def test_routes_goto(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        button = None
        # TODO: replace with name search when updated
        buttons = self.browser.find_by_tag("button")
        for b in buttons:
            if b.has_class("mr-1 dropdown-toggle btn btn-dark"):
                button = b
                break

        self.assertIsNotNone(button)
        button.click()

        # get the drop-down menu
        all_divs = self.browser.find_by_tag("div")
        divs = []
        for d in all_divs:
            if d.has_class("dropdown-item"):
                divs.append(d)

        self.assertGreater(len(divs), 0)

        # Page 4, since 1-indexed
        divs[3].click()
        self.assertTrue(self.browser.is_text_present("3O 122"))

    def test_routes_per_page(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        for i in range(4):
            # TODO: replace with name search when updated
            drop_buttons = []
            buttons = self.browser.find_by_tag("button")
            for b in buttons:
                if b.has_class("ml-1 dropdown-toggle btn btn-dark"):
                    drop_buttons.append(b)
            self.assertTrue(len(drop_buttons) > 0)

            button = drop_buttons[-1]
            self.assertIsNotNone(button)
            button.click()

            # get the drop-down menu
            all_divs = self.browser.find_by_tag("div")
            divs = []
            for d in all_divs:
                if d.has_class("dropdown-item"):
                    divs.append(d)

            self.assertGreater(len(divs), 0)

            divs[i].click()
            # Test 1st, 9th, 21st, and 41st elements to guarantee new ones are showing
            self.assertTrue(self.browser.is_text_present("0B 108"))  # 1st
            self.assertEqual(self.browser.is_text_present("0B 156"), i > 0)  # 9th
            self.assertEqual(self.browser.is_text_present("0B 410"), i > 1)  # 21st
            self.assertEqual(self.browser.is_text_present("2D 302"), i > 2)  # 41st

    def test_routes_click(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        trows = self.browser.find_by_xpath("//table/tbody/tr")
        self.assertIsNotNone(trows)
        row = trows.first
        self.assertIsNotNone(row)

        row.click()
        self.assertTrue(self.browser.url == URL + "7139")
        self.assertTrue(self.browser.is_text_present("Blue Air Flight 108"))

    def test_routes_sorting(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        col_text = [
            "Flight number",
            "Departure city",
            "Departure country",
            "Arrival city",
            "Arrival country",
        ]

        def get_col(i):
            table_headers = []
            headers = self.browser.find_by_tag("th")
            for h in headers:
                if h.has_class("cursor-pointer sort-hover"):
                    table_headers.append(h)
            return table_headers[i]

        # Must get headers each time, since they change after one is clicked
        for i in range(5):
            # First click: Ascending (except i == 0, which becomes descending)
            header = get_col(i)
            header.click()
            if i == 0:
                self.assertTrue(self.browser.is_text_present("ZX 7305"))
            elif i == 1:
                self.assertTrue(self.browser.is_text_present("SK 7281"))
            elif i == 2:
                self.assertTrue(self.browser.is_text_present("EP 567"))
            elif i == 3:
                self.assertTrue(self.browser.is_text_present("SK 7754"))
            elif i == 4:
                self.assertTrue(self.browser.is_text_present("FG 332"))
            else:
                self.assertTrue(False)

            # Second click: Descending (except i == 0, which becomes ascending again)
            header = get_col(i)
            header.click()
            if i == 0:
                self.assertTrue(self.browser.is_text_present("0B 108"))
            elif i == 1:
                self.assertTrue(self.browser.is_text_present("WK 1412"))
            elif i == 2:
                self.assertTrue(self.browser.is_text_present("SA 41"))
            elif i == 3:
                self.assertTrue(self.browser.is_text_present("2L 1247"))
            elif i == 4:
                self.assertTrue(self.browser.is_text_present("DT 587"))
            else:
                self.assertTrue(False)

    def test_routes_filter(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        self.browser.find_by_text(" Change Filters").click()
        self.assertTrue(self.browser.is_text_present("Airline"))

        # TODO: Add fill to parameters
        self.browser.fill("Airline", "AA\n")
        self.browser.fill("Arrival City", "Guate\n")
        self.browser.fill("Arrival Country", "Guate\n")
        self.browser.fill("Departure City", "Dall\n")

        self.browser.find_by_text("Apply").click()
        self.assertTrue(self.browser.is_text_present("AA 1013"))

    def test_routes_search(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Airplane Routes"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("0B 108"))

        self.browser.fill("Airplane Routes Search", "Austin\n")
        self.assertTrue(self.browser.is_text_present("5Y 8087"))


if __name__ == "__main__":
    unittest.main()
