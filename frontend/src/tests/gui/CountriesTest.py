import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL + "countries/"


class TestCountriesGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_countries_grid(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        # TODO: replace with name search when updated
        divs = self.browser.find_by_tag("div")
        deck = None
        for d in divs:
            if d.has_class("card-deck"):
                deck = d
                break
        self.assertIsNotNone(deck)

        # Check number of cards
        cards = 0
        for d in divs:
            if d.has_class("card"):
                cards += 1
        self.assertTrue(cards == 20)

    def test_countries_card(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first flight
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        divs = self.browser.find_by_tag("div")
        # Find first card
        card = None
        for d in divs:
            if d.has_class("card"):
                card = d
                break
        self.assertIsNotNone(card)

        txt = "Afghanistan\nCountry Code: AF\nRegion: Asia\nCoordinates: 33°, 65°\nPopulation: 27,657,145\nCapital: Kabul"
        self.assertEqual(card.text, txt)

    def test_countries_next(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        # Go to next page
        self.browser.find_by_text("Next ").click()
        self.assertTrue(self.browser.is_text_present("Belarus"))

    def test_countries_last(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        # Go to last page
        self.browser.find_by_text("Last ").click()
        self.assertTrue(self.browser.is_text_present("Vanuatu"))

    def test_countries_prev(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        # Go to last page
        self.browser.find_by_text("Last ").click()
        self.assertTrue(self.browser.is_text_present("Vanuatu"))

        # Go to previous page
        self.browser.find_by_text(" Prev").click()
        self.assertTrue(self.browser.is_text_present("Thailand"))

    def test_countries_first(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        # Go to next page
        self.browser.find_by_text("Next ").click()
        self.assertTrue(self.browser.is_text_present("Belarus"))

        # Go to first page
        self.browser.find_by_text(" First").click()
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

    def test_countries_goto(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        button = None
        # TODO: replace with name search when updated
        buttons = self.browser.find_by_tag("button")
        for b in buttons:
            if b.has_class("mr-1 dropdown-toggle btn btn-dark"):
                button = b
                break

        self.assertIsNotNone(button)
        button.click()

        # get the drop-down menu
        all_divs = self.browser.find_by_tag("div")
        divs = []
        for d in all_divs:
            if d.has_class("dropdown-item"):
                divs.append(d)

        self.assertGreater(len(divs), 0)

        # Page 4, since 1-indexed
        divs[3].click()
        self.assertTrue(self.browser.is_text_present("Denmark"))

    def test_countries_per_page(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        for i in range(4):
            # TODO: replace with name search when updated
            drop_buttons = []
            buttons = self.browser.find_by_tag("button")
            for b in buttons:
                if b.has_class("ml-1 dropdown-toggle btn btn-dark"):
                    drop_buttons.append(b)
            self.assertTrue(len(drop_buttons) > 0)

            button = drop_buttons[-1]
            self.assertIsNotNone(button)
            button.click()

            # get the drop-down menu
            all_divs = self.browser.find_by_tag("div")
            divs = []
            for d in all_divs:
                if d.has_class("dropdown-item"):
                    divs.append(d)

            self.assertGreater(len(divs), 0)

            divs[i].click()
            # Test 1st, 9th, 21st, and 41st elements to guarantee new ones are showing
            self.assertTrue(self.browser.is_text_present("Afghanistan"))  # 1st
            self.assertEqual(self.browser.is_text_present("Antarctica"), i > 0)  # 9th
            self.assertEqual(self.browser.is_text_present("Belarus"), i > 1)  # 21st
            self.assertEqual(self.browser.is_text_present("Canada"), i > 2)  # 41st

    def test_countries_click(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        # TODO: more elegant way of selecting which link to click (currently just do first)
        divs = self.browser.find_by_tag("div")
        # Find first card
        card = None
        for d in divs:
            if d.has_class("card"):
                card = d
                break
        self.assertIsNotNone(card)

        txt = "Afghanistan\nCountry Code: AF\nRegion: Asia\nCoordinates: 33°, 65°\nPopulation: 27,657,145\nCapital: Kabul"
        self.assertEqual(card.text, txt)

        card.click()
        self.assertTrue(self.browser.url == URL + "31")
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

    def test_countries_sorting(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        self.browser.find_by_text("Sort By: ").click()
        self.browser.find_by_text("Region").click()

        self.assertTrue(self.browser.is_text_present("Mayotte"))

    def test_countries_filter(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        self.browser.find_by_text(" Change Filters").click()
        self.assertTrue(self.browser.is_text_present("Min:"))

        # TODO: Add fill to parameters
        self.browser.fill("Population Min", "0")
        self.browser.fill("Population Max", "1000000")
        self.browser.fill("Latitude Min", "0")
        self.browser.fill("Latitude Max", "50")
        self.browser.fill("Longitude Min", "-50")
        self.browser.fill("Longitude Max", "0")
        self.browser.fill("Region", "Afr\n")

        self.browser.find_by_text("Apply").click()
        self.assertTrue(self.browser.is_text_present("Cabo Verde"))

    def test_countries_search(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Countries"))
        # To ensure first page, show first country
        self.assertTrue(self.browser.is_text_present("Afghanistan"))

        self.browser.fill("Countries Search", "Washington\n")
        self.assertTrue(self.browser.is_text_present("United States of America"))


if __name__ == "__main__":
    unittest.main()
