import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options, GitlabURL

URL = MAIN_URL


class TestHomeGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_countries(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.browser.links.find_by_text("View countries").click()
        self.assertTrue(self.browser.url == URL + "countries")

    def test_articles(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.browser.links.find_by_text("View news articles").click()
        self.assertTrue(self.browser.url == URL + "articles")

    def test_routes(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.browser.links.find_by_text("View airplane routes").click()
        self.assertTrue(self.browser.url == URL + "routes")

    def test_about(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.browser.links.find_by_text("About Us").click()
        self.assertTrue(self.browser.url == URL + "about")

    def test_repo(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present("Your solution for mitigating travel risk.")
        )
        self.browser.links.find_by_text("GitLab Repository").click()
        self.assertTrue(self.browser.url == GitlabURL)


if __name__ == "__main__":
    unittest.main()
