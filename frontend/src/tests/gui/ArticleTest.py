import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL + "articles/3"


class TestArticleGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_article_country(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

        canada = self.browser.links.find_by_text("South Africa")
        canada.first.click()
        self.assertTrue(self.browser.url == MAIN_URL + "countries/162")
        self.browser.back()
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

    def test_article_source(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

        source = self.browser.links.find_by_text(" View article")
        source.first.click()
        self.assertTrue(
            self.browser.url
            == "https://www.archaeology.org/news/9523-210315-antikythera-mechanism-model"
        )
        self.browser.back()
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

    def test_article_source_site(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

        source = self.browser.links.find_by_text("https://www.archaeology.org/")
        source.click()
        self.assertTrue(self.browser.url == "https://www.archaeology.org/")
        self.browser.back()
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

    def test_article_route(self):
        self.browser.visit(URL)
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))

        route = self.browser.links.find_by_text("Israel")
        route.click()
        self.assertTrue(self.browser.url == MAIN_URL + "routes/269")
        self.browser.back()
        self.assertTrue(
            self.browser.is_text_present(
                "Researchers Develop Digital Model of the Antikythera Mechanism - Archaeology"
            )
        )
        self.assertTrue(self.browser.is_text_present("About the article source:"))


if __name__ == "__main__":
    unittest.main()
