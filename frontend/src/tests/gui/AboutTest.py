import unittest
from splinter import Browser

from Globals import *

URL = MAIN_URL + "about/"


class TestAboutGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    # Test all Team Member Cards
    def test_about(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))

        divs = self.browser.find_by_tag("div")
        local_members = 0
        for div in divs:
            if div.has_class("about-card card"):
                local_members += 1
        self.assertEqual(local_members, MEMBERS)

    # Test all links on the About page
    def test_repo(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))

        self.browser.links.find_by_text("Gitlab repo").click()
        self.assertTrue(self.browser.url == GitlabURL)

    def test_postman(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Postman documentation").click()
        self.assertTrue(self.browser.url == PostmanURL)

    def test_countries(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_href(CountriesAPI).click()
        self.assertTrue(self.browser.url == CountriesAPI)

    def test_advisories(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Travel advisories").click()
        self.assertTrue(self.browser.url == AdvisoryAPI)

    def test_covid(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Covid data").click()
        self.assertTrue(self.browser.url == CovidAPI)

    def test_covid_behavior(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Covid behavior").click()
        self.assertTrue(self.browser.url == CovidBehaviorAPI)

    def test_routes(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Aviation routes").click()
        self.assertTrue(self.browser.url == RoutesAPI)

    def test_maps(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Google maps").click()
        self.assertTrue(self.browser.url == MapsAPI)

    def test_news(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("News").click()
        self.assertTrue(self.browser.url == NewsAPI)

    def test_serpapi(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Serpapi").click()
        self.assertTrue(self.browser.url == SerpapiAPI)

    def test_youtube(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Youtube data").click()
        self.assertTrue(self.browser.url == YoutubeAPI)

    def test_gitlab_data(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Meet the team:"))
        self.browser.links.find_by_text("Gitlab data").click()
        self.assertTrue(self.browser.url == GitlabAPI)


if __name__ == "__main__":
    unittest.main()
