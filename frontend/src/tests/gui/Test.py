import unittest

from AboutTest import TestAboutGui
from ArticlesTest import TestArticlesGui
from ArticleTest import TestArticleGui
from CountriesTest import TestCountriesGui
from CountryTest import TestCountryGui
from HomeTest import TestHomeGui
from NavbarTest import TestNavbarGui
from RoutesTest import TestRoutesGui
from RouteTest import TestRouteGui

if __name__ == "__main__":
    about_tests = unittest.TestLoader().loadTestsFromTestCase(TestAboutGui)
    articles_tests = unittest.TestLoader().loadTestsFromTestCase(TestArticlesGui)
    article_tests = unittest.TestLoader().loadTestsFromTestCase(TestArticleGui)
    countries_tests = unittest.TestLoader().loadTestsFromTestCase(TestCountriesGui)
    country_tests = unittest.TestLoader().loadTestsFromTestCase(TestCountryGui)
    home_tests = unittest.TestLoader().loadTestsFromTestCase(TestHomeGui)
    navbar_tests = unittest.TestLoader().loadTestsFromTestCase(TestNavbarGui)
    routes_tests = unittest.TestLoader().loadTestsFromTestCase(TestRoutesGui)
    route_tests = unittest.TestLoader().loadTestsFromTestCase(TestRouteGui)

    about_suite = unittest.TestSuite([about_tests])
    articles_suite = unittest.TestSuite([articles_tests, article_tests])
    countries_suite = unittest.TestSuite([countries_tests, country_tests])
    routes_suite = unittest.TestSuite([routes_tests, route_tests])
    splash_suite = unittest.TestSuite([home_tests, navbar_tests])

    suites = [about_suite, articles_suite, countries_suite, routes_suite, splash_suite]

    success = True

    for suite in suites:
        result = unittest.TextTestRunner(verbosity=2).run(suite)
        if not result.wasSuccessful():
            success = False

    if success:
        exit(0)
    else:
        exit(1)
