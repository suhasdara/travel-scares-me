import unittest
from splinter import Browser

from Globals import BROWSER, PATH, MAIN_URL, command_line_options

URL = MAIN_URL + "articles/"


class TestArticlesGui(unittest.TestCase):

    # Create a browser before all tests
    @classmethod
    def setUpClass(cls):
        cls.browser = Browser(
            BROWSER, executable_path=PATH, options=command_line_options
        )

    # Close browser after all tests complete
    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()

    def test_articles_grid(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first article
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        # TODO: replace with name search when updated
        divs = self.browser.find_by_tag("div")
        deck = None
        for d in divs:
            if d.has_class("card-deck"):
                deck = d
                break
        self.assertIsNotNone(deck)

        # Check number of cards
        cards = 0
        for d in divs:
            if d.has_class("card"):
                cards += 1
        self.assertTrue(cards == 20)

    def test_articles_card(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first article
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        divs = self.browser.find_by_tag("div")
        # Find first card
        card = None
        for d in divs:
            if d.has_class("card"):
                card = d
                break
        self.assertIsNotNone(card)

        txt = "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit\nCategory: Technology\nPublished: Tue Mar 16 2021 3:38:48 AM\nLanguage: English\nCountry: South Africa"
        self.assertEqual(card.text, txt)

    def test_articles_next(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        # Go to next page
        self.browser.find_by_text("Next ").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Is this the first modified Merc-AMG GT Black Series? - Top Gear"
            )
        )

    def test_articles_last(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        # Go to last page
        self.browser.find_by_text("Last ").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Strenger Frost an der Mosel: Winzer haben mit Eiswein-Lese begonnen - Luxemburger Wort"
            )
        )

    def test_articles_prev(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        # Go to last page
        self.browser.find_by_text("Last ").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Strenger Frost an der Mosel: Winzer haben mit Eiswein-Lese begonnen - Luxemburger Wort"
            )
        )

        # Go to previous page
        self.browser.find_by_text(" Prev").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Volcanoes Today - summary of volcanic activity world-wide - VolcanoDiscovery"
            )
        )

    def test_articles_first(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        # Go to next page
        self.browser.find_by_text("Next ").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Is this the first modified Merc-AMG GT Black Series? - Top Gear"
            )
        )

        # Go to first page
        self.browser.find_by_text(" First").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

    def test_articles_goto(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        button = None
        # TODO: replace with name search when updated
        buttons = self.browser.find_by_tag("button")
        for b in buttons:
            if b.has_class("mr-1 dropdown-toggle btn btn-dark"):
                button = b
                break

        self.assertIsNotNone(button)
        button.click()

        # get the drop-down menu
        all_divs = self.browser.find_by_tag("div")
        divs = []
        for d in all_divs:
            if d.has_class("dropdown-item"):
                divs.append(d)

        self.assertGreater(len(divs), 0)

        # Page 4, since 1-indexed
        divs[3].click()
        self.assertTrue(
            self.browser.is_text_present(
                "Tinder will add background-checking feature to screen dates for violent crimes, offenses - Fox Business"
            )
        )

    def test_articles_per_page(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        for i in range(4):
            # TODO: replace with name search when updated
            drop_buttons = []
            buttons = self.browser.find_by_tag("button")
            for b in buttons:
                if b.has_class("ml-1 dropdown-toggle btn btn-dark"):
                    drop_buttons.append(b)
            self.assertTrue(len(drop_buttons) > 0)

            button = drop_buttons[-1]
            self.assertIsNotNone(button)
            button.click()

            # get the drop-down menu
            all_divs = self.browser.find_by_tag("div")
            divs = []
            for d in all_divs:
                if d.has_class("dropdown-item"):
                    divs.append(d)

            self.assertGreater(len(divs), 0)

            divs[i].click()
            # Test 1st, 9th, 21st, and 41st elements to guarantee new ones are showing
            self.assertTrue(
                self.browser.is_text_present(
                    "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
                )
            )  # 1st
            self.assertEqual(
                self.browser.is_text_present(
                    "Microsoft rolls back update to fix access issues for thousands on Teams - TimesLIVE"
                ),
                i > 0,
            )  # 9th
            self.assertEqual(
                self.browser.is_text_present(
                    "Is this the first modified Merc-AMG GT Black Series? - Top Gear"
                ),
                i > 1,
            )  # 21st
            self.assertEqual(
                self.browser.is_text_present(
                    'Respawn Rolls Out Apex Legends Update, Includes "Several" Nintendo Switch Fixes - Nintendo Life'
                ),
                i > 2,
            )  # 41st

    def test_articles_click(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        divs = self.browser.find_by_tag("div")
        # Find first card
        card = None
        for d in divs:
            if d.has_class("card"):
                card = d
                break
        self.assertIsNotNone(card)

        txt = "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit\nCategory: Technology\nPublished: Tue Mar 16 2021 3:38:48 AM\nLanguage: English\nCountry: South Africa"
        self.assertEqual(card.text, txt)

        card.click()
        self.assertTrue(self.browser.url == URL + "5436")
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

    def test_articles_sorting(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        self.browser.find_by_text("Sort By: ").click()
        self.browser.find_by_text("Country").click()

        self.assertTrue(
            self.browser.is_text_present(
                "Cómo la veterinaria contribuye al avance de la medicina humana - Diario Veterinario"
            )
        )

    def test_articles_filter(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        self.browser.find_by_text(" Change Filters").click()
        self.assertTrue(self.browser.is_text_present("Filters"))

        # TODO: Add fill to parameters
        self.browser.fill("Country", "Austri\n")
        self.browser.fill("Category", "Techn\n")
        self.browser.fill("Language", "Germ\n")

        self.browser.find_by_text("Apply").click()
        self.assertTrue(
            self.browser.is_text_present(
                "Xbox Wireless Headset - Test, Hardware, Android, iPad, iPhone, PC, Xbox One, Xbox Series X - 4Players Portal"
            )
        )

    def test_articles_search(self):
        self.browser.visit(URL)
        self.assertTrue(self.browser.is_text_present("Articles"))
        # To ensure first page, show first country
        self.assertTrue(
            self.browser.is_text_present(
                "Severed Steel is an action-packed FPS in which you can't reload your gun - Critical Hit"
            )
        )

        self.browser.fill("Articles Search", "Overwatch\n")
        self.assertTrue(
            self.browser.is_text_present(
                "Overwatch Adds NVIDIA Reflex Support And Here's How It's Beneficial – Pokde.Net - Pokde.Net"
            )
        )


if __name__ == "__main__":
    unittest.main()
