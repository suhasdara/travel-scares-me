from selenium.webdriver.chrome.options import Options

# Modifiable Variables depending on machine and browser
BROWSER = "chrome"

# PATH = "chromedrivers/win/chromedriver.exe"
PATH = "chromedrivers/linux/chromedriver"

# MAIN_URL = "http://localhost:3000/"
MAIN_URL = "https://travelscares.me/"

MEMBERS = 4

command_line_options = Options()
command_line_options.add_argument("--headless")
command_line_options.add_argument("--no-sandbox")
command_line_options.add_argument("--disable-dev-shm-usage")

##########################
# Links for AboutTest.py #
##########################
GitlabURL = "https://gitlab.com/suhasdara/travel-scares-me"
PostmanURL = "https://documenter.getpostman.com/view/14734911/Tz5jeL2c"

CountriesAPI = "https://restcountries.eu/"
AdvisoryAPI = "https://www.state.gov/developer/"
CovidAPI = "https://api.covid19api.com/"
CovidBehaviorAPI = "https://gisumd.github.io/COVID-19-API-Documentation/docs/home.html"
RoutesAPI = "https://aviation-edge.com/"
MapsAPI = "https://developers.google.com/maps/documentation/embed/get-started"
NewsAPI = "https://newsapi.org/docs/"
SerpapiAPI = "https://serpapi.com/images-results"
YoutubeAPI = "https://developers.google.com/youtube/v3"
GitlabAPI = "https://docs.gitlab.com/ee/api/README.html"
