import React from "react";
import "assets/styles/App.css";

import Navbar from "components/Navbar/Navbar";
import Routes from "components/Navigation/Routes";

const App = () => {
  return (
    <>
      <Navbar />
      <Routes />
    </>
  );
};

export default App;
