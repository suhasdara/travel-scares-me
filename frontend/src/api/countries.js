import qs from "query-string";

const mapsApiKey = "AIzaSyBA8C6PD5DuFBqaqNHPYIfmGuOlWFuV02Q";

const getCountry = async (id) => {
  const countryRaw = await fetch(`/api/countries/${id}`);
  let country = await countryRaw.json();
  country.maps_url = `https://www.google.com/maps/embed/v1/place?key=${mapsApiKey}&q=${country.name}`;

  return country;
};

const getCountries = async (pageNum, pageSize, filters) => {
  let filterQuery = "";
  if (Object.keys(filters).length > 0) {
    filterQuery = `&${qs.stringify(filters, { skipEmptyString: true })}`;
  }
  const countriesRaw = await fetch(
    `/api/countries?page=${pageNum}&pageSize=${pageSize}${filterQuery}`
  );
  const { count, results } = await countriesRaw.json();
  return {
    count,
    countries: results,
  };
};

export { getCountry, getCountries };
