import qs from "query-string";

const mapsApiKey = "AIzaSyBA8C6PD5DuFBqaqNHPYIfmGuOlWFuV02Q";

const getRoute = async (id) => {
  const routeRaw = await fetch(`/api/routes/${id}`);
  let route = await routeRaw.json();

  route.departure.maps_url = `https://www.google.com/maps/embed/v1/place?key=${mapsApiKey}&q=${route.departure.airport} airport`;
  route.arrival.maps_url = `https://www.google.com/maps/embed/v1/place?key=${mapsApiKey}&q=${route.arrival.airport} airport`;

  return route;
};

const getRoutes = async (pageNum, pageSize, filters) => {
  let filterQuery = "";
  if (Object.keys(filters).length > 0) {
    filterQuery = `&${qs.stringify(filters, { skipEmptyString: true })}`;
  }
  const routesRaw = await fetch(
    `/api/routes?page=${pageNum}&pageSize=${pageSize}${filterQuery}`
  );
  const { count, results } = await routesRaw.json();
  return {
    count,
    routes: results,
  };
};

export { getRoute, getRoutes };
