import qs from "query-string";

const youtubeApiKey = "AIzaSyBA8C6PD5DuFBqaqNHPYIfmGuOlWFuV02Q";

const getArticle = async (id) => {
  const articleRaw = await fetch(`/api/articles/${id}`);
  let article = await articleRaw.json();
  const youtubeDataRaw = await fetch(
    `https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=${article.title}&key=${youtubeApiKey}`
  );
  if (!youtubeDataRaw.ok) {
    article.youtube_url = null;
  } else {
    let youtubeData = await youtubeDataRaw.json();
    if (youtubeData.items.length === 0) {
      article.youtube_url = null;
    } else {
      let videoId = youtubeData.items[0].id.videoId;
      article.youtube_url = `https://www.youtube.com/embed/${videoId}`;
    }
  }

  return article;
};

const getArticles = async (pageNum, pageSize, filters) => {
  let filterQuery = "";
  if (Object.keys(filters).length > 0) {
    filterQuery = `&${qs.stringify(filters, { skipEmptyString: true })}`;
  }
  const articlesRaw = await fetch(
    `/api/articles?page=${pageNum}&pageSize=${pageSize}${filterQuery}`
  );
  const { count, results } = await articlesRaw.json();
  return {
    count,
    articles: results,
  };
};

export { getArticle, getArticles };
