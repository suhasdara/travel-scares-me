import React from "react";
import { Button, Dropdown, InputGroup } from "react-bootstrap";

const Sort = ({
  columns,
  orderBy,
  onOrderByChange,
  ascending,
  onAscendingChange,
}) => {
  return (
    <div className="ml-auto">
      <InputGroup>
        <InputGroup.Prepend>
          <Dropdown className="d-inline-flex">
            <Dropdown.Toggle variant="dark">Sort By: {orderBy}</Dropdown.Toggle>
            <Dropdown.Menu alignRight>
              {columns.map((column) => (
                <Dropdown.Item
                  key={column}
                  active={column === orderBy}
                  onClick={() => onOrderByChange(column)}
                >
                  {column}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </InputGroup.Prepend>
        <InputGroup.Append>
          <Button
            onClick={() => onAscendingChange(!ascending)}
            variant="dark"
            title="Toggle Sort Order"
          >
            <i
              className={`fas fa-arrow-${ascending ? "up" : "down"} btn-icon`}
            />
          </Button>
        </InputGroup.Append>
      </InputGroup>
    </div>
  );
};

export default Sort;
