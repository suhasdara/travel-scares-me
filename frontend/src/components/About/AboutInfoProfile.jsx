import React from "react";
import { Card, Spinner } from "react-bootstrap";

import "assets/styles/About.css";

const AboutInfoProfile = (props) => {
  const { profile } = props;
  return (
    <Card className="about-card">
      <Card.Img src={profile.image} />
      <Card.Body>
        <Card.Title>{profile.name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">
          {profile.role}
        </Card.Subtitle>
        <Card.Text className="profile-about">{profile.about}</Card.Text>
        <hr />
        <Card.Text>
          Commits:{" "}
          {profile.commits === 0 ? (
            <Spinner as="span" animation="border" className="about-spinner" />
          ) : (
            <span>{profile.commits}</span>
          )}
        </Card.Text>
        <Card.Text>
          Issues:{" "}
          {profile.issues === 0 ? (
            <Spinner as="span" animation="border" className="about-spinner" />
          ) : (
            <span>{profile.issues}</span>
          )}
        </Card.Text>
        <Card.Text>
          Tests: <span>{profile.tests}</span>
        </Card.Text>
      </Card.Body>
      <Card.Footer>
        <Card.Link href={profile.linkedIn} className="stretched-link">
          <i className="fab fa-linkedin" /> LinkedIn
        </Card.Link>
      </Card.Footer>
    </Card>
  );
};

export default AboutInfoProfile;
