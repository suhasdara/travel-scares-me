import React from "react";
import { Row, Col, Spinner, CardDeck } from "react-bootstrap";

import { getStatsData } from "components/About/AboutStats";
import { AboutStrings } from "components/About/AboutStrings";
import AboutInfoProfile from "components/About/AboutInfoProfile";

import "assets/styles/About.css";

class About extends React.Component {
  constructor(props) {
    super(props);
    const aboutStrings = AboutStrings();
    aboutStrings.suhas.commits = 0;
    aboutStrings.peter.commits = 0;
    aboutStrings.joseph.commits = 0;
    aboutStrings.matthew.commits = 0;
    aboutStrings.suhas.issues = 0;
    aboutStrings.peter.issues = 0;
    aboutStrings.joseph.issues = 0;
    aboutStrings.matthew.issues = 0;

    this.state = {
      aboutStrings,
      totalCommits: 0,
      totalIssues: 0,
    };
  }

  componentDidMount() {
    getStatsData().then((data) => {
      const aboutStrings = { ...this.state.aboutStrings };
      aboutStrings.suhas.commits = data.commits[aboutStrings.suhas.name] ?? 0;
      aboutStrings.peter.commits = data.commits[aboutStrings.peter.name] ?? 0;
      aboutStrings.joseph.commits = data.commits[aboutStrings.joseph.name] ?? 0;
      aboutStrings.matthew.commits =
        data.commits[aboutStrings.matthew.name] ?? 0;
      aboutStrings.suhas.issues = data.issues[aboutStrings.suhas.name] ?? 0;
      aboutStrings.peter.issues = data.issues[aboutStrings.peter.name] ?? 0;
      aboutStrings.joseph.issues = data.issues[aboutStrings.joseph.name] ?? 0;
      aboutStrings.matthew.issues = data.issues[aboutStrings.matthew.name] ?? 0;

      const totalCommits =
        aboutStrings.suhas.commits +
        aboutStrings.peter.commits +
        aboutStrings.joseph.commits +
        aboutStrings.matthew.commits;
      const totalIssues =
        aboutStrings.suhas.issues +
        aboutStrings.peter.issues +
        aboutStrings.joseph.issues +
        aboutStrings.matthew.issues;

      this.setState({ aboutStrings, totalCommits, totalIssues });
    });
  }

  render() {
    const { aboutStrings, totalCommits, totalIssues } = this.state;
    let team = [
      aboutStrings.suhas,
      aboutStrings.peter,
      aboutStrings.joseph,
      aboutStrings.matthew,
    ];

    return (
      <div className="px-5 pt-5">
        <Row>
          <Col xs="7">
            <Row>
              <h2>Meet the team:</h2>
            </Row>
            <Row>
              <CardDeck className="about-carddeck">
                {team.map((profile) => (
                  <AboutInfoProfile key={profile.name} profile={profile} />
                ))}
              </CardDeck>
            </Row>
            <Row className="d-flex justify-content-center">
              <p>
                <b>Commits:</b>{" "}
                {totalCommits === 0 ? (
                  <Spinner
                    as="span"
                    animation="border"
                    className="about-spinner"
                  />
                ) : (
                  <span>{totalCommits}</span>
                )}
                , <b>Issues:</b>{" "}
                {totalIssues === 0 ? (
                  <Spinner
                    as="span"
                    animation="border"
                    className="about-spinner"
                  />
                ) : (
                  <span>{totalIssues}</span>
                )}
                , <b>Tests:</b> <span>{aboutStrings.totalTests}</span>
              </p>
            </Row>
          </Col>
          <Col xs="5">
            <Row>
              <h1>About</h1>
            </Row>
            <Row>
              <p>{aboutStrings.about}</p>
            </Row>
            <Row>
              <ul className="no-list-style">
                {aboutStrings.resources.map((resource) => (
                  <li key={resource.name} className="about-li">
                    <i className={resource.icon} />{" "}
                    <a href={resource.link}>{resource.name}</a>
                  </li>
                ))}
              </ul>
            </Row>
            <Row>
              <h2>Resources</h2>
            </Row>
            <Row>
              <ul className="no-list-style">
                {aboutStrings.dataSources.map((source) => (
                  <li key={source.name} className="about-li">
                    <i className={source.icon} />{" "}
                    <a href={source.link}>{source.name}</a>: {source.about}
                  </li>
                ))}
              </ul>
            </Row>
            <Row>
              <h2>Tools</h2>
            </Row>
            <Row>
              <ul className="no-list-style">
                {aboutStrings.tools.map((tool) => (
                  <li key={tool.name} className="about-li">
                    <i className={tool.icon} /> {tool.name}: {tool.about}
                  </li>
                ))}
              </ul>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default About;
