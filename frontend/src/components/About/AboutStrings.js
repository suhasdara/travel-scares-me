import suhasImage from "assets/images/suhas.png";
import peterImage from "assets/images/peter.png";
import josephImage from "assets/images/joseph.png";
import matthewImage from "assets/images/matthew.png";

export const AboutStrings = () => {
  return {
    totalTests: 260,
    suhas: {
      linkedIn: "https://www.linkedin.com/in/suhas-dara/",
      name: "Suhas Dara",
      image: suhasImage,
      role: "Fullstack",
      about: "A CS senior who is biting off more than he can chew",
      tests: 53,
    },
    peter: {
      linkedIn: "https://www.linkedin.com/in/peter-cardenas/",
      name: "Peter Cardenas",
      image: peterImage,
      role: "Frontend",
      about: "I'm a 2nd year CS Student having a quarter life crisis",
      tests: 73,
    },
    joseph: {
      linkedIn: "https://www.linkedin.com/in/joseph-jiang-656865161/",
      name: "Joseph Jiang",
      image: josephImage,
      role: "Backend",
      about: "I'm a junior CS student not having a great time",
      tests: 63,
    },
    matthew: {
      linkedIn: "https://www.linkedin.com/in/bianchima/",
      name: "Matthew Bianchi",
      image: matthewImage,
      role: "Frontend",
      about:
        "Senior in Computer Science. I have an interest in Software Engineering, Gaming, and AI",
      tests: 71,
    },
    about:
      "In the current era of COVID-19, travel has become very unsafe and has been discouraged if it is not " +
      "necessary. We aim to bring the latest information about airline routes between different countries, what " +
      "travel advisories are currently imposed on these countries, and the latest news about these countries. We " +
      "also provide the latest COVID-19 statistical data and people's behavioral data to make travel decisions " +
      "easier.",
    resources: [
      {
        name: "Gitlab repo",
        link: "https://gitlab.com/suhasdara/travel-scares-me",
        icon: "fab fa-gitlab",
      },
      {
        name: "Postman documentation",
        link: "https://documenter.getpostman.com/view/14734911/Tz5jeL2c",
        icon: "fas fa-server",
      },
      {
        name: "Presentation",
        link: "https://www.youtube.com/watch?v=fWFjnH2Fas4",
        icon: "fas fa-video",
      }
    ],
    tools: [
      {
        name: "AWS",
        about: "Utilized to host our website",
        icon: "fab fa-aws",
      },
      {
        name: "Namecheap",
        about: "Provides the domain for our website",
        icon: "fas fa-window-maximize",
      },
      {
        name: "Gitlab",
        about: "Used for version control and issue tracking",
        icon: "fab fa-gitlab",
      },
      {
        name: "Postman",
        about: "Used for defining the API routes",
        icon: "fas fa-server",
      },
      {
        name: "React",
        about: "Library used to build the website's frontend",
        icon: "fab fa-react",
      },
      {
        name: "Flask",
        about: "Library used to build the website's backend",
        icon: "fas fa-flask",
      },
    ],
    dataSources: [
      {
        name: "Countries",
        link: "https://restcountries.eu/",
        about: "Generic data about countries",
        icon: "fas fa-globe-europe",
      },
      {
        name: "Travel advisories",
        link: "https://www.state.gov/developer/",
        about: "Travel advisories, threat level for countries",
        icon: "fas fa-plane-slash",
      },
      {
        name: "Covid data",
        link: "https://api.covid19api.com/",
        about: "COVID cases data for countries",
        icon: "fas fa-virus",
      },
      {
        name: "Covid behavior",
        link: "https://covidmap.umd.edu/api.html",
        about: "Human behavior data during COVID for countries",
        icon: "fas fa-head-side-cough",
      },
      {
        name: "Aviation routes",
        link: "https://aviation-edge.com/",
        about: "Aviation routes between countries",
        icon: "fas fa-plane-departure",
      },
      {
        name: "Google maps",
        link:
          "https://developers.google.com/maps/documentation/embed/get-started",
        about: "Embed google maps locations",
        icon: "fas fa-map-marked",
      },
      {
        name: "News",
        link: "https://newsapi.org/docs/",
        about: "Top news articles from different countries",
        icon: "fas fa-newspaper",
      },
      {
        name: "Serpapi",
        link: "https://serpapi.com/images-results",
        about: "Search for news articles source logos",
        icon: "fas fa-search",
      },
      {
        name: "Youtube data",
        link: "https://developers.google.com/youtube/v3",
        about: "Search for videos relevant to news articles",
        icon: "fab fa-youtube",
      },
      {
        name: "Gitlab data",
        link: "https://docs.gitlab.com/ee/api/README.html",
        about: "Retrieve issues and commits count",
        icon: "fab fa-gitlab",
      },
    ],
    gitlabAPI: {
      commitsUrl:
        "https://gitlab.com/api/v4/projects/24747543/repository/commits",
      issuesUrl: "https://gitlab.com/api/v4/projects/24747543/issues",
    },
  };
};
