import { AboutStrings } from "components/About/AboutStrings";

const aboutStrings = AboutStrings();

export async function getStatsData() {
  const { commitsUrl, issuesUrl } = aboutStrings.gitlabAPI;

  let page = 1;
  let commits = [];
  let commitsPage = [];
  do {
    const commitsPageUrl = `${commitsUrl}?page=${page++}`;
    commitsPage = await fetch(commitsPageUrl).then((resp) => resp.json());
    commits = commits.concat(commitsPage);
  } while (commitsPage.length > 0);

  const commitStats = {};
  commits.forEach((commit) => {
    if (!(commit.author_name in commitStats))
      commitStats[commit.author_name] = 0;
    commitStats[commit.author_name]++;
  });

  page = 1;
  let issues = [];
  let issuesPage = [];
  do {
    const issuesPageUrl = `${issuesUrl}?page=${page++}`;
    issuesPage = await fetch(issuesPageUrl).then((resp) => resp.json());
    issues = issues.concat(issuesPage);
  } while (issuesPage.length > 0);

  const issueStats = {};
  issues.forEach((issue) => {
    if (issue.closed_by) {
      if (!(issue.closed_by.name in issueStats))
        issueStats[issue.closed_by.name] = 0;
      issueStats[issue.closed_by.name]++;
    }
  });

  return {
    commits: commitStats,
    issues: issueStats,
  };
}
