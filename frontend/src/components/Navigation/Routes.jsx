import React from "react";

import { Route, Switch } from "react-router-dom";

import Countries from "components/Country/Countries";
import Country from "components/Country/Country";
import Articles from "components/Article/Articles";
import Article from "components/Article/Article";
import AirplaneRoute from "components/AirplaneRoute/AirplaneRoute";
import AirplaneRoutes from "components/AirplaneRoute/AirplaneRoutes";
import About from "components/About/About";
import Splash from "components/Splash/Splash";
import Search from "components/GlobalSearch/Search";
import Visualization from "components/Visualization/Visualization";

const Routes = () => {
  return (
    <div className="page">
      <Switch>
        <Route exact path="/" render={() => <Splash />} />
        <Route path="/about" render={() => <About />} />
        <Route
          path="/routes/:routeId"
          render={({
            match: {
              params: { routeId },
            },
          }) => <AirplaneRoute routeId={routeId} />}
        />
        <Route path="/routes" render={() => <AirplaneRoutes />} />
        <Route
          path="/countries/:countryId"
          render={({
            match: {
              params: { countryId },
            },
          }) => <Country countryId={countryId} />}
        />
        <Route path="/countries" render={() => <Countries />} />
        <Route
          path="/articles/:articleId"
          render={({
            match: {
              params: { articleId },
            },
          }) => <Article articleId={articleId} />}
        />
        <Route path="/articles" render={() => <Articles />} />
        <Route path="/visualization" render={() => <Visualization />} />
        <Route
          path="/search/q=:searchQuery"
          render={({
            match: {
              params: { searchQuery },
            },
          }) => <Search searchQuery={searchQuery} />}
        />
      </Switch>
    </div>
  );
};

export default Routes;
