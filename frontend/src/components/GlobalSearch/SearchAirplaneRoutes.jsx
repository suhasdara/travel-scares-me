import React, { useState } from "react";
import { Table } from "react-bootstrap";
import RoutePreview from "components/AirplaneRoute/RoutePreview";
import SearchPagination from "components/GlobalSearch/SearchPagination";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getRoutes } from "api/routes";

const FIXED_PAGE_SIZE = 10;

const SearchAirplaneRoutes = ({ searchQuery }) => {
  const q = {
    q: searchQuery,
    orderBy: "flight",
    ordering: "asc",
  };

  const [page, setPage] = useState(1);
  const { isLoading, data } = useQuery(["routes", page], () =>
    getRoutes(page, FIXED_PAGE_SIZE, q)
  );

  return (
    <div>
      <h2>Airplane Routes</h2>
      {isLoading ? (
        <GlobalSpinner />
      ) : data && data.routes.length > 0 ? (
        <>
          <Table bordered hover variant="dark" className="routes-table">
            <thead>
              <tr>
                <th>Flight number</th>
                <th>Source city</th>
                <th>Source country</th>
                <th>Dest city</th>
                <th>Dest country</th>
              </tr>
            </thead>
            <tbody>
              {data.routes.map((route) => (
                <RoutePreview
                  key={route.id}
                  routeData={route}
                  searchQuery={searchQuery}
                />
              ))}
            </tbody>
          </Table>
          <SearchPagination
            numResults={data.count}
            pageSize={FIXED_PAGE_SIZE}
            currPage={page}
            onPageClick={setPage}
          />
        </>
      ) : (
        <b>No matches found</b>
      )}
    </div>
  );
};

export default SearchAirplaneRoutes;
