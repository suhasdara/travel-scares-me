import React from "react";
import SearchCountries from "components/GlobalSearch/SearchCountries";
import SearchArticles from "components/GlobalSearch/SearchArticles";
import SearchAirplaneRoutes from "components/GlobalSearch/SearchAirplaneRoutes";

import "assets/styles/Search.css";

const Search = ({ searchQuery }) => {
  return (
    <div className="p-5 page-div">
      <h1>Search Results for {searchQuery}</h1>
      <hr className="thick-hr" />
      <SearchCountries searchQuery={searchQuery} />
      <hr className="thick-hr" />
      <SearchArticles searchQuery={searchQuery} />
      <hr className="thick-hr" />
      <SearchAirplaneRoutes searchQuery={searchQuery} />
      <hr className="thick-hr" />
    </div>
  );
};

export default Search;
