import React from "react";

import { Dropdown } from "react-bootstrap";
import PaginationRow from "components/Pagination/PaginationRow";

const SearchPagination = ({ pageSize, currPage, onPageClick, numResults }) => {
  const numPages = Math.ceil(numResults / pageSize);
  const pages = Array.from(new Array(numPages), (x, i) => i + 1);

  return (
    <PaginationRow
      currPage={currPage}
      numPages={numPages}
      onPageClick={onPageClick}
    >
      <Dropdown className="d-flex align-items-center" drop="up">
        <Dropdown.Toggle className="mr-1" variant="dark">
          {currPage}
        </Dropdown.Toggle>
        of {numPages}
        <Dropdown.Menu>
          {pages.map((pageNo) => (
            <Dropdown.Item
              key={pageNo}
              as="div"
              active={pageNo === currPage}
              onClick={() => onPageClick(pageNo)}
            >
              {pageNo}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    </PaginationRow>
  );
};

export default SearchPagination;
