import React, { useState } from "react";
import { CardDeck, Spinner } from "react-bootstrap";
import SearchPagination from "components/GlobalSearch/SearchPagination";
import CountryPreview from "components/Country/CountryPreview";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getCountries } from "api/countries";

import "assets/styles/Search.css";

const FIXED_PAGE_SIZE = 6;

const SearchCountries = ({ searchQuery }) => {
  const q = {
    q: searchQuery,
    orderBy: "name",
    ordering: "asc",
  };

  const [page, setPage] = useState(1);
  const { isLoading, data } = useQuery(["countries", page], () =>
    getCountries(page, FIXED_PAGE_SIZE, q)
  );

  return (
    <div>
      <h2>Countries</h2>
      {isLoading ? (
        <GlobalSpinner />
      ) : data && data.countries.length > 0 ? (
        <>
          <CardDeck className="d-flex justify-content-center search">
            {data.countries.map((country) => (
              <CountryPreview
                key={country.code}
                countryData={country}
                searchQuery={searchQuery}
              />
            ))}
          </CardDeck>
          <SearchPagination
            numResults={data.count}
            pageSize={FIXED_PAGE_SIZE}
            currPage={page}
            onPageClick={setPage}
          />
        </>
      ) : (
        <b>No matches found</b>
      )}
    </div>
  );
};

export default SearchCountries;
