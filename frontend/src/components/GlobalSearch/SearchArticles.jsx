import React, { useState } from "react";
import { CardDeck } from "react-bootstrap";
import SearchPagination from "components/GlobalSearch/SearchPagination";
import ArticlePreview from "components/Article/ArticlePreview";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getArticles } from "api/articles";

import "assets/styles/Search.css";

const FIXED_PAGE_SIZE = 6;

const SearchArticles = ({ searchQuery }) => {
  const q = {
    q: searchQuery,
    orderBy: "published",
    ordering: "desc",
  };

  const [page, setPage] = useState(1);
  const { isLoading, data } = useQuery(["articles", page], () =>
    getArticles(page, FIXED_PAGE_SIZE, q)
  );

  return (
    <div>
      <h2>Articles</h2>
      {isLoading ? (
        <GlobalSpinner />
      ) : data && data.articles.length > 0 ? (
        <>
          <CardDeck className="d-flex justify-content-center search">
            {data.articles.map((article) => (
              <ArticlePreview
                key={article.id}
                articleData={article}
                searchQuery={searchQuery}
              />
            ))}
          </CardDeck>
          <SearchPagination
            numResults={data.count}
            pageSize={FIXED_PAGE_SIZE}
            currPage={page}
            onPageClick={setPage}
          />
        </>
      ) : (
        <b>No matches found</b>
      )}
    </div>
  );
};

export default SearchArticles;
