import React from "react";
import Highlighter from "react-highlight-words";

import "assets/styles/Search.css";

const TextHighlight = ({ searchWords, textToHighlight }) => {
  return (
    <Highlighter
      highlightClassName="search-highlight"
      searchWords={searchWords}
      autoEscape={true}
      textToHighlight={textToHighlight}
    />
  );
};

export default TextHighlight;
