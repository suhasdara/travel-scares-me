import React, { useState } from "react";
import { Button, Form, FormControl, InputGroup } from "react-bootstrap";

const SearchBar = ({ name, setSearchQuery, className }) => {
  const [searchValue, setSearchValue] = useState("");

  const handleOnSubmit = () => setSearchQuery(searchValue);
  const handleOnChange = (event) => setSearchValue(event.target.value);

  const handleOnKeyUp = (event) => {
    event.preventDefault();
    if (event.key === "Enter" && event.keyCode === 13) {
      handleOnSubmit();
    }
  };

  const preventFormSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <Form inline onSubmit={preventFormSubmit} className={className}>
      <InputGroup>
        <FormControl
          name={name}
          onChange={handleOnChange}
          onKeyUp={handleOnKeyUp}
          type="text"
          placeholder="Search page"
          value={searchValue}
          className="model-form"
        />
        <InputGroup.Append>
          <Button
            onClick={handleOnSubmit}
            variant="dark"
            // disabled={!searchValue}
          >
            <i className="fas fa-search" />
          </Button>
        </InputGroup.Append>
      </InputGroup>
    </Form>
  );
};

export default SearchBar;
