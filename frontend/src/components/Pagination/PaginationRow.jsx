import React from "react";
import { Button, Col, Row } from "react-bootstrap";

const PaginationRow = ({
  currPage,
  numPages,
  onPageClick,
  className,
  children,
}) => {
  return (
    <Row className={className}>
      <Col>
        <Button
          className="mr-2"
          onClick={() => onPageClick(1)}
          disabled={currPage === 1}
          variant="dark"
        >
          <i className="fas fa-chevron-left" />
          <i className="fas fa-chevron-left" /> First
        </Button>
        <Button
          onClick={() => onPageClick(currPage - 1)}
          disabled={currPage === 1}
          variant="dark"
        >
          <i className="fas fa-chevron-left" /> Prev
        </Button>
      </Col>
      <Col className="d-flex justify-content-center">{children}</Col>
      <Col className="d-flex justify-content-end">
        <Button
          className="mr-2"
          onClick={() => onPageClick(currPage + 1)}
          disabled={currPage === numPages}
          variant="dark"
        >
          Next <i className="fas fa-chevron-right" />
        </Button>
        <Button
          onClick={() => onPageClick(numPages)}
          disabled={currPage === numPages}
          variant="dark"
        >
          Last <i className="fas fa-chevron-right" />
          <i className="fas fa-chevron-right" />
        </Button>
      </Col>
    </Row>
  );
};

export default PaginationRow;
