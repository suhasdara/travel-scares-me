import PaginationRow from "components/Pagination/PaginationRow";
import React from "react";

import { Dropdown } from "react-bootstrap";

const Pagination = ({
  pageSize,
  currPage,
  onPageClick,
  numResults,
  handlePageSizeChange,
  children,
}) => {
  const numPages = Math.ceil(numResults / pageSize);
  const pages = Array.from(new Array(numPages), (x, i) => i + 1);

  return (
    <>
      <PaginationRow
        className="mb-3"
        currPage={currPage}
        numPages={numPages}
        onPageClick={onPageClick}
      >
        <h5>
          Total <b>{numResults}</b> items
        </h5>
      </PaginationRow>
      {children}
      <PaginationRow
        currPage={currPage}
        numPages={numPages}
        onPageClick={onPageClick}
      >
        <Dropdown className="d-flex align-items-center" drop="up">
          <Dropdown.Toggle className="mr-1" variant="dark">
            {currPage}
          </Dropdown.Toggle>
          of {numPages}
          <Dropdown.Menu>
            {pages.map((pageNo) => (
              <Dropdown.Item
                key={`page-${pageNo}`}
                as="div"
                active={pageNo === currPage}
                onClick={() => onPageClick(pageNo)}
              >
                {pageNo}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </PaginationRow>
      <div className="">
        <div className="d-flex justify-content-center">
          <Dropdown className="d-flex align-items-center" drop="up">
            Number of instances to display:
            <Dropdown.Toggle className="ml-1" variant="dark">
              {pageSize}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {[8, 20, 40, 80].map((size) => (
                <Dropdown.Item
                  key={`page-size-${size}`}
                  as="div"
                  active={size === pageSize}
                  onClick={() => {
                    onPageClick(1);
                    handlePageSizeChange(size);
                  }}
                >
                  {size}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    </>
  );
};

export default Pagination;
