import React from "react";
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getArticle } from "api/articles";

const Article = ({ articleId }) => {
  const { isLoading, data } = useQuery(["article", articleId], () =>
    getArticle(articleId)
  );

  return (
    <div className="px-5 pt-5 page-div">
      {isLoading ? (
        <GlobalSpinner />
      ) : (
        <div>
          <h1>{data?.title}</h1>
          <Row>
            <Col xs="6">
              {data?.author && (
                <p>
                  <b>Author(s):</b> {data.author}
                </p>
              )}
              {data?.url_to_image && (
                <img
                  src={data.url_to_image}
                  alt={`${data?.title}`}
                  className="mb-3"
                  width="80%"
                />
              )}
              <p>
                <b>Category:</b> {data?.category}
              </p>
              <p>
                <b>Country:</b>{" "}
                <Link to={`/countries/${data?.country_id}`}>
                  {data?.country} ({data?.country_code})
                </Link>
              </p>
              <p>
                <b>Language:</b> {data?.article_language}
              </p>
              <p>
                <b>Published:</b> {new Date(data?.published).toString()}
              </p>
              {data?.description && (
                <p>
                  <b>Description:</b> {data.description}
                </p>
              )}
              <p>
                <a href={data?.url}>
                  <i className="fas fa-link" /> View article
                </a>
              </p>
              <p>
                <b>Most relevant YouTube video:</b> <br />
                {data?.youtube_url ? (
                  <div className="video-container">
                    <iframe
                      title={data?.title}
                      src={data?.youtube_url}
                      width="560"
                      height="315"
                      loading="lazy"
                    />
                  </div>
                ) : (
                  <span>No video available</span>
                )}
              </p>
            </Col>
            <Col xs="6">
              <h4>About the article source:</h4>
              <Row>
                <Col xs="4">
                  <img
                    src={data?.source_logo_url}
                    alt={`${data?.source}`}
                    className="mb-3"
                    width="100%"
                  />
                </Col>
                <Col xs="8">
                  <p>
                    <b>Source:</b> {data?.source}
                  </p>
                  {data?.source_description && (
                    <p>
                      <b>Description:</b> {data.source_description}
                    </p>
                  )}
                  <p>
                    <b>Website:</b>{" "}
                    <a href={data?.url_to_source}>{data?.url_to_source}</a>
                  </p>
                </Col>
              </Row>
              <h4>Flights to {data?.country}:</h4>
              {data?.related_routes.length > 0 ? (
                <ul>
                  {data.related_routes.map((route) => (
                    <li key={route.id}>
                      From{" "}
                      <Link to={`/routes/${route.id}`}>{route.source}</Link>
                    </li>
                  ))}
                </ul>
              ) : (
                <p>Unavailable</p>
              )}
            </Col>
          </Row>
        </div>
      )}
    </div>
  );
};

export default Article;
