import React, { useState } from "react";
import { CardDeck } from "react-bootstrap";
import { useQuery } from "react-query";
import ArticlePreview from "components/Article/ArticlePreview";
import Pagination from "components/Pagination";
import FilterModal from "components/Filter/FilterModal";
import Sort from "components/Sort/Sort";
import SearchBar from "components/Search/SearchBar";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import ARTICLE_FILTERS from "components/Article/ArticleFilters";
import { filterStateToJSON } from "components/Filter/utils";
import { getArticles } from "api/articles";

import "assets/styles/Models.css";

const DEFAULT_PAGE_SIZE = 20;
const SORT_COLUMNS = ["Published", "Category", "Country", "Language", "Title"];

const Articles = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(DEFAULT_PAGE_SIZE);
  const [filters, setFilters] = useState(ARTICLE_FILTERS);
  const [orderBy, setOrderBy] = useState(SORT_COLUMNS[0]);
  const [ascending, setAscending] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const parsedFilters = filterStateToJSON(filters);
  parsedFilters.orderBy = orderBy.toLowerCase();
  parsedFilters.ordering = ascending ? "asc" : "desc";
  parsedFilters.q = searchQuery;
  const { isLoading, data } = useQuery(
    ["articles", page, pageSize, parsedFilters],
    () => getArticles(page, pageSize, parsedFilters)
  );

  return (
    <div className="p-5">
      <div className="d-flex mb-1">
        <h1>Articles</h1>
      </div>
      <div className="d-flex mb-2">
        <FilterModal filters={filters} onFilterSubmit={setFilters} />
        <Sort
          columns={SORT_COLUMNS}
          orderBy={orderBy}
          onOrderByChange={setOrderBy}
          ascending={ascending}
          onAscendingChange={setAscending}
        />
        <SearchBar
          name="Articles Search"
          setSearchQuery={setSearchQuery}
          className="ml-2"
        />
      </div>
      <hr className="mb-3 thick-hr" />
      {isLoading ? (
        <GlobalSpinner />
      ) : data && data.articles.length > 0 ? (
        <Pagination
          numResults={data.count}
          pageSize={pageSize}
          currPage={page}
          onPageClick={setPage}
          handlePageSizeChange={setPageSize}
        >
          <CardDeck className="d-flex justify-content-center models">
            {data.articles.map((article) => (
              <ArticlePreview
                key={`article-${article.id}`}
                articleData={article}
                searchQuery={searchQuery}
              />
            ))}
          </CardDeck>
        </Pagination>
      ) : (
        <p className="d-flex justify-content-center">
          <b>No data available</b>
        </p>
      )}
    </div>
  );
};

export default Articles;
