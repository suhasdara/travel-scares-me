import React from "react";

import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import TextHighlight from "components/GlobalSearch/TextHighlight";

import "assets/styles/Search.css";

const ArticlePreview = ({ articleData, searchQuery }) => {
  const searchWords = searchQuery ? [searchQuery] : [];
  const publishedDate =
    new Date(articleData.published).toDateString() +
    " " +
    new Date(articleData.published).toLocaleTimeString();

  return (
    <Card className="hover">
      <Card.Img src={articleData.url_to_image} variant="top" />
      <Card.Body>
        <Card.Title>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={articleData.title}
          />
        </Card.Title>
        <Card.Text>
          <b>Category: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={articleData.category}
          />
          <br />

          <b>Published: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={publishedDate}
          />
          <br />

          <b>Language: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={articleData.article_language}
          />
          <br />

          <b>Country: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={articleData.country}
          />
        </Card.Text>
      </Card.Body>
      <Link to={`/articles/${articleData.id}`} className="stretched-link" />
    </Card>
  );
};

export default ArticlePreview;
