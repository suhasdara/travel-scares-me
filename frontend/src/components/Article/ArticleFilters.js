import { SELECT_FILTER } from "components/Filter/utils";
import categories from "data/filter_article_category.json";
import countries from "data/filter_article_country.json";
import languages from "data/filter_article_language.json";

const ARTICLE_FILTERS = [
  {
    ...SELECT_FILTER,
    name: "Country",
    options: countries,
  },
  {
    ...SELECT_FILTER,
    name: "Category",
    options: categories,
  },
  {
    ...SELECT_FILTER,
    name: "Language",
    options: languages,
  },
];

export default ARTICLE_FILTERS;
