import React from "react";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getArticles } from "api/articles";

import "assets/styles/Visualization.css";

const ArticleChart = ({ data, title }) => (
  <div className="chart-div-articles">
    <Chart
      width="100%"
      height="100%"
      chartType="PieChart"
      loader={<GlobalSpinner />}
      data={data}
      options={{
        title: title,
        is3D: true,
        tooltip: { text: "value" },
        pieSliceText: "none",
      }}
    />
  </div>
);

const Articles = () => {
  const { isLoading, data } = useQuery(["articles"], () => getData());

  const getData = async () => {
    let data = await getArticles(1, 8000, {});
    let languages = data.articles.reduce(
      (counts, val) =>
        (counts[val["article_language"]] =
          counts[val["article_language"]] + 1 || 1) && counts,
      {}
    );
    languages = Object.keys(languages).map((key) => [key, languages[key]]);
    languages.splice(0, 0, ["Language", "Number of articles"]);
    let countries = data.articles.reduce(
      (counts, val) =>
        (counts[val["country"]] = counts[val["country"]] + 1 || 1) && counts,
      {}
    );
    countries = Object.keys(countries).map((key) => [key, countries[key]]);
    countries.splice(0, 0, ["Country", "Number of articles"]);

    data = { languages: languages, countries: countries };
    return data;
  };

  return (
    <div>
      {isLoading ? (
        <GlobalSpinner className="visual-div" />
      ) : (
        <div className="p-3 justify-content-center visual-div">
          <div>
            <h3>Distribution of articles by language and country</h3>
          </div>
          {data && (
            <div className="d-flex justify-content-between">
              <ArticleChart
                data={data.languages}
                title="Article distribution by language"
              />
              <ArticleChart
                data={data.countries}
                title="Article distribution by country"
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Articles;
