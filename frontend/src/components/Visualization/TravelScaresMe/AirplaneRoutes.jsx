import React from "react";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getRoutes } from "api/routes";

import "assets/styles/Visualization.css";

const AirplaneRoutes = () => {
  const { isLoading, data } = useQuery(["routes"], () => getData());

  const getData = async () => {
    let data = await getRoutes(1, 9000, {});
    data = data.routes.reduce(
      (counts, val) =>
        (counts[val["arrival_country"]] =
          counts[val["arrival_country"]] + 1 || 1) && counts,
      {}
    );
    data = Object.keys(data).map((key) => [key, data[key]]);
    data.sort((a, b) => b[1] - a[1]);
    data = data.slice(0, 50);
    data.splice(0, 0, ["Country", "Number of flights"]);
    return data;
  };

  return (
    <div>
      {isLoading ? (
        <GlobalSpinner className="visual-div" />
      ) : (
        <div className="p-3 justify-content-center visual-div">
          <div>
            <h3>50 most travelled to countries</h3>
          </div>
          {data && (
            <div>
              <Chart
                width="100%"
                height="500px"
                chartType="ColumnChart"
                loader={<GlobalSpinner />}
                data={data}
                options={{
                  title: "Most flights",
                  legend: { position: "none" },
                  chartArea: {
                    width: "90%",
                    height: "60%",
                  },
                  hAxis: {
                    title: "Country",
                    showTextEvery: 1,
                    slantedText: true,
                    slantedTextAngle: 45,
                  },
                  vAxis: {
                    title: "Number of flights",
                    minValue: 0,
                  },
                }}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default AirplaneRoutes;
