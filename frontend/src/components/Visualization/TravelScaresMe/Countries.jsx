import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getCountries } from "api/countries";

import "assets/styles/Visualization.css";

const CountryChart = ({ type, data, options }) => (
  <div>
    <Chart
      width="100%"
      height="500px"
      chartType={type}
      loader={<GlobalSpinner />}
      data={data}
      options={options}
    />
  </div>
);

const Countries = () => {
  const { isLoading, data } = useQuery(["countries"], () => getData());
  const [geo, setGeo] = useState(false);

  const getData = async () => {
    let data = await getCountries(1, 250, {});
    let bubble = data.countries.map((obj) => [
      obj.code,
      obj.longitude,
      obj.latitude,
      obj.name,
      obj.population,
    ]);
    bubble.splice(0, 0, [
      "Code",
      "Longitude",
      "Latitude",
      "Country",
      "Population",
    ]);
    let geo = data.countries.map((obj) => [obj.code, obj.population]);
    geo.splice(0, 0, ["Country", "Population"]);

    data = { bubble: bubble, geo: geo };
    return data;
  };

  return (
    <div>
      {isLoading ? (
        <GlobalSpinner className="visual-div" />
      ) : (
        <div className="p-3 justify-content-center visual-div">
          <div className="d-flex">
            <h3 className="d-inline-block">
              Population map of the world
              {geo ? "" : " (Zoomable, Pannable)"}
            </h3>
            <Button
              onClick={() => setGeo(false)}
              variant="dark"
              className="ml-auto pb-1 d-inline-block"
            >
              Axes
            </Button>
            <Button
              onClick={() => setGeo(true)}
              variant="dark"
              className="ml-2 pb-1 d-inline-block"
            >
              Map
            </Button>
          </div>
          {data &&
            (geo ? (
              <CountryChart
                type="GeoChart"
                data={data.geo}
                options={{
                  title: "Location vs Population",
                  legend: { position: "none" },
                  chartArea: {
                    width: "90%",
                    height: "70%",
                  },
                }}
              />
            ) : (
              <CountryChart
                type="BubbleChart"
                data={data.bubble}
                options={{
                  title: "Location vs Population",
                  legend: { position: "none" },
                  chartArea: {
                    width: "90%",
                    height: "70%",
                  },
                  hAxis: {
                    position: "none",
                    minValue: -180,
                    maxValue: 180,
                  },
                  vAxis: {
                    position: "none",
                    minValue: -90,
                    maxValue: 90,
                  },
                  bubble: {
                    textStyle: { fontSize: 9 },
                  },
                  explorer: {
                    actions: ["dragToPan", "dragToZoom", "rightClickToReset"],
                    keepInBounds: true,
                    naxZoomIn: 0.125,
                    maxZoomOut: 1,
                  },
                }}
              />
            ))}
        </div>
      )}
    </div>
  );
};

export default Countries;
