import React from "react";
import Tabs from "react-bootstrap/Tabs";
import { Tab } from "react-bootstrap";
import Countries from "components/Visualization/TravelScaresMe/Countries";
import AirplaneRoutes from "components/Visualization/TravelScaresMe/AirplaneRoutes";
import Articles from "components/Visualization/TravelScaresMe/Articles";
import CountriesAAA from "components/Visualization/AidAllAround/CountriesAAA";
import CharitiesAAA from "components/Visualization/AidAllAround/CharitiesAAA";
import ArticlesAAA from "components/Visualization/AidAllAround/ArticlesAAA";

import "assets/styles/Visualization.css";

const LazyTab = ({ children, eventKey, title }) => (
  <Tab
    eventKey={eventKey}
    title={title}
    mountOnEnter={true}
    unmountOnExit={false}
  >
    {children}
  </Tab>
);

const Visualization = () => {
  return (
    <div className="p-5">
      <h1>Our Visualizations</h1>
      <Tabs defaultActiveKey="airplaneRoutes">
        <LazyTab eventKey="countries" title="Countries">
          <Countries />
        </LazyTab>
        <LazyTab eventKey="airplaneRoutes" title="Airplane Routes">
          <AirplaneRoutes />
        </LazyTab>
        <LazyTab eventKey="articles" title="Articles">
          <Articles />
        </LazyTab>
      </Tabs>
      <h1>
        <a href="https://www.aidallaround.me">Aid All Around</a> (Provider)
        Visualizations
      </h1>
      <Tabs defaultActiveKey="countries">
        <LazyTab eventKey="countries" title="Countries">
          <CountriesAAA />
        </LazyTab>
        <LazyTab eventKey="charities" title="Charities">
          <CharitiesAAA />
        </LazyTab>
        <LazyTab eventKey="articles" title="Articles">
          <ArticlesAAA />
        </LazyTab>
      </Tabs>
    </div>
  );
};

export default Visualization;
