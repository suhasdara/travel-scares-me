import React from "react";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";

import "assets/styles/Visualization.css";

const ArticlesAAA = () => {
  const { isLoading, data } = useQuery(["articlesAAA"], () => getData());

  const getData = async () => {
    let data = await fetch(
      "https://www.aidallaround.me/api/article?page=1&results_per_page=100"
    );
    data = await data.json();
    let dataPage2 = await fetch(
      "https://www.aidallaround.me/api/article?page=2&results_per_page=100"
    );
    dataPage2 = await dataPage2.json();
    data = data.objects.concat(dataPage2.objects);

    let counts = {};
    for (const obj of data) {
      let date = new Date(new Date(obj["publishedAt"]).toDateString());
      if (!(date in counts)) {
        counts[date] = [0, 0];
      }
      if (obj["sourceCountry"] === "United States") {
        counts[date][0]++;
      } else if (obj["sourceCountry"] === "Great Britain") {
        counts[date][1]++;
      }
    }
    data = Object.keys(counts).map((key) => [new Date(key), ...counts[key]]);
    data.sort((a, b) => a[0] - b[0]);
    data = data.map((obj) => [obj[0].toDateString().slice(3), obj[1], obj[2]]);
    data.splice(0, 0, ["Date", "US articles", "UK articles"]);
    return data;
  };

  return (
    <div>
      {isLoading ? (
        <GlobalSpinner className="visual-div" />
      ) : (
        <div className="p-3 justify-content-center visual-div">
          <div>
            <h3>Number of articles from different source country over time</h3>
          </div>
          {data && (
            <div>
              <Chart
                width="100%"
                height="500px"
                chartType="ColumnChart"
                loader={<GlobalSpinner />}
                data={data}
                options={{
                  title: "Published date vs number of articles",
                  chartArea: {
                    width: "80%",
                    height: "70%",
                  },
                  hAxis: {
                    title: "Published date",
                    showTextEvery: 1,
                  },
                  vAxis: {
                    title: "Number of articles",
                    minValue: 0,
                  },
                }}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default ArticlesAAA;
