import React from "react";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";

import "assets/styles/Visualization.css";

const CountriesAAA = () => {
  const { isLoading, data } = useQuery(["countriesAAA"], () => getData());

  const getData = async () => {
    let data = await fetch(
      "https://www.aidallaround.me/api/country?page=1&results_per_page=100"
    );
    data = await data.json();
    let dataPage2 = await fetch(
      "https://www.aidallaround.me/api/country?page=2&results_per_page=100"
    );
    dataPage2 = await dataPage2.json();
    data = data.objects.concat(dataPage2.objects);
    data = data.filter((obj) => obj.gini > 20);
    data = data.map((obj) => [
      obj.alpha2Code,
      obj.population,
      obj.gini,
      obj.name,
      obj.area,
    ]);
    data.splice(0, 0, [
      "ID",
      "Population (logarithmic)",
      "Gini",
      "Country",
      "Area",
    ]);
    return data;
  };

  return (
    <div>
      {isLoading ? (
        <GlobalSpinner className="visual-div" />
      ) : (
        <div className="p-3 justify-content-center visual-div">
          <div>
            <h3>Countries Population and Gini index correlation</h3>
          </div>
          {data && (
            <div>
              <Chart
                width="100%"
                height="500px"
                chartType="BubbleChart"
                loader={<GlobalSpinner />}
                data={data}
                options={{
                  title: "Population vs Gini index",
                  legend: { position: "none" },
                  chartArea: {
                    width: "90%",
                    height: "70%",
                  },
                  hAxis: {
                    title: "Population",
                    scaleType: "log",
                    minValue: 100000,
                    maxValue: 3000000000,
                  },
                  vAxis: {
                    title: "Gini Index (0 - 100)",
                    minValue: 10,
                    maxValue: 70,
                  },
                  bubble: {
                    textStyle: { fontSize: 9 },
                  },
                }}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default CountriesAAA;
