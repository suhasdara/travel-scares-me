import React from "react";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";

import "assets/styles/Visualization.css";

const CharityChart = ({ data, title }) => (
  <div className="chart-div-charitiesAAA">
    <Chart
      width="100%"
      height="100%"
      chartType="PieChart"
      loader={<GlobalSpinner />}
      data={data}
      options={{
        title: title,
        is3D: true,
        tooltip: { text: "percentage" },
        pieSliceText: "none",
      }}
    />
  </div>
);

const CharitiesAAA = () => {
  const { isLoading, data } = useQuery(["charitiesAAA"], () => getData());

  const brackets = [
    "$10K-$100K",
    "$100K-$1M",
    "$1M-$10M",
    "$10M-$100M",
    "$100M-$1B",
    "$1B-$10B",
  ];

  const getData = async () => {
    let data = await fetch(
      "https://www.aidallaround.me/api/charity?page=1&results_per_page=100"
    );
    data = await data.json();
    let dataPage2 = await fetch(
      "https://www.aidallaround.me/api/charity?page=2&results_per_page=100"
    );
    dataPage2 = await dataPage2.json();
    data = data.objects.concat(dataPage2.objects);

    let ratings = data.reduce(
      (counts, val) =>
        (counts[val["rating"]] = counts[val["rating"]] + 1 || 1) && counts,
      {}
    );
    ratings = Object.keys(ratings).map((key) => [key, ratings[key]]);
    ratings.sort((a, b) => b[0] - a[0]);
    ratings.splice(0, 0, ["Rating", "Number of charities"]);

    let income = data.reduce((counts, val) => {
      let i = Math.floor(Math.log10(val["incomeAmount"] / 10000));
      return (counts[brackets[i]] = counts[brackets[i]] + 1 || 1) && counts;
    }, {});
    income = Object.keys(income).map((key) => [key, income[key]]);
    income.sort((a, b) => brackets.indexOf(a[0]) - brackets.indexOf(b[0]));
    income.splice(0, 0, ["Income amount", "Number of charities"]);

    let assets = data.reduce((counts, val) => {
      let i = Math.floor(Math.log10(val["assetAmount"] / 10000));
      return (counts[brackets[i]] = counts[brackets[i]] + 1 || 1) && counts;
    }, {});
    assets = Object.keys(assets).map((key) => [key, assets[key]]);
    assets.sort((a, b) => brackets.indexOf(a[0]) - brackets.indexOf(b[0]));
    assets.splice(0, 0, ["Assets amount", "Number of charities"]);

    data = { ratings: ratings, income: income, assets: assets };
    return data;
  };

  return (
    <div>
      {isLoading ? (
        <GlobalSpinner className="visual-div" />
      ) : (
        <div className="p-3 justify-content-center visual-div">
          <div>
            <h3>Charities distributions</h3>
          </div>
          {data && (
            <div className="d-flex justify-content-between">
              <CharityChart
                data={data.ratings}
                title={"Charity distribution by ratings"}
              />
              <CharityChart
                data={data.income}
                title={"Charity distribution by income amount"}
              />
              <CharityChart
                data={data.assets}
                title={"Charity distribution by assets amount"}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default CharitiesAAA;
