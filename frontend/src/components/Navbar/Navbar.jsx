import React, { useState } from "react";

import BootstrapNavbar from "react-bootstrap/Navbar";
import { Nav, Form, FormControl, InputGroup, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

import logoImg from "assets/images/logo.png";

const Navbar = () => {
  const [activeKey, setActiveKey] = useState(undefined);
  const [searchQuery, setSearchQuery] = useState("");

  const preventFormSubmit = (event) => {
    event.preventDefault();
  };

  const handleOnSubmit = () => {
    if (searchQuery) {
      window.location.assign(`/search/q=${searchQuery}`);
    }
  };

  const handleOnChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const handleOnKeyUp = (event) => {
    event.preventDefault();
    if (event.key === "Enter" && event.keyCode === 13) {
      handleOnSubmit();
    }
  };

  return (
    <BootstrapNavbar
      expand="md"
      className="bg-dark"
      variant="dark"
      collapseOnSelect
    >
      <BootstrapNavbar.Brand
        as={Link}
        to="/"
        className="d-inline-flex py-2"
        onClick={() => setActiveKey("home")}
      >
        <img src={logoImg} alt="travel logo" height="33.6" className="mr-2" />
        <h3>Travel Scares Me</h3>
      </BootstrapNavbar.Brand>
      <BootstrapNavbar.Toggle aria-controls="navbarResponsive" />
      <BootstrapNavbar.Collapse id="navbarResponsive" className="bg-dark">
        <Nav
          activeKey={activeKey}
          onSelect={(key) => setActiveKey(key)}
          className="mr-auto"
        >
          <Nav.Item className="py-1 px-3 px-md-1">
            <Nav.Link eventKey="about" as={Link} to="/about">
              About
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="py-1 px-3 px-md-1">
            <Nav.Link eventKey="countries" as={Link} to="/countries">
              Countries
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="py-1 px-3 px-md-1">
            <Nav.Link eventKey="routes" as={Link} to="/routes">
              Airplane Routes
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="py-1 px-3 px-md-1">
            <Nav.Link eventKey="articles" as={Link} to="/articles">
              Articles
            </Nav.Link>
          </Nav.Item>
          <Nav.Item className="py-1 px-3 px-md-1">
            <Nav.Link eventKey="visualization" as={Link} to="/visualization">
              Visualizations
            </Nav.Link>
          </Nav.Item>
        </Nav>
        <Form inline onSubmit={preventFormSubmit}>
          <InputGroup className="m-1">
            <FormControl
              name="Global Search"
              onChange={handleOnChange}
              onKeyUp={handleOnKeyUp}
              type="text"
              placeholder="Search website"
              value={searchQuery}
              className="mr-sm-2"
            />
            <InputGroup.Append>
              <Button
                onClick={handleOnSubmit}
                variant="outline-light"
                disabled={!searchQuery}
              >
                <i className="fas fa-search" />
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </BootstrapNavbar.Collapse>
    </BootstrapNavbar>
  );
};

export default Navbar;
