const RANGE_FILTER = {
  type: "range",
  activeMin: false,
  activeMax: false,
  min: "",
  max: "",
};

const SELECT_FILTER = {
  type: "select",
  active: false,
  value: "",
};

const filterStateToJSON = (filters) => {
  const parsedFilters = {};
  filters.forEach((filter) => {
    const filterNameArr = filter.name.split(" ");
    filterNameArr[0] = filterNameArr[0].toLowerCase();
    const filterName = filterNameArr.join("");
    if (filter.type === "select" && filter.active) {
      parsedFilters[filterName] = filter.value;
    } else if (filter.type === "range") {
      if (filter.activeMin) {
        parsedFilters[`${filterName}Min`] = filter.min;
      }
      if (filter.activeMax) {
        parsedFilters[`${filterName}Max`] = filter.max;
      }
    }
  });
  return parsedFilters;
};

export { filterStateToJSON, RANGE_FILTER, SELECT_FILTER };
