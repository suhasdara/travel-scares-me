import React, { useState } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import Filter from "components/Filter/Filter";
import FilterBadge from "components/Filter/FilterBadge";
import FilterRange from "components/Filter/FilterRange";

const FilterModal = ({ filters: oldFilters, onFilterSubmit }) => {
  const [showFilterModal, setShowFilterModal] = useState(false);
  const [filters, setFilters] = useState(oldFilters);
  const [error, setError] = useState(null);

  const updateFilter = (index, key, value) => {
    const newFilters = [...filters];
    const filter = { ...newFilters[index] };
    filter[key] = value;
    if (
      filter.name === error?.name &&
      parseInt(filter.min) <= parseInt(filter.max)
    ) {
      setError(null);
    }
    if (filter.type === "range") {
      if (key === "min") {
        filter.activeMin = true;
      } else if (key === "max") {
        filter.activeMax = true;
      }
    } else if (filter.type === "select") {
      filter.active = true;
    }
    newFilters[index] = filter;
    setFilters(newFilters);
  };

  const clearFilter = (index, key) => {
    const newFilters = [...filters];
    const filter = { ...newFilters[index] };
    filter[key] = "";
    if (filter.name === error?.name) {
      setError(null);
    }
    if (filter.type === "range") {
      if (key === "min") {
        filter.activeMin = false;
      } else if (key === "max") {
        filter.activeMax = false;
      }
    } else if (filter.type === "select") {
      filter.active = false;
    }
    newFilters[index] = filter;
    setFilters(newFilters);
  };

  const applyFilters = () => {
    if (error) {
      return;
    }
    for (const filter of filters) {
      if (
        filter.type === "range" &&
        filter.activeMin &&
        filter.activeMax &&
        parseInt(filter.min) > parseInt(filter.max)
      ) {
        setError({
          name: filter.name,
          message: `${filter.name} has a max less than it's min`,
        });
        return;
      }
    }

    onFilterSubmit(filters);
    setShowFilterModal(false);
  };

  const closeFilterModal = () => {
    setFilters(oldFilters);
    setShowFilterModal(false);
  };

  return (
    <>
      <Button onClick={() => setShowFilterModal(true)} variant="dark">
        <i className="fas fa-filter" /> Change Filters
      </Button>
      {oldFilters.map((filter) => (
        <FilterBadge key={`badge-${filter.name}`} filter={filter} />
      ))}
      <Modal centered show={showFilterModal} onHide={closeFilterModal}>
        <Modal.Header closeButton>Filters</Modal.Header>
        <Modal.Body>
          <Container>
            {filters.map((filter, index) => (
              <Row key={`filter-${filter.name}`} className="mb-4" noGutters>
                <Col xs="4" className="d-inline-flex align-items-center">
                  <b>{filter.name}</b>
                </Col>
                {filter.type === "range" && (
                  <FilterRange
                    filter={filter}
                    updateFilter={(key, value) =>
                      updateFilter(index, key, value)
                    }
                    clearFilter={(key) => clearFilter(index, key)}
                  />
                )}
                {filter.type === "select" && (
                  <Filter
                    filter={filter}
                    updateFilter={(value) =>
                      updateFilter(index, "value", value)
                    }
                    clearFilter={(key) => clearFilter(index, key)}
                  />
                )}
              </Row>
            ))}
            {error && <Row className="text-danger">{error.message}</Row>}
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={applyFilters} variant="dark">
            Apply
          </Button>
          <Button onClick={closeFilterModal} variant="dark">
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default FilterModal;
