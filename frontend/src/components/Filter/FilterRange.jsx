import React from "react";
import { Col, Row } from "react-bootstrap";

const FilterRange = ({ filter, updateFilter, clearFilter }) => {
  return (
    <Col xs="12" className="d-inline-flex align-items-center">
      <Row noGutters style={{ width: "100%" }}>
        <Col xs="5">
          Min:
          <input
            className="ml-1"
            name={`${filter.name} Min`}
            type="number"
            value={filter.min}
            min={filter.absoluteMin}
            max={filter.absoluteMax}
            style={{ width: "95%" }}
            onChange={(e) => updateFilter("min", e.target.value)}
          />
        </Col>
        <Col xs="1">
          <br />
          {filter.activeMin && (
            <i
              className="fas fa-trash btn-icon ml-1"
              onClick={() => clearFilter("min")}
            />
          )}
        </Col>
        <Col xs="5">
          <div className="d-inline">
            Max:
            <input
              className="ml-1"
              name={`${filter.name} Max`}
              type="number"
              value={filter.max}
              min={filter.absoluteMin}
              max={filter.absoluteMax}
              style={{ width: "95%" }}
              onChange={(e) => updateFilter("max", e.target.value)}
            />
          </div>
        </Col>
        <Col xs="1">
          <br />
          {filter.activeMax && (
            <i
              className="fas fa-trash btn-icon ml-1"
              onClick={() => clearFilter("max")}
            />
          )}
        </Col>
      </Row>
    </Col>
  );
};

export default FilterRange;
