import React, { useEffect, useState } from "react";
import { Col, Dropdown } from "react-bootstrap";

const Filter = ({ filter, updateFilter, clearFilter }) => {
  const options = filter.options;
  const [query, setQuery] = useState("");
  const [selectedOption, setSelectedOption] = useState(filter.value);
  const [currentOptions, setCurrentOptions] = useState(options);

  const filterOptions = (newQuery) => {
    const queryRegex = new RegExp(`^${newQuery}`, "i");
    const newOptions = options.filter((option) => queryRegex.test(option));
    setQuery(newQuery);
    setCurrentOptions(newOptions);
  };

  useEffect(() => {
    setQuery("");
    setSelectedOption(filter.value);
  }, [filter.value]);

  return (
    <>
      <Col xs="7" className="d-inline-flex justify-content-center">
        <Dropdown
          alignRight
          className="d-inline-flex"
          style={{ maxHeight: "40px" }}
        >
          <Dropdown.Toggle variant="dark">
            <input
              className="border-0"
              name={filter.name}
              type="text"
              value={query ? query : selectedOption}
              onChange={(e) => filterOptions(e.target.value)}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  if (currentOptions.length > 0) {
                    setSelectedOption(currentOptions[0]);
                    updateFilter(currentOptions[0]);
                  }
                  setQuery("");
                  setCurrentOptions(options);
                }
              }}
            />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {currentOptions.map((option, idx) => (
              <Dropdown.Item
                key={option}
                active={query ? idx === 0 : option === selectedOption}
                onClick={() => {
                  setSelectedOption(option);
                  updateFilter(option);
                  setQuery("");
                  setCurrentOptions(options);
                }}
              >
                {option}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </Col>
      <Col xs="1" className="d-inline-flex align-items-center">
        {filter.active && (
          <i
            className="fas fa-trash btn-icon ml-1"
            onClick={() => clearFilter("value")}
          />
        )}
      </Col>
    </>
  );
};

export default Filter;
