import React from "react";
import { Badge } from "react-bootstrap";

const FilterBadge = ({ filter }) => {
  if (filter.type === "range") {
    return (
      <>
        {filter.activeMin && (
          <Badge pill className="ml-1">
            Min {filter.name}: {filter.min}
          </Badge>
        )}
        {filter.activeMax && (
          <Badge pill className="ml-1">
            Max {filter.name}: {filter.max}
          </Badge>
        )}
      </>
    );
  }
  if (filter.type === "select") {
    return (
      <>
        {filter.active && (
          <Badge pill className="ml-1">
            {filter.name}: {filter.value}
          </Badge>
        )}
      </>
    );
  }
  return null;
};

export default FilterBadge;
