import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import travelImg from "assets/images/travel.svg";
import worldImg from "assets/images/world.svg";
import newspaperImg from "assets/images/newspaper.svg";
import mapImg from "assets/images/map.svg";
import "assets/styles/Splash.css";

const Splash = () => {
  return (
    <Container fluid className="splash-page">
      <Row className="py-5 align-items-center justify-content-center">
        <Col xs="6" md="4">
          <h4>Your solution for mitigating travel risk.</h4>
        </Col>
        <Col xs="6" md="3">
          <img
            src={travelImg}
            alt="airplane going over a globe"
            className="w-100"
          />
        </Col>
      </Row>
      <Row className="py-5 align-items-center justify-content-center text-right">
        <Col xs="6" md="3">
          <img src={worldImg} alt="globe wearing a mask" height="200px" />
        </Col>
        <Col xs="6" md="4">
          <h4>
            Find all the information you need about your destination, from COVID
            data to threat levels.
          </h4>
          <Link to="/countries">View countries</Link>
        </Col>
      </Row>
      <Row className="py-5 align-items-center justify-content-center">
        <Col xs="6" md="4">
          <h4>
            Inform yourself on all the latest occurrences of your destination.
          </h4>
          <Link to="/articles">View news articles</Link>
        </Col>
        <Col xs="6" md="3">
          <img src={newspaperImg} alt="newspaper" height="200px" />
        </Col>
      </Row>
      <Row className="py-5 align-items-center justify-content-center text-right">
        <Col xs="6" md="3">
          <img src={mapImg} alt="map with airplane over it" height="200px" />
        </Col>
        <Col xs="6" md="4">
          <h4>Plan out your travel routes to your selected destinations.</h4>
          <Link to="/routes">View airplane routes</Link>
        </Col>
      </Row>
      <Row className="align-items-center justify-content-center text-center bg-dark">
        <Link to="/about" className="mr-1">
          About Us
        </Link>
        |
        <a
          href="https://gitlab.com/suhasdara/travel-scares-me"
          className="ml-1"
        >
          GitLab Repository
        </a>
      </Row>
    </Container>
  );
};

export default Splash;
