import { SELECT_FILTER } from "components/Filter/utils";
import airlines from "data/filter_route_airline.json";
import arrivalCities from "data/filter_route_arrival_city.json";
import arrivalCountries from "data/filter_route_arrival_country.json";
import departureCities from "data/filter_route_departure_city.json";
import departureCountries from "data/filter_route_departure_country.json";

const AIRPLANE_ROUTE_FILTERS = [
  {
    ...SELECT_FILTER,
    name: "Airline",
    options: airlines,
  },
  {
    ...SELECT_FILTER,
    name: "Arrival City",
    options: arrivalCities,
  },
  {
    ...SELECT_FILTER,
    name: "Arrival Country",
    options: arrivalCountries,
  },
  {
    ...SELECT_FILTER,
    name: "Departure City",
    options: departureCities,
  },
  {
    ...SELECT_FILTER,
    name: "Departure Country",
    options: departureCountries,
  },
];

export default AIRPLANE_ROUTE_FILTERS;
