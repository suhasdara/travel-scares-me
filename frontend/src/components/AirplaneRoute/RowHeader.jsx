import React from "react";

const RowHeader = ({ title, active, asc, onChange, sortKey }) => {
  return (
    <th
      className="cursor-pointer sort-hover"
      onClick={() => onChange(sortKey, active ? !asc : true)}
      title={`Sort table by ${title}`}
    >
      {title}{" "}
      {active && <i className={`fas fa-arrow-${asc ? "up" : "down"}`} />}
    </th>
  );
};

export default RowHeader;
