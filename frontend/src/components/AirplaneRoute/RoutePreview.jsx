import React from "react";

import { Link } from "react-router-dom";
import TextHighlight from "components/GlobalSearch/TextHighlight";

const LinkedTd = ({ children, link }) => (
  <td className="position-relative">
    <Link className="position-absolute w-100 h-100" to={link} />
    {children}
  </td>
);

const RoutePreview = ({ routeData, searchQuery }) => {
  const searchWords = searchQuery ? [searchQuery] : [];
  const flightName = routeData.airline_code + " " + routeData.flight_num;
  const routeLink = `/routes/${routeData.id}`;

  return (
    <tr className="route-row cursor-pointer">
      <LinkedTd link={routeLink}>
        <TextHighlight searchWords={searchWords} textToHighlight={flightName} />
      </LinkedTd>
      <LinkedTd link={routeLink}>
        <TextHighlight
          searchWords={searchWords}
          textToHighlight={routeData.departure_city}
        />
      </LinkedTd>
      <LinkedTd link={routeLink}>
        <TextHighlight
          searchWords={searchWords}
          textToHighlight={routeData.departure_country}
        />
      </LinkedTd>
      <LinkedTd link={routeLink}>
        <TextHighlight
          searchWords={searchWords}
          textToHighlight={routeData.arrival_city}
        />
      </LinkedTd>
      <LinkedTd link={routeLink}>
        <TextHighlight
          searchWords={searchWords}
          textToHighlight={routeData.arrival_country}
        />
      </LinkedTd>
    </tr>
  );
};

export default RoutePreview;
