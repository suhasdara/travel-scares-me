import React, { useState } from "react";
import { Table } from "react-bootstrap";
import { useQuery } from "react-query";
import RoutePreview from "components/AirplaneRoute/RoutePreview";
import Pagination from "components/Pagination";
import FilterModal from "components/Filter/FilterModal";
import RowHeader from "components/AirplaneRoute/RowHeader";
import SearchBar from "components/Search/SearchBar";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import AIRPLANE_ROUTE_FILTERS from "components/AirplaneRoute/AirplaneRouteFilters";
import { filterStateToJSON } from "components/Filter/utils";
import { getRoutes } from "api/routes";

const DEFAULT_PAGE_SIZE = 40;
const COLUMNS = [
  { title: "Flight number", sortKey: "flight" },
  { title: "Departure city", sortKey: "departure_city" },
  { title: "Departure country", sortKey: "departure_country" },
  { title: "Arrival city", sortKey: "arrival_city" },
  { title: "Arrival country", sortKey: "arrival_country" },
];

const AirplaneRoutes = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(DEFAULT_PAGE_SIZE);
  const [filters, setFilters] = useState(AIRPLANE_ROUTE_FILTERS);
  const [orderBy, setOrderBy] = useState("flight");
  const [ascending, setAscending] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");
  const parsedFilters = filterStateToJSON(filters);
  parsedFilters.orderBy = orderBy.toLowerCase();
  parsedFilters.ordering = ascending ? "asc" : "desc";
  parsedFilters.q = searchQuery;
  const { isLoading, data } = useQuery(
    ["routes", page, pageSize, parsedFilters],
    () => getRoutes(page, pageSize, parsedFilters)
  );

  return (
    <div className="p-5">
      <div className="d-flex mb-1">
        <h1>Airplane Routes</h1>
      </div>
      <div className="d-flex mb-2">
        <FilterModal filters={filters} onFilterSubmit={setFilters} />
        <SearchBar
          name="Airplane Routes Search"
          setSearchQuery={setSearchQuery}
          className="ml-auto"
        />
      </div>
      <hr className="mb-3 thick-hr" />
      {isLoading ? (
        <GlobalSpinner />
      ) : data && data.routes.length > 0 ? (
        <Pagination
          numResults={data.count}
          pageSize={pageSize}
          currPage={page}
          onPageClick={setPage}
          handlePageSizeChange={setPageSize}
        >
          <Table bordered hover variant="dark" className="routes-table">
            <thead>
              <tr>
                {COLUMNS.map(({ title, sortKey }) => (
                  <RowHeader
                    key={title}
                    sortKey={sortKey}
                    title={title}
                    active={sortKey === orderBy}
                    asc={ascending}
                    onChange={(key, asc) => {
                      setOrderBy(key);
                      setAscending(asc);
                    }}
                  />
                ))}
              </tr>
            </thead>
            <tbody>
              {data.routes.map((route) => (
                <RoutePreview
                  key={`route-${route.id}`}
                  routeData={route}
                  searchQuery={searchQuery}
                />
              ))}
            </tbody>
          </Table>
        </Pagination>
      ) : (
        <p className="d-flex justify-content-center">
          <b>No data available</b>
        </p>
      )}
    </div>
  );
};

export default AirplaneRoutes;
