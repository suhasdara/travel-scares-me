import React from "react";
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getRoute } from "api/routes";

const AirplaneRoute = ({ routeId }) => {
  const { isLoading, data } = useQuery(["route", routeId], () =>
    getRoute(routeId)
  );

  const mapsDepartureUrl = data?.departure.maps_url;
  const mapsArrivalUrl = data?.arrival.maps_url;

  return (
    <div className="px-5 pt-5 page-div">
      {isLoading ? (
        <GlobalSpinner />
      ) : (
        <div>
          <h1>
            {data?.airline} Flight {data?.flight_num}
          </h1>
          <p>
            <b>Airline country: </b> {data?.airline_country}
          </p>
          <Row>
            <Col xs="4">
              <h3>Departure</h3>
              <img
                src={data?.departure.country_flag_url}
                alt={`${data?.departure.country} flag`}
                className="mb-3 route-flag"
                height="150px"
              />
              <p>
                <b>Country: </b>
                <Link to={`/countries/${data?.departure.country_id}`}>
                  {data?.departure.country}
                </Link>
              </p>
              <p>
                <b>City:</b> {data?.departure.city} ({data?.departure.city_code}
                )
              </p>
              <p>
                <b>Timezone:</b> {data?.departure.time_zone}
              </p>
              <br />
              <p>
                <b>Airport:</b> {data?.departure.airport} (
                {data?.departure.airport_code})
              </p>
              <p>
                <b>Airport Coordinates:</b> {data?.departure.latitude}°{", "}
                {data?.departure.longitude}°
              </p>
              <p>
                <b>Terminal:</b>
                {data?.departure.terminal ? (
                  <span> {data.departure.terminal}</span>
                ) : (
                  <span> Unavailable</span>
                )}
              </p>
              <p>
                <b>Time:</b>
                {data?.departure.time ? (
                  <span> {data.departure.time}</span>
                ) : (
                  <span> Unavailable</span>
                )}
              </p>
              <iframe
                title={`${data?.departure.airport} map`}
                src={mapsDepartureUrl}
                width="80%"
                height="60%"
                loading="lazy"
              />
            </Col>
            <Col xs="4">
              <h3>Arrival</h3>
              <img
                src={data?.arrival.country_flag_url}
                alt={`${data?.arrival.country} flag`}
                className="mb-3 route-flag"
                height="150px"
              />
              <p>
                <b>Country: </b>
                <Link to={`/countries/${data?.arrival.country_id}`}>
                  {data?.arrival.country}
                </Link>
              </p>
              <p>
                <b>City:</b> {data?.arrival.city} ({data?.arrival.city_code})
              </p>
              <p>
                <b>Timezone:</b> {data?.arrival.time_zone}
              </p>
              <br />
              <p>
                <b>Airport:</b> {data?.arrival.airport} (
                {data?.arrival.airport_code})
              </p>
              <p>
                <b>Airport Coordinates:</b> {data?.arrival.latitude}°{", "}
                {data?.arrival.longitude}°
              </p>
              <p>
                <b>Terminal:</b>
                {data?.arrival.terminal ? (
                  <span> {data.arrival.terminal}</span>
                ) : (
                  <span> Unavailable</span>
                )}
              </p>
              <p>
                <b>Time:</b>
                {data?.arrival.time ? (
                  <span> {data.arrival.time}</span>
                ) : (
                  <span> Unavailable</span>
                )}
              </p>
              <iframe
                title={`${data?.arrival.airport} map`}
                src={mapsArrivalUrl}
                width="80%"
                height="60%"
                loading="lazy"
              />
            </Col>
            <Col xs="4">
              <h4>News associated with {data?.arrival.country}:</h4>
              {data?.related_articles.length > 0 ? (
                <ul>
                  {data?.related_articles.map((news) => (
                    <li key={news.id}>
                      <Link to={`/articles/${news.id}`}>{news.title}</Link>
                    </li>
                  ))}
                </ul>
              ) : (
                <p>Unavailable</p>
              )}
            </Col>
          </Row>
        </div>
      )}
    </div>
  );
};

export default AirplaneRoute;
