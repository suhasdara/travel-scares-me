import React from "react";
import { Chart } from "react-google-charts";
import GlobalSpinner from "components/Utils/GlobalSpinner";

const CovidChart = (props) => {
  let yes = props.metric;
  let no = 100 - props.metric;

  return (
    <Chart
      width={"100%"}
      height={"100%"}
      chartType="PieChart"
      loader={<GlobalSpinner />}
      data={[
        ["Value", "Percentage"],
        ["Yes", yes],
        ["No", no],
      ]}
      options={{
        title: props.title,
        is3D: true,
        legend: { position: "none" },
        tooltip: { text: "percentage" },
        pieSliceText: "label",
        slices: {
          0: { color: props.yesGood ? "green" : "red" },
          1: { color: props.yesGood ? "red" : "green" },
        },
      }}
    />
  );
};

export default CovidChart;
