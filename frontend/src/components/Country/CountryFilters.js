import { RANGE_FILTER, SELECT_FILTER } from "components/Filter/utils";
import regions from "data/filter_country_region.json";

const COUNTRY_FILTERS = [
  {
    ...RANGE_FILTER,
    name: "Population",
    absoluteMin: 0,
  },
  {
    ...RANGE_FILTER,
    name: "Latitude",
    absoluteMin: -90,
    absoluteMax: 90,
  },
  {
    ...RANGE_FILTER,
    name: "Longitude",
    absoluteMin: -180,
    absoluteMax: 180,
  },
  {
    ...SELECT_FILTER,
    name: "Region",
    options: regions,
  },
];

export default COUNTRY_FILTERS;
