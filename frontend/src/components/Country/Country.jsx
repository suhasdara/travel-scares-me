import React from "react";
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import parse from "html-react-parser";
import CovidChart from "components/Country/CovidChart";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import { useQuery } from "react-query";
import { getCountry } from "api/countries";

import "assets/styles/Countries.css";

const Country = ({ countryId }) => {
  const { isLoading, data } = useQuery(["country", countryId], () =>
    getCountry(countryId)
  );

  return (
    <div className="px-5 pt-5 page-div">
      {isLoading ? (
        <GlobalSpinner />
      ) : (
        <div>
          <h1>{data?.name}</h1>
          <Row>
            <Col xs="4">
              <Row>
                <Col>
                  <img
                    src={data?.flag_url}
                    alt={`${data?.name} flag`}
                    className="mb-3"
                    width="80%"
                  />
                </Col>
                <Col>
                  <iframe
                    title={`${data?.name} map`}
                    src={data?.maps_url}
                    width="80%"
                    height="80%"
                    loading="lazy"
                  />
                </Col>
              </Row>
              <p>
                <b>Country Code:</b> {data?.code}
              </p>
              <p>
                <b>Coordinates:</b> {data?.latitude}°{", "}
                {data?.longitude}°
              </p>
              <p>
                <b>Region:</b> {data?.region}
              </p>
              <p>
                <b>Capital:</b> {data?.capital}
              </p>
              <p>
                <b>Population:</b> {data?.population.toLocaleString()}
              </p>
              <p>
                <b>Languages spoken: </b>
                {data?.languages.map((language, index) => (
                  <span key={index}>{(index ? ", " : "") + language}</span>
                ))}
              </p>
              <p>
                <b>Currencies used: </b>
                {data?.currencies.map((currency, index) => (
                  <span key={index}>{(index ? ", " : "") + currency}</span>
                ))}
              </p>
              <p>
                <b>Timezones: </b>
                {data?.timezones.map((timezone, index) => (
                  <span key={index}>{(index ? ", " : "") + timezone}</span>
                ))}
              </p>
              {data?.neighbors.length > 0 && (
                <div>
                  <b>Neighboring countries: </b>
                  <ul>
                    {data.neighbors.map((neighbor, index) => (
                      <li key={index}>
                        <Link to={`/countries/${neighbor.id}`}>
                          {neighbor.name}
                        </Link>
                      </li>
                    ))}
                  </ul>
                </div>
              )}
              <b>COVID-19 Data:</b>
              {data?.covid &&
              !Object.values(data?.covid).every((o) => o === null) ? (
                <div>
                  <p>
                    Cases:{" "}
                    {data.covid.cases && (
                      <span>{data.covid.cases.toLocaleString()}</span>
                    )}
                    , Deaths:{" "}
                    {data.covid.deaths && (
                      <span>{data.covid.deaths.toLocaleString()}</span>
                    )}
                  </p>
                  <Row className="covid-box">
                    {data.covid.percent_covid_symptoms && (
                      <Col xs="6">
                        <CovidChart
                          metric={data.covid.percent_covid_symptoms}
                          title="Percentage of people with symptoms"
                          yesGood={false}
                        />
                      </Col>
                    )}
                  </Row>
                  <Row className="covid-box">
                    {data.covid.percent_vaccinated && (
                      <Col xs="6">
                        <CovidChart
                          metric={data.covid.percent_vaccinated}
                          title="Percentage of people vaccinated"
                          yesGood={true}
                        />
                      </Col>
                    )}
                  </Row>
                  <Row className="covid-box">
                    {data.covid.percent_mask && (
                      <Col xs="6">
                        <CovidChart
                          metric={data.covid.percent_mask}
                          title="Percentage of people wearing masks"
                          yesGood={true}
                        />
                      </Col>
                    )}
                  </Row>
                </div>
              ) : (
                <p>Unavailable</p>
              )}
            </Col>
            <Col xs="4">
              <h4>
                Travel advisories{" "}
                {data?.threat_level && (
                  <span>(threat level {data.threat_level})</span>
                )}
                :
              </h4>
              {data?.travel_advisories ? (
                <div>{parse(data.travel_advisories)}</div>
              ) : (
                <p>Unavailable</p>
              )}
            </Col>
            <Col xs="4">
              <h4>Related News:</h4>
              {data?.related_articles.length > 0 ? (
                <ul>
                  {data.related_articles.map((article) => (
                    <li key={article.id}>
                      <Link to={`/articles/${article.id}`}>
                        {article.title}
                      </Link>
                    </li>
                  ))}
                </ul>
              ) : (
                <p>Unavailable</p>
              )}
              <h4>Flights from {data?.name}:</h4>
              {data?.related_routes.length > 0 ? (
                <ul>
                  {data.related_routes.map((route) => (
                    <li key={route.id}>
                      To{" "}
                      <Link to={`/routes/${route.id}`}>
                        {route.destination}
                      </Link>
                    </li>
                  ))}
                </ul>
              ) : (
                <p>Unavailable</p>
              )}
            </Col>
          </Row>
        </div>
      )}
    </div>
  );
};

export default Country;
