import React from "react";

import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import TextHighlight from "components/GlobalSearch/TextHighlight";

const CountryPreview = ({ countryData, searchQuery }) => {
  const searchWords = searchQuery ? [searchQuery] : [];
  const coordinates =
    countryData.latitude + "°, " + countryData.longitude + "°";

  return (
    <Card className="hover">
      <Card.Img src={countryData.flag_url} variant="top" />
      <Card.Body>
        <Card.Title>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={countryData.name}
          />
        </Card.Title>
        <Card.Text>
          <b>Country Code: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={countryData.code}
          />
          <br />

          <b>Region: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={countryData.region}
          />
          <br />

          <b>Coordinates: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={coordinates}
          />
          <br />

          <b>Population: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={countryData.population.toLocaleString()}
          />
          <br />

          <b>Capital: </b>
          <TextHighlight
            searchWords={searchWords}
            textToHighlight={countryData.capital}
          />
        </Card.Text>
      </Card.Body>
      <Link to={`/countries/${countryData.id}`} className="stretched-link" />
    </Card>
  );
};

export default CountryPreview;
