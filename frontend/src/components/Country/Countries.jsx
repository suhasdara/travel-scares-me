import React, { useState } from "react";
import { useQuery } from "react-query";
import { CardDeck } from "react-bootstrap";
import Pagination from "components/Pagination";
import CountryPreview from "components/Country/CountryPreview";
import FilterModal from "components/Filter/FilterModal";
import Sort from "components/Sort/Sort";
import SearchBar from "components/Search/SearchBar";
import GlobalSpinner from "components/Utils/GlobalSpinner";
import COUNTRY_FILTERS from "components/Country/CountryFilters";
import { filterStateToJSON } from "components/Filter/utils";
import { getCountries } from "api/countries";

import "assets/styles/Models.css";

const DEFAULT_PAGE_SIZE = 20;
const SORT_COLUMNS = [
  "Name",
  "Code",
  "Region",
  "Population",
  "Capital",
  "Latitude",
  "Longitude",
];

const Countries = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(DEFAULT_PAGE_SIZE);
  const [filters, setFilters] = useState(COUNTRY_FILTERS);
  const [orderBy, setOrderBy] = useState(SORT_COLUMNS[0]);
  const [ascending, setAscending] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");
  const parsedFilters = filterStateToJSON(filters);
  parsedFilters.orderBy = orderBy.toLowerCase();
  parsedFilters.ordering = ascending ? "asc" : "desc";
  parsedFilters.q = searchQuery;
  const { isLoading, data } = useQuery(
    ["countries", page, pageSize, parsedFilters],
    () => getCountries(page, pageSize, parsedFilters)
  );

  return (
    <div className="p-5">
      <div className="d-flex mb-1">
        <h1>Countries</h1>
      </div>
      <div className="d-flex mb-2">
        <FilterModal filters={filters} onFilterSubmit={setFilters} />
        <Sort
          columns={SORT_COLUMNS}
          orderBy={orderBy}
          onOrderByChange={setOrderBy}
          ascending={ascending}
          onAscendingChange={setAscending}
        />
        <SearchBar
          name="Countries Search"
          setSearchQuery={setSearchQuery}
          className="ml-2"
        />
      </div>
      <hr className="mb-3 thick-hr" />
      {isLoading ? (
        <GlobalSpinner />
      ) : data && data.countries.length > 0 ? (
        <Pagination
          numResults={data.count}
          pageSize={pageSize}
          currPage={page}
          onPageClick={setPage}
          handlePageSizeChange={setPageSize}
        >
          <CardDeck className="d-flex justify-content-center models">
            {data.countries.map((country) => (
              <CountryPreview
                key={`country-${country.code}`}
                countryData={country}
                searchQuery={searchQuery}
              />
            ))}
          </CardDeck>
        </Pagination>
      ) : (
        <p className="d-flex justify-content-center">
          <b>No data available</b>
        </p>
      )}
    </div>
  );
};

export default Countries;
