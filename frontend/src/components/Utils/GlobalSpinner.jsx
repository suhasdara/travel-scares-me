import React from "react";
import { Spinner } from "react-bootstrap";

const GlobalSpinner = ({ className }) => {
  className = className ? " " + className : "";

  return (
    <div
      className={
        "page-spinner d-flex justify-content-center align-items-center" +
        className
      }
    >
      <Spinner animation="border" />
    </div>
  );
};

export default GlobalSpinner;
