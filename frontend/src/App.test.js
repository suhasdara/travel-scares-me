import React from "react";
import App from "App";
import { renderWithRouter as r } from "tests/utils";

const render = () => r(<App />);

describe("App", () => {
  it("displays the project name", () => {
    const { getByText } = render();

    expect(getByText(/Travel Scares Me/)).toBeInTheDocument();
  });
});
