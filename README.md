# Travel Scares Me

In the current era of COVID-19, travel has become very unsafe and has been discouraged if it is not necessary. We aim to bring the latest information about airline routes between different countries, what travel advisories are currently imposed on these countries, and the latest news about these countries. We also provide the latest COVID-19 statistical data and people's behavioral data to make travel decisions easier.

## Members

| Name            | EID     | Gitlab ID       |
| --------------- | ------- | --------------- |
| Matthew Bianchi | mjb4937 | @bianchima      |
| Peter Cardenas  | pc27234 | @peter-cardenas |
| Suhas Dara      | sd35633 | @suhasdara      |
| Joseph Jiang    | jkj876  | @josephjiang123 |

## Phases

| Phase   | Leader          | Git SHA                                  |
| ------- | --------------- | ---------------------------------------- |
| Phase 1 | Suhas Dara      | ae63d57b7b69e479f5e3b486c41dc94b8d3bb37f |
| Phase 2 | Peter Cardenas  | 28e9d7a432a9d9e9deaeb489149dee2a8d8686df |
| Phase 3 | Matthew Bianchi | b1c3f2874fdcb1278bb576817b79fea995537645 |
| Phase 4 | Joseph Jiang    | ee5caae740e63caa6fda95e91216c4628f859aa2 |

## Website

https://www.travelscares.me

## Postman documentation

https://documenter.getpostman.com/view/14734911/Tz5jeL2c

## Final presentation video

https://www.youtube.com/watch?v=fWFjnH2Fas4

## Pipelines

https://gitlab.com/suhasdara/travel-scares-me/-/pipelines

## Estimated and actual completion times for each member (hours)

### Phase 1

| Member          | Estimated | Actual |
| --------------- | --------- | ------ |
| Matthew Bianchi | 8         | 8      |
| Peter Cardenas  | 12        | 12     |
| Suhas Dara      | 12        | 20     |
| Joseph Jiang    | 10        | 10     |

### Phase 2

| Member          | Estimated | Actual |
| --------------- | --------- | ------ |
| Matthew Bianchi | 20        | 20     |
| Peter Cardenas  | 20        | 25     |
| Suhas Dara      | 25        | 55     |
| Joseph Jiang    | 20        | 20     |

### Phase 3

| Member          | Estimated | Actual |
| --------------- | --------- | ------ |
| Matthew Bianchi | 15        | 15     |
| Peter Cardenas  | 20        | 18     |
| Suhas Dara      | 20        | 45     |
| Joseph Jiang    | 15        | 15     |

### Phase 4

| Member          | Estimated | Actual |
| --------------- | --------- | ------ |
| Matthew Bianchi |  3        |  3     |
| Peter Cardenas  |  3        |  4     |
| Suhas Dara      | 12        | 18     |
| Joseph Jiang    |  5        |  3     |

## Comments

We used some inspiration from some of the previous semester projects including Texas Votes and Burnin' Up.
