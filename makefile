# BACKEND
build-backend:
	cd backend; \
	docker build -t travelscaresme-backend .

start-backend:
	cd backend; \
	docker run -it --rm --name travelscaresme-backend -p 8080:8080 travelscaresme-backend

stop-backend:
	docker rm --force travelscaresme-backend

deploy-backend:
	cd scripts && ./deploy_backend.sh

#FRONTEND
build-frontend:
	cd frontend && npm install && npm build
	
start-frontend:
	cd frontend && npm start

deploy-frontend:
	cd scripts && ./deploy_frontend.sh
	
#TESTS
python-unit-tests:
	python3 backend/python_tests.py

postman-tests:
	newman run postman_tests.json
	
jest-unit-tests:
	cd frontend && CI=true npm test

splinter-gui-tests:
	cd frontend/src/tests/gui && python3 Test.py

#FORMAT
format:
	black backend/
	black frontend/src/tests/gui/
	cd frontend && npx prettier --write .

