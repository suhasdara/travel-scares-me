echo "Deploying Frontend..."
cd ../frontend
export REACT_APP_API_URL=/api
npm run build
aws s3 sync build/ s3://travelscaresme-frontend
aws cloudfront create-invalidation --distribution-id "E3K9RR985136ZA" --paths "/index.html"
