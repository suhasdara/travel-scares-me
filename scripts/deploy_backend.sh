echo "Deploying Backend..."
cd ../backend
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 223861603201.dkr.ecr.us-east-2.amazonaws.com
docker build -t travelscaresme-backend .
docker tag travelscaresme-backend:latest 223861603201.dkr.ecr.us-east-2.amazonaws.com/travelscaresme-backend:latest
docker push 223861603201.dkr.ecr.us-east-2.amazonaws.com/travelscaresme-backend:latest
cd aws_deploy
eb deploy
