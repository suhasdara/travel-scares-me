from utils import get_sort_direction, get_route_param
from models import Route
from sqlalchemy import or_


def format_airport(route, airport, arrival=True):
    airport["airport_code"] = airport.pop("code")
    airport["country_id"] = airport["country_id"]["id"]
    airport.pop("id")
    if arrival:
        airport["time"] = route.pop("arrival_time")
        airport["terminal"] = route.pop("arrival_terminal")
    else:
        airport["time"] = route.pop("departure_time")
        airport["terminal"] = route.pop("departure_terminal")


def format_route(route):
    route["arrival"] = route.pop("arrival_airport")
    route["departure"] = route.pop("departure_airport")
    route.pop("arrival_country_name")
    route.pop("departure_country_name")
    format_airport(route, route["arrival"], arrival=True)
    format_airport(route, route["departure"], arrival=False)


def format_multiple_routes(routes):
    new_routes = []
    for route in routes:
        new_route = {
            "id": route["id"],
            "departure_city": route["departure_airport"]["city"],
            "arrival_city": route["arrival_airport"]["city"],
            "departure_country": route["departure_country"]["name"],
            "arrival_country": route["arrival_country"]["name"],
            "flight_num": route["flight_num"],
            "airline_code": route["airline_code"],
        }
        new_routes += (new_route,)
    return new_routes


def sort_routes(query, order_by, ordering, dep_airport, arr_airport):
    if order_by is not None and ordering is not None:
        if order_by == "flight":
            query = query.order_by(
                get_sort_direction(Route.airline_code, ordering),
                get_sort_direction(Route.flight_num, ordering),
            )
        elif order_by == "departure_city":
            query = query.order_by(get_sort_direction(dep_airport.city, ordering))
        elif order_by == "arrival_city":
            query = query.order_by(get_sort_direction(arr_airport.city, ordering))
        elif order_by == "departure_country":
            query = query.order_by(get_sort_direction(dep_airport.country, ordering))
        elif order_by == "arrival_country":
            query = query.order_by(get_sort_direction(arr_airport.country, ordering))
        else:
            raise ValueError
        return query
    elif order_by is None and ordering is None:
        return query
    else:
        raise ValueError


def filter_routes(query, args, dep_airport, arr_airport):
    airline = get_route_param(args, "airline")
    dep_city = get_route_param(args, "departureCity")
    dep_country = get_route_param(args, "departureCountry")
    arr_city = get_route_param(args, "arrivalCity")
    arr_country = get_route_param(args, "arrivalCountry")

    if airline is not None:
        query = query.filter(Route.airline_code == airline)
    if arr_city is not None:
        query = query.filter(arr_airport.city == arr_city)
    if dep_city is not None:
        query = query.filter(dep_airport.city == dep_city)
    if arr_country is not None:
        query = query.filter(arr_airport.country == arr_country)
    if dep_country is not None:
        query = query.filter(dep_airport.country == dep_country)

    return query


def search_routes(query, q, dep_airport, arr_airport):
    if q is None:
        return query

    word = q.strip().lower()
    search_queries = []
    search_queries.append(Route.airline_code.contains(word))
    search_queries.append(Route.airline.contains(word))
    search_queries.append(Route.flight_num.contains(word))
    search_queries.append(dep_airport.country.contains(word))
    search_queries.append(dep_airport.city.contains(word))
    search_queries.append(dep_airport.airport.contains(word))
    search_queries.append(dep_airport.code.contains(word))
    search_queries.append(dep_airport.time_zone.contains(word))
    search_queries.append(Route.departure_time.contains(word))
    search_queries.append(Route.departure_terminal.contains(word))
    search_queries.append(arr_airport.country.contains(word))
    search_queries.append(arr_airport.city.contains(word))
    search_queries.append(arr_airport.airport.contains(word))
    search_queries.append(arr_airport.code.contains(word))
    search_queries.append(arr_airport.time_zone.contains(word))
    search_queries.append(Route.arrival_time.contains(word))
    search_queries.append(Route.arrival_terminal.contains(word))

    query = query.filter(or_(*search_queries))
    return query
