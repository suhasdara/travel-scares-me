def args_to_dict(args):
    d = args.to_dict(flat=False)
    d = {k: (v if len(v) > 1 else v[0]) for k, v in d.items()}
    return d


def get_route_param(args, name):
    try:
        return args[name]
    except KeyError:
        return None


def get_sort_direction(attr, ordering):
    if ordering == "asc":
        return attr
    elif ordering == "desc":
        return attr.desc()
    else:
        raise ValueError
