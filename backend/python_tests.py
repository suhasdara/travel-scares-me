from unittest import main, TestCase
import app
import requests
import json


api_base = "http://localhost:8080/api"
# api_base = "https://travelscares.me/api"


class python_tests(TestCase):
    # Countries API Test

    country_response = requests.get("{}/countries/3".format(api_base))
    c_json = country_response.json()

    def test_countries_response_200(self):
        self.assertEqual(self.country_response.status_code, 200)

    def test_countries_capital(self):
        self.assertTrue("capital" in self.c_json.keys())

    def test_countries_code(self):
        self.assertTrue("code" in self.c_json.keys())

    def test_countries_covid(self):
        self.assertTrue("covid" in self.c_json.keys())

    def test_countries_currencies(self):
        self.assertTrue("currencies" in self.c_json.keys())
        self.assertGreater(len(self.c_json.get("currencies")), 0)

    def test_countries_flag_url(self):
        self.assertTrue("flag_url" in self.c_json.keys())

    def test_countries_id(self):
        self.assertTrue("id" in self.c_json.keys())

    def test_countries_languages(self):
        self.assertTrue("languages" in self.c_json.keys())
        self.assertGreater(len(self.c_json.get("languages")), 0)

    # Routes API Test

    routes_response = requests.get("{}/routes/3".format(api_base))
    r_json = routes_response.json()

    def test_routes_response_200(self):
        self.assertEqual(self.routes_response.status_code, 200)

    def test_routes_airline(self):
        self.assertTrue("airline" in self.r_json.keys())

    def test_routes_airline_code(self):
        self.assertTrue("airline_code" in self.r_json.keys())

    def test_routes_airline_country(self):
        self.assertTrue("airline_country" in self.r_json.keys())

    def test_routes_arrival(self):
        self.assertTrue("arrival" in self.r_json.keys())
        arrival = self.r_json.get("arrival")
        self.assertTrue("airport" in arrival.keys())
        self.assertTrue("airport_code" in arrival.keys())
        self.assertTrue("city" in arrival.keys())
        self.assertTrue("city_code" in arrival.keys())
        self.assertTrue("country" in arrival.keys())
        self.assertTrue("terminal" in arrival.keys())
        self.assertTrue("time" in arrival.keys())

    def test_routes_departure(self):
        self.assertTrue("departure" in self.r_json.keys())
        departure = self.r_json.get("arrival")
        self.assertTrue("airport" in departure.keys())
        self.assertTrue("airport_code" in departure.keys())
        self.assertTrue("city" in departure.keys())
        self.assertTrue("city_code" in departure.keys())
        self.assertTrue("country" in departure.keys())
        self.assertTrue("terminal" in departure.keys())
        self.assertTrue("time" in departure.keys())

    def test_routes_flight_num(self):
        self.assertTrue("flight_num" in self.r_json.keys())

    def test_routes_id(self):
        self.assertTrue("id" in self.r_json.keys())

    # Articles API Test

    article_response = requests.get("{}/articles/3".format(api_base))
    a_json = article_response.json()

    def test_articles_response_200(self):
        self.assertEqual(self.article_response.status_code, 200)

    def test_articles_language(self):
        self.assertTrue("article_language" in self.a_json.keys())

    def test_articles_author(self):
        self.assertTrue("author" in self.a_json.keys())

    def test_articles_category(self):
        self.assertTrue("category" in self.a_json.keys())

    def test_articles_country(self):
        self.assertTrue("country" in self.a_json.keys())

    def test_articles_description(self):
        self.assertTrue("description" in self.a_json.keys())

    def test_articles_related_routes(self):
        self.assertTrue("id" in self.a_json.keys())

    def test_articles_id(self):
        self.assertTrue("id" in self.a_json.keys())

    # test multiple countries instances
    multiple_countries_response = requests.get(
        "{}/countries?page=1&pageSize=20".format(api_base)
    )
    mc_json = multiple_countries_response.json()

    def test_multiple_countries_response_200(self):
        self.assertEqual(self.multiple_countries_response.status_code, 200)

    def test_country_count(self):
        self.assertLessEqual(self.mc_json.get("count"), 1000)
        self.assertGreaterEqual(self.mc_json.get("count"), 200)

    def test_country_page_count(self):
        self.assertEqual(len(self.mc_json.get("results")), 20)

    def test_country_result_1(self):
        sample = self.mc_json.get("results")[0]
        self.assertTrue("capital" in sample.keys())
        self.assertTrue("code" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("latitude" in sample.keys())
        self.assertTrue("longitude" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("population" in sample.keys())
        self.assertTrue("region" in sample.keys())

    def test_country_result_2(self):
        sample = self.mc_json.get("results")[1]
        self.assertTrue("capital" in sample.keys())
        self.assertTrue("code" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("latitude" in sample.keys())
        self.assertTrue("longitude" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("population" in sample.keys())
        self.assertTrue("region" in sample.keys())

    def test_country_result_3(self):
        sample = self.mc_json.get("results")[2]
        self.assertTrue("capital" in sample.keys())
        self.assertTrue("code" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("latitude" in sample.keys())
        self.assertTrue("longitude" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("population" in sample.keys())
        self.assertTrue("region" in sample.keys())

    def test_country_result_4(self):
        sample = self.mc_json.get("results")[3]
        self.assertTrue("capital" in sample.keys())
        self.assertTrue("code" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("latitude" in sample.keys())
        self.assertTrue("longitude" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("population" in sample.keys())
        self.assertTrue("region" in sample.keys())

    def test_country_result_5(self):
        sample = self.mc_json.get("results")[4]
        self.assertTrue("capital" in sample.keys())
        self.assertTrue("code" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("latitude" in sample.keys())
        self.assertTrue("longitude" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("population" in sample.keys())
        self.assertTrue("region" in sample.keys())

    def test_country_content_1(self):
        sample = self.mc_json.get("results")[0]
        self.assertTrue(isinstance(sample.get("capital"), str))
        self.assertTrue(isinstance(sample.get("code"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("latitude"), float))
        self.assertTrue(isinstance(sample.get("longitude"), float))
        self.assertTrue(isinstance(sample.get("name"), str))
        self.assertTrue(isinstance(sample.get("population"), int))
        self.assertTrue(isinstance(sample.get("region"), str))

    def test_country_content_2(self):
        sample = self.mc_json.get("results")[1]
        self.assertTrue(isinstance(sample.get("capital"), str))
        self.assertTrue(isinstance(sample.get("code"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("latitude"), float))
        self.assertTrue(isinstance(sample.get("longitude"), float))
        self.assertTrue(isinstance(sample.get("name"), str))
        self.assertTrue(isinstance(sample.get("population"), int))
        self.assertTrue(isinstance(sample.get("region"), str))

    def test_country_content_3(self):
        sample = self.mc_json.get("results")[2]
        self.assertTrue(isinstance(sample.get("capital"), str))
        self.assertTrue(isinstance(sample.get("code"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("latitude"), float))
        self.assertTrue(isinstance(sample.get("longitude"), float))
        self.assertTrue(isinstance(sample.get("name"), str))
        self.assertTrue(isinstance(sample.get("population"), int))
        self.assertTrue(isinstance(sample.get("region"), str))

    # test multiple routes instances
    multiple_routes_response = requests.get(
        "{}/routes?page=1&pageSize=20".format(api_base)
    )
    rc_json = multiple_routes_response.json()

    def test_multiple_routes_response_200(self):
        self.assertEqual(self.multiple_routes_response.status_code, 200)

    def test_routes_count(self):
        self.assertLessEqual(self.rc_json.get("count"), 10000)
        self.assertGreaterEqual(self.rc_json.get("count"), 8000)

    def test_routes_page_count(self):
        self.assertEqual(len(self.rc_json.get("results")), 20)

    def test_routes_result_1(self):
        sample = self.rc_json.get("results")[0]
        self.assertTrue("airline_code" in sample.keys())
        self.assertTrue("arrival_city" in sample.keys())
        self.assertTrue("arrival_country" in sample.keys())
        self.assertTrue("departure_city" in sample.keys())
        self.assertTrue("departure_country" in sample.keys())
        self.assertTrue("flight_num" in sample.keys())
        self.assertTrue("id" in sample.keys())

    def test_routes_result_2(self):
        sample = self.rc_json.get("results")[1]
        self.assertTrue("airline_code" in sample.keys())
        self.assertTrue("arrival_city" in sample.keys())
        self.assertTrue("arrival_country" in sample.keys())
        self.assertTrue("departure_city" in sample.keys())
        self.assertTrue("departure_country" in sample.keys())
        self.assertTrue("flight_num" in sample.keys())
        self.assertTrue("id" in sample.keys())

    def test_routes_result_3(self):
        sample = self.rc_json.get("results")[2]
        self.assertTrue("airline_code" in sample.keys())
        self.assertTrue("arrival_city" in sample.keys())
        self.assertTrue("arrival_country" in sample.keys())
        self.assertTrue("departure_city" in sample.keys())
        self.assertTrue("departure_country" in sample.keys())
        self.assertTrue("flight_num" in sample.keys())
        self.assertTrue("id" in sample.keys())

    def test_routes_result_4(self):
        sample = self.rc_json.get("results")[3]
        self.assertTrue("airline_code" in sample.keys())
        self.assertTrue("arrival_city" in sample.keys())
        self.assertTrue("arrival_country" in sample.keys())
        self.assertTrue("departure_city" in sample.keys())
        self.assertTrue("departure_country" in sample.keys())
        self.assertTrue("flight_num" in sample.keys())
        self.assertTrue("id" in sample.keys())

    def test_routes_result_5(self):
        sample = self.rc_json.get("results")[4]
        self.assertTrue("airline_code" in sample.keys())
        self.assertTrue("arrival_city" in sample.keys())
        self.assertTrue("arrival_country" in sample.keys())
        self.assertTrue("departure_city" in sample.keys())
        self.assertTrue("departure_country" in sample.keys())
        self.assertTrue("flight_num" in sample.keys())
        self.assertTrue("id" in sample.keys())

    def test_routes_content_1(self):
        sample = self.rc_json.get("results")[0]
        self.assertTrue(isinstance(sample.get("airline_code"), str))
        self.assertTrue(isinstance(sample.get("arrival_city"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("arrival_country"), str))
        self.assertTrue(isinstance(sample.get("departure_city"), str))
        self.assertTrue(isinstance(sample.get("departure_country"), str))
        self.assertTrue(isinstance(sample.get("flight_num"), str))

    def test_routes_content_2(self):
        sample = self.rc_json.get("results")[1]
        self.assertTrue(isinstance(sample.get("airline_code"), str))
        self.assertTrue(isinstance(sample.get("arrival_city"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("arrival_country"), str))
        self.assertTrue(isinstance(sample.get("departure_city"), str))
        self.assertTrue(isinstance(sample.get("departure_country"), str))
        self.assertTrue(isinstance(sample.get("flight_num"), str))

    def test_routes_content_3(self):
        sample = self.rc_json.get("results")[2]
        self.assertTrue(isinstance(sample.get("airline_code"), str))
        self.assertTrue(isinstance(sample.get("arrival_city"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("arrival_country"), str))
        self.assertTrue(isinstance(sample.get("departure_city"), str))
        self.assertTrue(isinstance(sample.get("departure_country"), str))
        self.assertTrue(isinstance(sample.get("flight_num"), str))

    # test multiple articles instances
    multiple_articles_response = requests.get(
        "{}/articles?page=1&pageSize=20".format(api_base)
    )
    ac_json = multiple_articles_response.json()

    def test_multiple_articles_response_200(self):
        self.assertEqual(self.multiple_articles_response.status_code, 200)

    def test_articles_count(self):
        self.assertLessEqual(self.ac_json.get("count"), 10000)
        self.assertGreaterEqual(self.ac_json.get("count"), 7000)

    def test_articles_page_count(self):
        self.assertEqual(len(self.ac_json.get("results")), 20)

    def test_articles_result_1(self):
        sample = self.ac_json.get("results")[0]
        self.assertTrue("article_language" in sample.keys())
        self.assertTrue("category" in sample.keys())
        self.assertTrue("country" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("published" in sample.keys())
        self.assertTrue("title" in sample.keys())
        self.assertTrue("url_to_image" in sample.keys())

    def test_articles_result_2(self):
        sample = self.ac_json.get("results")[1]
        self.assertTrue("article_language" in sample.keys())
        self.assertTrue("category" in sample.keys())
        self.assertTrue("country" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("published" in sample.keys())
        self.assertTrue("title" in sample.keys())
        self.assertTrue("url_to_image" in sample.keys())

    def test_articles_result_3(self):
        sample = self.ac_json.get("results")[2]
        self.assertTrue("article_language" in sample.keys())
        self.assertTrue("category" in sample.keys())
        self.assertTrue("country" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("published" in sample.keys())
        self.assertTrue("title" in sample.keys())
        self.assertTrue("url_to_image" in sample.keys())

    def test_articles_result_4(self):
        sample = self.ac_json.get("results")[3]
        self.assertTrue("article_language" in sample.keys())
        self.assertTrue("category" in sample.keys())
        self.assertTrue("country" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("published" in sample.keys())
        self.assertTrue("title" in sample.keys())
        self.assertTrue("url_to_image" in sample.keys())

    def test_articles_result_5(self):
        sample = self.ac_json.get("results")[4]
        self.assertTrue("article_language" in sample.keys())
        self.assertTrue("category" in sample.keys())
        self.assertTrue("country" in sample.keys())
        self.assertTrue("id" in sample.keys())
        self.assertTrue("published" in sample.keys())
        self.assertTrue("title" in sample.keys())
        self.assertTrue("url_to_image" in sample.keys())

    def test_articles_content_1(self):
        sample = self.ac_json.get("results")[0]
        self.assertTrue(isinstance(sample.get("article_language"), str))
        self.assertTrue(isinstance(sample.get("country"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("published"), str))
        self.assertTrue(isinstance(sample.get("title"), str))
        self.assertTrue(isinstance(sample.get("url_to_image"), str))

    def test_articles_content_2(self):
        sample = self.ac_json.get("results")[1]
        self.assertTrue(isinstance(sample.get("article_language"), str))
        self.assertTrue(isinstance(sample.get("country"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("published"), str))
        self.assertTrue(isinstance(sample.get("title"), str))
        self.assertTrue(isinstance(sample.get("url_to_image"), str))

    def test_articles_content_3(self):
        sample = self.ac_json.get("results")[3]
        self.assertTrue(isinstance(sample.get("article_language"), str))
        self.assertTrue(isinstance(sample.get("country"), str))
        self.assertTrue(isinstance(sample.get("id"), int))
        self.assertTrue(isinstance(sample.get("published"), str))
        self.assertTrue(isinstance(sample.get("title"), str))
        self.assertTrue(isinstance(sample.get("url_to_image"), str))

    # test general search for countries instances
    general_countries_response = requests.get(
        "{}/countries?page=1&pageSize=10&q=India".format(api_base)
    )
    gc_json = general_countries_response.json()

    def test_general_search_countries_status_ok(self):
        self.assertEqual(self.general_countries_response.status_code, 200)

    def test_general_search_countries_content(self):
        self.assertEqual(self.gc_json.get("count"), 10)
        for sample in self.gc_json.get("results"):
            self.assertTrue(isinstance(sample.get("capital"), str))
            self.assertTrue(isinstance(sample.get("code"), str))
            self.assertTrue(isinstance(sample.get("id"), int))
            self.assertTrue(isinstance(sample.get("latitude"), float))
            self.assertTrue(isinstance(sample.get("longitude"), float))
            self.assertTrue(isinstance(sample.get("name"), str))
            self.assertTrue(isinstance(sample.get("population"), int))
            self.assertTrue(isinstance(sample.get("region"), str))

    # test general search for articles instances
    general_articles_response = requests.get(
        "{}/articles?page=1&pageSize=10&q=Coronavirus".format(api_base)
    )
    ga_json = general_articles_response.json()

    def test_general_search_articles_status_ok(self):
        self.assertEqual(self.general_articles_response.status_code, 200)

    def test_general_search_articles_content(self):
        self.assertEqual(self.ga_json.get("count"), 251)
        for sample in self.ga_json.get("results"):
            self.assertTrue(isinstance(sample.get("article_language"), str))
            self.assertTrue(isinstance(sample.get("country"), str))
            self.assertTrue(isinstance(sample.get("id"), int))
            self.assertTrue(isinstance(sample.get("published"), str))
            self.assertTrue(isinstance(sample.get("title"), str))

    # test general search for articles instances
    general_routes_response = requests.get(
        "{}/routes?page=1&pageSize=10&q=New%20York".format(api_base)
    )
    gr_json = general_routes_response.json()

    def test_general_search_routes_status_ok(self):
        self.assertEqual(self.general_routes_response.status_code, 200)

    def test_general_search_routes_content(self):
        self.assertEqual(self.gr_json.get("count"), 92)
        for sample in self.gr_json.get("results"):
            self.assertTrue(isinstance(sample.get("airline_code"), str))
            self.assertTrue(isinstance(sample.get("arrival_city"), str))
            self.assertTrue(isinstance(sample.get("id"), int))
            self.assertTrue(isinstance(sample.get("arrival_country"), str))
            self.assertTrue(isinstance(sample.get("departure_city"), str))
            self.assertTrue(isinstance(sample.get("departure_country"), str))
            self.assertTrue(isinstance(sample.get("flight_num"), str))


if __name__ == "__main__":
    main()
