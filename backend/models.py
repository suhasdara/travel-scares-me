from flask import Flask
from init_db import init_db
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow import fields

app = Flask(__name__)
CORS(app)
db = init_db(app)
ma = Marshmallow(app)


###### HELPER TABLES


countries_languages = db.Table(
    "countries_languages",
    db.Column(
        "country_id", db.Integer, db.ForeignKey("countries.id"), primary_key=True
    ),
    db.Column(
        "language_id", db.Integer, db.ForeignKey("languages.id"), primary_key=True
    ),
)

countries_currencies = db.Table(
    "countries_currencies",
    db.Column(
        "country_id", db.Integer, db.ForeignKey("countries.id"), primary_key=True
    ),
    db.Column(
        "currency_id", db.Integer, db.ForeignKey("currencies.id"), primary_key=True
    ),
)

countries_time_zones = db.Table(
    "countries_time_zones",
    db.Column(
        "country_id", db.Integer, db.ForeignKey("countries.id"), primary_key=True
    ),
    db.Column(
        "time_zone_id", db.Integer, db.ForeignKey("time_zones.id"), primary_key=True
    ),
)

countries_neighbors = db.Table(
    "countries_neighbors",
    db.Column(
        "country_id", db.Integer, db.ForeignKey("countries.id"), primary_key=True
    ),
    db.Column(
        "neighbor_id", db.Integer, db.ForeignKey("neighbors.id"), primary_key=True
    ),
)


###### MODELS


class Country(db.Model):
    __tablename__ = "countries"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String, nullable=False)
    capital = db.Column(db.String, nullable=False)
    region = db.Column(db.String, nullable=False)
    population = db.Column(db.Integer, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    covid_cases = db.Column(db.Integer, nullable=True)
    covid_deaths = db.Column(db.Integer, nullable=True)
    percent_vaccinated = db.Column(db.Float, nullable=True)
    percent_with_covid_symptoms = db.Column(db.Float, nullable=True)
    percent_wearing_mask = db.Column(db.Float, nullable=True)
    flag_url = db.Column(db.String, nullable=False)
    threat_level = db.Column(db.Integer, nullable=True)
    travel_advisories = db.Column(db.String, nullable=True)
    languages = db.relationship(
        "Language",
        secondary=countries_languages,
        backref=db.backref("languages", lazy="dynamic"),
    )
    currencies = db.relationship(
        "Currency",
        secondary=countries_currencies,
        backref=db.backref("currencies", lazy="dynamic"),
    )
    timezones = db.relationship(
        "Timezone",
        secondary=countries_time_zones,
        backref=db.backref("timezones", lazy="dynamic"),
    )
    neighbors = db.relationship(
        "Neighbor",
        secondary=countries_neighbors,
        backref=db.backref("neighbors", lazy="dynamic"),
    )

    airport = db.relationship(
        "Airport", backref="current_country", lazy=True, uselist=False
    )
    article = db.relationship(
        "Article", backref="current_country", lazy=True, uselist=False
    )


class Language(db.Model):
    __tablename__ = "languages"
    id = db.Column(db.Integer, primary_key=True)
    country_code = db.Column(db.String, nullable=False)
    language_name = db.Column(db.String, nullable=False)


class Currency(db.Model):
    __tablename__ = "currencies"
    id = db.Column(db.Integer, primary_key=True)
    country_code = db.Column(db.String, nullable=False)
    currency = db.Column(db.String, nullable=False)


class Timezone(db.Model):
    __tablename__ = "time_zones"
    id = db.Column(db.Integer, primary_key=True)
    country_code = db.Column(db.String, nullable=False)
    time_zone = db.Column(db.String, nullable=False)


class Neighbor(db.Model):
    __tablename__ = "neighbors"
    id = db.Column(db.Integer, primary_key=True)
    country_code = db.Column(db.String, nullable=False)
    neighbor_country_code = db.Column(db.String, nullable=False)
    neighbor_country_name = db.Column(db.String, nullable=False)
    neighbor_country_id = db.Column(db.Integer, nullable=False)


class Route(db.Model):
    __tablename__ = "airplane_routes"
    id = db.Column(db.Integer, primary_key=True)
    departure = db.Column(db.String, nullable=False)
    arrival = db.Column(db.String, nullable=False)
    airline = db.Column(db.String, nullable=False)
    airline_country = db.Column(db.String, nullable=True)
    airline_code = db.Column(db.String, nullable=True)
    flight_num = db.Column(db.String, nullable=False)
    departure_time = db.Column(db.String, nullable=True)
    departure_terminal = db.Column(db.String, nullable=True)
    arrival_time = db.Column(db.String, nullable=True)
    arrival_terminal = db.Column(db.String, nullable=True)
    departure_airport_id = db.Column(
        db.Integer, db.ForeignKey("airports.id"), nullable=False
    )
    arrival_airport_id = db.Column(
        db.Integer, db.ForeignKey("airports.id"), nullable=False
    )
    departure_country_name = db.Column(db.String, nullable=False)
    arrival_country_name = db.Column(db.String, nullable=False)


class Airport(db.Model):
    __tablename__ = "airports"
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String, nullable=False)
    country = db.Column(db.String, nullable=False)
    airport = db.Column(db.String, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    city_code = db.Column(db.String, nullable=False)
    country_code = db.Column(db.String, nullable=False)
    code = db.Column(db.String, nullable=False)
    time_zone = db.Column(db.String, nullable=False)
    country_flag_url = db.Column(db.String, nullable=False)
    country_id = db.Column(db.Integer, db.ForeignKey("countries.id"), nullable=False)
    route_departure = db.relationship(
        "Route",
        backref="current_source_airport",
        foreign_keys="Route.departure_airport_id",
        lazy=True,
        uselist=False,
    )
    route_arrival = db.relationship(
        "Route",
        backref="current_dest_airport",
        foreign_keys="Route.arrival_airport_id",
        lazy=True,
        uselist=False,
    )


class Article(db.Model):
    __tablename__ = "news_articles"
    id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.String, nullable=False)
    author = db.Column(db.String, nullable=True)
    title = db.Column(db.String, nullable=False)
    description = db.Column(db.String, nullable=True)
    url = db.Column(db.String, nullable=False)
    published = db.Column(db.String, nullable=False)
    country = db.Column(db.String, nullable=False)
    category = db.Column(db.String, nullable=False)
    url_to_image = db.Column(db.String, nullable=True)
    url_to_source = db.Column(db.String, nullable=False)
    source_logo_url = db.Column(db.String, nullable=False)
    source_description = db.Column(db.String, nullable=True)
    article_language = db.Column(db.String, nullable=False)
    country_code = db.Column(db.String, nullable=False)
    country_id = db.Column(db.Integer, db.ForeignKey("countries.id"), nullable=False)


###### SCHEMAS


class CountrySchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    code = fields.Str(required=True)
    capital = fields.Str(required=True)
    region = fields.Str(required=True)
    population = fields.Int(required=True)
    latitude = fields.Float(required=True)
    longitude = fields.Float(required=True)
    covid_cases = fields.Int(required=False)
    covid_deaths = fields.Int(required=False)
    percent_vaccinated = fields.Float(required=False)
    percent_with_covid_symptoms = fields.Float(required=False)
    percent_wearing_mask = fields.Float(required=False)
    flag_url = fields.Str(required=True)
    threat_level = fields.Int(required=False)
    travel_advisories = fields.Str(required=False)
    languages = fields.Pluck("LanguageSchema", "language_name", many=True)
    currencies = fields.Pluck("CurrencySchema", "currency", many=True)
    timezones = fields.Pluck("TimezoneSchema", "time_zone", many=True)
    neighbors = fields.Nested(
        "NeighborSchema",
        only=["neighbor_country_name", "neighbor_country_id"],
        required=True,
        many=True,
    )


class LanguageSchema(ma.Schema):
    id = fields.Int(required=True)
    country_code = fields.Str(required=True)
    language_name = fields.Str(required=True)


class CurrencySchema(ma.Schema):
    id = fields.Int(required=True)
    country_code = fields.Str(required=True)
    currency = fields.Str(required=True)


class TimezoneSchema(ma.Schema):
    id = fields.Int(required=True)
    country_code = fields.Str(required=True)
    time_zone = fields.Str(required=True)


class NeighborSchema(ma.Schema):
    id = fields.Int(required=True)
    country_code = fields.Str(required=True)
    neighbor_country_code = fields.Str(required=True)
    neighbor_country_name = fields.Str(required=True)
    neighbor_country_id = fields.Int(required=True)


class RouteSchema(ma.Schema):
    id = fields.Int(required=True)
    airline_code = fields.Str(required=False)
    flight_num = fields.Str(required=True)

    # for single route
    departure = fields.Str(required=True)
    arrival = fields.Str(required=True)
    airline = fields.Str(required=True)
    airline_country = fields.Str(required=False)
    departure_time = fields.Str(required=False)
    departure_terminal = fields.Str(required=False)
    arrival_time = fields.Str(required=False)
    arrival_terminal = fields.Str(required=False)
    departure_airport = fields.Nested(
        "AirportSchema", required=True, attribute="current_source_airport", many=False
    )
    arrival_airport = fields.Nested(
        "AirportSchema", required=True, attribute="current_dest_airport", many=False
    )

    # for multiple routes
    departure_country = fields.Str(required=False)
    departure_city = fields.Str(required=False)
    arrival_country = fields.Str(required=False)
    arrival_city = fields.Str(required=False)

    # for internal use: filter related routes
    departure_country_name = fields.Str(required=False)
    arrival_country_name = fields.Str(required=False)


class AirportSchema(ma.Schema):
    id = fields.Int(required=True)
    city = fields.Str(required=True)
    country = fields.Str(required=True)
    airport = fields.Str(required=True)
    latitude = fields.Float(required=True)
    longitude = fields.Float(required=True)
    city_code = fields.Str(required=True)
    country_code = fields.Str(required=True)
    code = fields.Str(required=True)
    time_zone = fields.Str(required=True)
    country_flag_url = fields.Str(required=True)
    country_id = fields.Nested(
        "CountrySchema",
        only=["id"],
        required=True,
        attribute="current_country",
        many=False,
    )


class ArticleSchema(ma.Schema):
    id = fields.Int(required=True)
    source = fields.Str(required=True)
    author = fields.Str(required=False)
    title = fields.Str(required=True)
    description = fields.Str(required=False)
    url = fields.Str(required=True)
    published = fields.Str(required=True)
    country = fields.Str(required=True)
    category = fields.Str(required=True)
    url_to_image = fields.Str(required=False)
    url_to_source = fields.Str(required=True)
    source_logo_url = fields.Str(required=True)
    source_description = fields.Str(required=False)
    article_language = fields.Str(required=True)
    country_code = fields.Str(required=True)
    country_id = fields.Nested(
        "CountrySchema",
        only=["id"],
        required=True,
        attribute="current_country",
        many=False,
    )


country_schema = CountrySchema()
language_schema = LanguageSchema()
currency_schema = CurrencySchema()
timezone_schema = TimezoneSchema()
route_schema = RouteSchema()
airport_schema = AirportSchema()
article_schema = ArticleSchema()
