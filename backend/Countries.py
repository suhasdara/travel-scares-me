from utils import get_sort_direction, get_route_param
from models import Country, Language, Neighbor, Currency, Timezone
from sqlalchemy import or_


def format_country(country):
    country["neighbors"] = [
        {
            "name": neighbor["neighbor_country_name"],
            "id": neighbor["neighbor_country_id"],
        }
        for neighbor in country["neighbors"]
    ]
    country["covid"] = {
        "cases": country.pop("covid_cases"),
        "deaths": country.pop("covid_deaths"),
        "percent_covid_symptoms": country.pop("percent_with_covid_symptoms"),
        "percent_vaccinated": country.pop("percent_vaccinated"),
        "percent_mask": country.pop("percent_wearing_mask"),
    }


def sort_countries(query, order_by, ordering):
    if order_by is not None and ordering is not None:
        if order_by == "name":
            query = query.order_by(get_sort_direction(Country.name, ordering))
        elif order_by == "code":
            query = query.order_by(get_sort_direction(Country.code, ordering))
        elif order_by == "region":
            query = query.order_by(get_sort_direction(Country.region, ordering))
        elif order_by == "population":
            query = query.order_by(get_sort_direction(Country.population, ordering))
        elif order_by == "capital":
            query = query.order_by(get_sort_direction(Country.capital, ordering))
        elif order_by == "latitude":
            query = query.order_by(get_sort_direction(Country.latitude, ordering))
        elif order_by == "longitude":
            query = query.order_by(get_sort_direction(Country.longitude, ordering))
        else:
            raise ValueError
        return query
    elif order_by is None and ordering is None:
        return query
    else:
        raise ValueError


def filter_country_param(query, column, values):
    min_value, max_value = values
    if min_value is not None and max_value is not None:
        low = int(min_value)
        high = int(max_value)
        if low > high:
            raise ValueError
        query = query.filter(column >= low, column <= high)
    elif min_value is not None:
        low = int(min_value)
        query = query.filter(column >= low)
    elif max_value is not None:
        high = int(max_value)
        query = query.filter(column <= high)
    return query


def filter_countries(query, args):
    region = get_route_param(args, "region")
    latitude = [
        get_route_param(args, "latitudeMin"),
        get_route_param(args, "latitudeMax"),
    ]
    longitude = [
        get_route_param(args, "longitudeMin"),
        get_route_param(args, "longitudeMax"),
    ]
    population = [
        get_route_param(args, "populationMin"),
        get_route_param(args, "populationMax"),
    ]

    if region is not None:
        query = query.filter(Country.region == region)
    query = filter_country_param(query, Country.latitude, latitude)
    query = filter_country_param(query, Country.longitude, longitude)
    query = filter_country_param(query, Country.population, population)

    return query


def search_countries(query, q):
    if q is None:
        return query

    word = q.strip().lower()
    search_queries = []
    search_queries.append(Country.name.contains(word))
    search_queries.append(Country.code.contains(word))
    search_queries.append(Country.capital.contains(word))
    search_queries.append(Country.region.contains(word))
    search_queries.append(Country.languages.any(Language.language_name.contains(word)))
    search_queries.append(
        Country.neighbors.any(Neighbor.neighbor_country_name.contains(word))
    )
    search_queries.append(Country.currencies.any(Currency.currency.contains(word)))
    search_queries.append(Country.timezones.any(Timezone.time_zone.contains(word)))
    try:
        num = int(word)
        search_queries.append(Country.latitude.contains(num))
        search_queries.append(Country.longitude.contains(num))
        search_queries.append(Country.population.contains(num))
        search_queries.append(Country.threat_level.contains(num))
        search_queries.append(Country.covid_cases.contains(num))
        search_queries.append(Country.covid_deaths.contains(num))
    except ValueError:
        pass

    query = query.filter(or_(*search_queries))
    return query
