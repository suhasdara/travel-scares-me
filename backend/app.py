import json
import flask
from flask import Flask, request


from models import (
    app,
    db,
    Country,
    Route,
    Airport,
    Article,
    country_schema,
    route_schema,
    article_schema,
)
from AirplaneRoutes import *
from Articles import *
from Countries import *
from utils import get_route_param, args_to_dict


# NOTE: This route is needed for the default EB health check route
@app.route("/")
def home():
    return "ok"


@app.route("/api/countries/<id>", methods=["GET"])
def get_country(id):
    country = Country.query.get(id)
    if country is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response

    country = country_schema.dump(country)
    format_country(country)

    articles_query = (
        db.session.query(Article.id, Article.title, Article.country_id)
        .filter_by(country_id=country["id"], category="health")
        .limit(20)
    )
    country["related_articles"] = [
        {"title": article["title"], "id": article["id"]}
        for article in article_schema.dump(articles_query, many=True)
    ]
    routes_search_query = (
        db.session.query(
            Route.id, Route.arrival_country_name, Route.departure_country_name
        )
        .filter_by(departure_country_name=country["name"])
        .limit(20)
    )
    country["related_routes"] = [
        {"destination": route["arrival_country_name"], "id": route["id"]}
        for route in route_schema.dump(routes_search_query, many=True)
    ]

    return country


@app.route("/api/routes/<id>", methods=["GET"])
def get_route(id):
    route = Route.query.get(id)
    if route is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response

    route = route_schema.dump(route)
    format_route(route)

    articles_query = (
        db.session.query(Article)
        .filter_by(country_id=route["arrival"]["country_id"], category="health")
        .limit(20)
    )
    route["related_articles"] = [
        {"title": article["title"], "id": article["id"]}
        for article in article_schema.dump(articles_query, many=True)
    ]

    return route


@app.route("/api/articles/<id>", methods=["GET"])
def get_article(id):
    article = Article.query.get(id)
    if article is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response

    article = article_schema.dump(article)
    format_article(article)

    routes_search_query = (
        db.session.query(
            Route.id, Route.departure_country_name, Route.arrival_country_name
        )
        .filter_by(arrival_country_name=article["country"])
        .limit(20)
    )
    article["related_routes"] = [
        {"source": route["departure_country_name"], "id": route["id"]}
        for route in route_schema.dump(routes_search_query, many=True)
    ]

    return article


@app.route("/api/countries", methods=["GET"])
def get_countries():
    args = args_to_dict(request.args)
    try:
        if not get_route_param(args, "page") or not get_route_param(args, "pageSize"):
            raise ValueError

        page = int(get_route_param(args, "page"))
        page_size = int(get_route_param(args, "pageSize"))

        fields = [
            Country.id,
            Country.capital,
            Country.code,
            Country.latitude,
            Country.longitude,
            Country.name,
            Country.population,
            Country.region,
            Country.flag_url,
        ]

        query = db.session.query(Country)

        order_by = get_route_param(args, "orderBy")
        ordering = get_route_param(args, "ordering")
        q = get_route_param(args, "q")
        query = sort_countries(query, order_by, ordering)
        query = filter_countries(query, args)
        query = search_countries(query, q)

        query = query.with_entities(*fields)
        count = query.count()
        countries = query.paginate(page=page, per_page=page_size)

        results = country_schema.dump(countries.items, many=True)
        return {"count": count, "results": results}

    except ValueError:
        response = flask.Response(
            json.dumps({"error": "invalid url"}), mimetype="application/json"
        )
        response.status_code = 406
        return response


@app.route("/api/routes", methods=["GET"])
def get_routes():
    args = args_to_dict(request.args)
    try:
        if not get_route_param(args, "page") or not get_route_param(args, "pageSize"):
            raise ValueError

        page = int(get_route_param(args, "page"))
        page_size = int(get_route_param(args, "pageSize"))

        dep_airport = db.aliased(Airport)
        arr_airport = db.aliased(Airport)
        entity_fields = [
            Route.id,
            Route.airline_code,
            Route.flight_num,
            dep_airport.country.label("departure_country"),
            dep_airport.city.label("departure_city"),
            arr_airport.country.label("arrival_country"),
            arr_airport.city.label("arrival_city"),
        ]
        query_fields = [
            Route.id,
            Route.airline_code,
            Route.flight_num,
            Route.departure_airport_id,
            Route.arrival_airport_id,
        ]

        query = db.session.query(*query_fields)
        query = query.join(
            dep_airport, dep_airport.id == Route.departure_airport_id
        ).join(arr_airport, arr_airport.id == Route.arrival_airport_id)

        order_by = get_route_param(args, "orderBy")
        ordering = get_route_param(args, "ordering")
        q = get_route_param(args, "q")
        query = sort_routes(query, order_by, ordering, dep_airport, arr_airport)
        query = filter_routes(query, args, dep_airport, arr_airport)
        query = search_routes(query, q, dep_airport, arr_airport)

        query = query.with_entities(*entity_fields)
        count = query.count()
        routes = query.paginate(page=page, per_page=page_size)

        results = route_schema.dump(routes.items, many=True)
        return {"count": count, "results": results}

    except ValueError:
        response = flask.Response(
            json.dumps({"error": "invalid url"}), mimetype="application/json"
        )
        response.status_code = 406
        return response


@app.route("/api/articles", methods=["GET"])
def get_articles():
    args = args_to_dict(request.args)
    try:
        if not get_route_param(args, "page") or not get_route_param(args, "pageSize"):
            raise ValueError

        page = int(get_route_param(args, "page"))
        page_size = int(get_route_param(args, "pageSize"))

        fields = [
            Article.id,
            Article.article_language,
            Article.category,
            Article.country,
            Article.published,
            Article.title,
            Article.url_to_image,
        ]

        query = db.session.query(Article)

        order_by = get_route_param(args, "orderBy")
        ordering = get_route_param(args, "ordering")
        q = get_route_param(args, "q")
        query = sort_articles(query, order_by, ordering)
        query = filter_articles(query, args)
        query = search_articles(query, q)

        query = query.with_entities(*fields)
        count = query.count()
        articles = query.paginate(page=page, per_page=page_size)

        results = article_schema.dump(articles.items, many=True)
        return {"count": count, "results": results}

    except ValueError:
        response = flask.Response(
            json.dumps({"error": "invalid url"}), mimetype="application/json"
        )
        response.status_code = 406
        return response


if __name__ == "__main__":
    app.run(port=8080)
