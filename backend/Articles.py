from utils import get_sort_direction, get_route_param
from models import Article
from sqlalchemy import or_


def format_article(article):
    article["country_id"] = article["country_id"]["id"]


def sort_articles(query, order_by, ordering):
    if order_by is not None and ordering is not None:
        if order_by == "category":
            query = query.order_by(get_sort_direction(Article.category, ordering))
        elif order_by == "country":
            query = query.order_by(get_sort_direction(Article.country, ordering))
        elif order_by == "language":
            query = query.order_by(
                get_sort_direction(Article.article_language, ordering)
            )
        elif order_by == "title":
            query = query.order_by(get_sort_direction(Article.title, ordering))
        elif order_by == "published":
            query = query.order_by(get_sort_direction(Article.published, ordering))
        else:
            raise ValueError
        return query
    elif order_by is None and ordering is None:
        return query
    else:
        raise ValueError


def filter_articles(query, args):
    category = get_route_param(args, "category")
    country = get_route_param(args, "country")
    language = get_route_param(args, "language")

    if category is not None:
        query = query.filter(Article.category == category)
    if country is not None:
        query = query.filter(Article.country == country)
    if language is not None:
        query = query.filter(Article.article_language == language)

    return query


def search_articles(query, q):
    if q is None:
        return query

    word = q.strip().lower()
    search_queries = []
    search_queries.append(Article.title.contains(word))
    search_queries.append(Article.article_language.contains(word))
    search_queries.append(Article.country.contains(word))
    search_queries.append(Article.category.contains(word))
    search_queries.append(Article.author.contains(word))
    search_queries.append(Article.source.contains(word))
    search_queries.append(Article.source_description.contains(word))
    search_queries.append(Article.description.contains(word))
    search_queries.append(Article.url_to_source.contains(word))

    query = query.filter(or_(*search_queries))
    return query
