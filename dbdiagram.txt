Table countries as C {
  id int [pk]
  name varchar(200)
  code varchar(10)
  capital varchar(100)
  region varchar(50)
  population int
  latitude double
  longitude double
  covid_cases int
  covid_deaths int
  percent_vaccinated double
  percent_with_covid_symptoms double
  percent_wearing_mask double
  flag_url varchar(100)
  threat_level int
  travel_advisories text
}

Table languages as LA {
  id int [pk]
  country_code varchar(10)
  language_name varchar(100)
}

Table currencies as CU {
  id int [pk]
  country_code varchar(10)
  currency varchar(100)
}

Table neighbors as NB {
  id int [pk]
  country_code varchar(10)
  neighbor_country_code varchar(10)
  neighbor_country_id int
  neighbor_country_name varchar(200)
}

Table time_zones as TZ {
  id int [pk]
  country_code varchar(10)
  time_zone varchar(20)
}

Table countries_languages as CLA {
  country_id int [ref: > C.id]
  language_id int [ref: > LA.id]
}

Table countries_currencies as CCU {
  country_id int [ref: > C.id]
  currency_id int [ref: > CU.id]
}

Table countries_neighbors as CNB {
  country_id int [ref: > C.id]
  neighbor_id int [ref: > NB.id]
}

Table countries_time_zones as CTZ {
  country_id int [ref: > C.id]
  time_zone_id int [ref: > TZ.id]
}

Table airplane_routes as AR {
  id int [pk]
  departure varchar(10)
  arrival varchar(10)
  airline varchar(100)
  airline_country varchar(100)
  airline_code varchar(10)
  flight_num varchar(10)
  departure_time varchar(20)
  departure_terminal varchar(10)
  arrival_time varchar(20)
  arrival_terminal varchar(10)
  departure_country_name varchar(200)
  arrival_country_name varchar(200)
  departure_airport_id int [ref: > AP.id]
  arrival_airport_id int [ref: > AP.id]
  
  Indexes {
    departure_country_name
    arrival_country_name
  }
}

Table airports as AP {
  id int [pk]
  city varchar(100)
  country varchar(200)
  airport varchar(200)
  latitude double
  longitude double
  city_code varchar(10)
  country_code varchar(10)
  code varchar(3)
  time_zone varchar(20)
  country_flag_url varchar(100)
  country_id int [ref: > C.id]
}

Table news_articles as AT {
  id int [pk]
  source varchar(200)
  author varchar(200)
  title varchar(500)
  description varchar(500)
  url varchar(2000)
  published varchar(50)
  country varchar(200)
  category varchar(50)
  url_to_image varchar(2000)
  url_to_source varchar(2000)
  source_logo_url varchar(2000)
  source_description varchar(500)
  country_code varchar(10)
  article_language varchar(50)
  country_id int [ref: > C.id]
}
